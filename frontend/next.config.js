// const withCSS = require("@zeit/next-css")
// const withImages = require("next-images")
/* module.exports = withCSS({
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      use: ["@svgr/webpack"]
    })

    return config
  }
}) */

// TODO: remove additional packages
// TODO: with-optimized-images
/* 
module.exports = withCSS(
  withImages({
    webpack(config) {
      return config
    }
  })
) */

const withPlugins = require('next-compose-plugins')
const images = require('next-images')

const nextConfig = {
  env: {
    BACKEND_URL: 'http://drinternet.ir/api/v1',
  },
  serverRuntimeConfig: {
    // Will only be available on the server side
    BACKEND_URL: 'http://backend:5000/api/v1',
  },
  publicRuntimeConfig: {
    // Will be available on both server and client
  },
  webpack: (config, options) => {
    config.module.rules.push({
      test: /\.svg$/,
      use: ['@svgr/webpack'],
    })

    return config
  },
}

module.exports = withPlugins(
  [
    [
      images,
      {
        exclude: {
          test: [/\.svg$/],
        },
      },
    ],
  ],
  nextConfig
)

/* module.exports = withImages({
  exclude: {
    test: [/\.svg$/]
  },
  webpack(config, options) {
    config.module.rules.push({
      test: /\.svg$/,
      use: ["@svgr/webpack"]
    })

    return config
  }
}) */
