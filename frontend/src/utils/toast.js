import { toast } from "react-toastify"

const setToast = (text, status, time = 3000) => {
  if (status === "fail") {
    toast.error(text, {
      position: "top-right",
      autoClose: time,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: true
    })
  } else if (status === "success") {
    toast.success(text, {
      position: "top-right",
      autoClose: time,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: true
    })
  } else if (status === "warn") {
    toast.warn(text, {
      position: "top-right",
      autoClose: time,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: true
    })
  }
}

export default setToast
