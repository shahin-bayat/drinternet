import categories from '../data/blog-categories.json'

export const navItems = categories

export const getCategoryInEnglish = categoryTitleInFarsi => {
  let categoryTitleInEnglish = ''
  switch (categoryTitleInFarsi) {
    case 'اخبار':
      categoryTitleInEnglish = 'news'
      break
    case 'نقد و بررسی':
      categoryTitleInEnglish = 'review'
      break
    case 'آموزش':
      categoryTitleInEnglish = 'education'
      break
    case 'review':
      categoryTitleInEnglish = 'tech'
      break
    default:
      categoryTitleInEnglish = ''
  }
  return categoryTitleInEnglish
}
