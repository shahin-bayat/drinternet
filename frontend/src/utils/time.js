import jMoment from "moment-jalaali"
import moment from "moment-timezone"

const changeTimeZone = date => {
  let tehranTime = moment(date)
    .tz("Asia/Tehran")
    .format()
    .split("T")[1]
    .split("+")[0]
    .substr(0, 5)

  date = date.split("T")
  let gDate = date[0]
  const jDate = jMoment(gDate, "YYYY-MM-D").format("jYYYY/jMM/jD")

  return {
    tehranTime,
    jDate
  }
}

export default changeTimeZone
