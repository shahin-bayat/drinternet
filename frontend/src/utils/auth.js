import Router from "next/router"

export const isAuthorized = (roles, ctx) => {
  let role = ""
  // roles: allowed roles , role: current user role
  if (ctx.store.getState().authentication.user) {
    role = ctx.store.getState().authentication.user.role
  }

  if (!roles.includes(role)) {
    if (ctx.req) {
      ctx.res
        .writeHead(302, {
          Location: "/"
        })
        .end()
    } else {
      Router.push("/")
    }
  }
}
