import { combineReducers } from 'redux'
import authReducer from './authReducer'
import modalReducer from './modalReducer'
import messageReducer from './messageReducer'

const rootReducer = combineReducers({
  authentication: authReducer,
  message: messageReducer,
  modal: modalReducer,
})

export default rootReducer
