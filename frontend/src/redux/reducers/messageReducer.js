import { MESSAGE_FETCH, MESSAGE_READ, MESSAGE_DELETE } from "../actionTypes"

const initialState = {
  messages: [],
  currentMessage: {}
}

const messageReducer = (state = initialState, action) => {
  switch (action.type) {
    case MESSAGE_FETCH:
      return { ...state, messages: action.payload }
    case MESSAGE_READ:
      const updatedMessages = state.messages.map(msg => {
        if (msg._id === action.payload._id) {
          msg.isRead = true
        }
        return msg
      })
      return {
        ...state,
        messages: updatedMessages,
        currentMessage: action.payload
      }

    case MESSAGE_DELETE:
      const filteredMessages = state.messages.filter(
        msg => msg._id !== action.payload
      )

      return { ...state, messages: filteredMessages }
    default:
      return state
  }
}

export default messageReducer
