import { AUTHENTICATE, DEAUTHENTICATE } from "../actionTypes"

const initialState = {
  user: null
}

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case AUTHENTICATE:
      return { ...state, user: action.payload }
    case DEAUTHENTICATE:
      return { user: null }
    default:
      return state
  }
}

export default authReducer
