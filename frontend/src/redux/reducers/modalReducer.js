import { MODAL_OPEN, MODAL_CLOSE } from '../actionTypes'

const initialState = [
  { page: 'adsl', modal: false },
  { page: 'contactUs', modal: false },
  { page: 'bandwidth', modal: false },
  { page: 'towers', modal: false },
  { page: 'transit', modal: false },
  { page: 'antivirus', modal: false },
  { page: 'camera', modal: false },
  { page: 'app', modal: false },
  { page: 'website', modal: false },
  { page: 'phone', modal: false },
  { page: 'ngn', modal: false },
]

const alertReducer = (state = initialState, action) => {
  switch (action.type) {
    case MODAL_OPEN:
      const opened = state.map(item => {
        if (item.page === action.payload) {
          item.modal = true
        }
        return item
      })
      return opened
    case MODAL_CLOSE:
      const closed = state.map(item => {
        item.modal = false
        return item
      })
      return closed
    default:
      return state
  }
}

export default alertReducer
