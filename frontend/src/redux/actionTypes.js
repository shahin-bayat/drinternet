export const AUTHENTICATE = 'AUTHENTICATE'
export const DEAUTHENTICATE = 'DEAUTHENTICATE'
export const MESSAGE_FETCH = 'MESSAGE_FETCH'
export const MESSAGE_READ = 'MESSAGE_READ'
export const MESSAGE_DELETE = 'MESSAGE_DELETE'

export const MODAL_OPEN = 'MODAL_OPEN'
export const MODAL_CLOSE = 'MODAL_CLOSE'
