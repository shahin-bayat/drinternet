import axios from 'axios'
import { MESSAGE_FETCH, MESSAGE_READ, MESSAGE_DELETE } from '../actionTypes'
import setToast from '../../utils/toast'
import Router from 'next/router'
import getConfig from 'next/config'
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig()

axios.defaults.withCredentials = true

// ! READ MESSAGE
export const readMessage = messageId => async dispatch => {
  try {
    // console.log("id", messageId)
    const {
      data: {
        data: { currentMessage },
      },
    } = await axios.get(`${process.env.BACKEND_URL}/messages/${messageId}`)
    dispatch({ type: MESSAGE_READ, payload: currentMessage })
  } catch (err) {
    console.log(err)
  }
}

// ! DELETE MESSAGE
export const deleteMessage = messageId => async dispatch => {
  try {
    const response = await axios.delete(
      `${process.env.BACKEND_URL}/messages/${messageId}`
    )
    dispatch({ type: MESSAGE_DELETE, payload: messageId })
  } catch (err) {
    console.log(err)
  }
}

// ! SEND TICKET
export const sendTicket = ticket => async dispatch => {
  try {
    const { data } = await axios.post(`${process.env.BACKEND_URL}/messages`, {
      subject: ticket.subject,
      body: ticket.ticketBody,
    })
    setToast(
      `درخواست شما با موفقیت ارسال شد. کارشناسان ما در اولین فرصت با شما تماس خواهند گرفت`,
      'success',
      3000
    )
    setTimeout(() => {
      Router.push('/profile')
    }, 3000)
  } catch (err) {
    if (
      err.response.data.message ===
      'Please provide the subject and message body'
    ) {
      setToast('لطفا موضوع و متن پیام را مشخص کنید', 'warn')
      console.log(err)
    } else {
      setToast(`ارسال پیام با خطا روبرو شد. مجدد تلاش کنید.`, 'fail')
      console.log(err)
    }
  }
}

// ! SEND MESSAGE , ADMIN
export const sendMessageAdmin = ({ subject, body, to }) => async dispatch => {
  try {
    const { data } = await axios.post(
      `${process.env.BACKEND_URL}/messages/admin`,
      {
        subject,
        body,
        to,
      }
    )

    setToast(`پاسخ با موفقیت ارسال شد`, 'success', 3000)
    setTimeout(() => {
      Router.push('/admin/messages')
    }, 3000)
  } catch (err) {
    setToast(`ارسال پیام با خطا روبرو شد. مجدد تلاش کنید.`, 'fail')
    console.log(err)
  }
}

// ! SEND GROUP MESSAGE, ADMIN
export const sendGroupMessage = ({
  subject,
  body,
  groupEmail,
}) => async dispatch => {
  try {
    const { data } = await axios.post(
      `${process.env.BACKEND_URL}/messages/admin`,
      {
        subject,
        body,
        groupEmail,
      }
    )

    setToast(`پاسخ با موفقیت ارسال شد`, 'success', 3000)
    setTimeout(() => {
      Router.push('/admin/messages')
    }, 3000)
  } catch (err) {
    setToast(`ارسال پیام با خطا روبرو شد. مجدد تلاش کنید.`, 'fail')
    console.log(err)
  }
}

// ! FETCH MESSAGES
export const fetchMessages = (cookies = {}, ctx) => async dispatch => {
  try {
    // in case there is token
    const cookie = JSON.stringify(cookies)
    //typeof cookies = object

    const {
      data: {
        data: { messages },
      },
    } = await axios.get(`${serverRuntimeConfig.BACKEND_URL}/messages`, {
      headers: {
        Cookie: cookie,
      },
    })

    return dispatch({ type: MESSAGE_FETCH, payload: messages })
  } catch (err) {
    console.log('No messages found!')
  }
}
