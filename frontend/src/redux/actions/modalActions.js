import { MODAL_OPEN, MODAL_CLOSE } from '../actionTypes'

export const openModal = page => async dispatch => {
  dispatch({ type: MODAL_OPEN, payload: page })
}
export const closeModal = () => async dispatch => {
  dispatch({ type: MODAL_CLOSE })
}
