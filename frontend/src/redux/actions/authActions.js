import Router from 'next/router'
import axios from 'axios'
import nookies from 'nookies'
import { fetchMessages } from './messageActions'
import Cookies from 'js-cookie'
import { AUTHENTICATE, DEAUTHENTICATE } from '../actionTypes'
import setToast from '../../utils/toast'
import getConfig from 'next/config'
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig()

axios.defaults.withCredentials = true

// ! REGISTER USER
export const registerUser = user => async dispatch => {
  try {
    const response = await axios.post(
      `${process.env.BACKEND_URL}/users/signup`,
      user,
      {
        headers: {
          'Content-Type': 'application/json',
        },
      }
    )
    if (response.data.status === 'success') {
      setToast('ثبت نام با موفقیت انجام شد.', 'success', 1000)
    }
    setTimeout(() => {
      Router.push('/login')
    }, 1000)
  } catch (err) {
    const error = err.response.data
    if (error.message === 'User has already registered') {
      setToast(
        'ایمیل وارد شده در حال حاضر در سیستم وجود دارد. لطفا از یک ایمیل دیگر استفاده کنید.',
        'warn'
      )
    }
  }
}

// ! LOGIN USER
export const loginUser = (user, route = true) => async dispatch => {
  try {
    const { data } = await axios.post(
      `${process.env.BACKEND_URL}/users/login`,
      user,
      {
        headers: {
          'Content-Type': 'application/json',
        },
      }
    )
    const payload = data.data.user
    dispatch({ type: AUTHENTICATE, payload })
    if (data.status === 'success' && route) {
      Router.push('/')
    }
  } catch (err) {
    const error = err.response.data
    if (error.message === 'Incorrect email or password') {
      setToast('ایمیل یا رمز ورود اشتباه است', 'fail')
    }
  }
}

// ! FORGOT PASSWORD
export const forgotPassword = email => async dispatch => {
  try {
    const response = await axios.post(
      `${process.env.BACKEND_URL}/users/forgotPassword`,
      {
        email,
      }
    )
    if (response.data.status === 'success') {
      setToast(
        `یک لینک برای بازیابی رمز عبور به ایمیل‌ شما ارسال گردید.
        لطفاً به ایمیل خود مراجعه کرده و بر روی لینک ارسال شده کلیک نمایید.
        `,
        'success',
        10000
      )
    }
  } catch (err) {
    const error = err.response.data
    if (error.message === 'There is no user with this email') {
      setToast('کاربری با این ایمیل پیدا نشد.', 'fail')
    }
  }
}

// ! RESET PASSWORD
export const resetPassword = (password, token) => async dispatch => {
  try {
    const response = await axios.patch(
      ` ${process.env.BACKEND_URL}/users/resetPassword/${token}`,
      { password }
    )
    if (response.data.status === 'success') {
      setToast('رمز عبور با موفقیت تغییر یافت', 'success')
    }
    Router.push('/login')
  } catch (err) {
    const error = err.response.data
    if (error.message === 'Token is invalid or has expired') {
      setToast(
        'زمان تغییر رمز عبور منقضی شده است. لطفا مجددا درخواست نمایید',
        'fail'
      )
    }
  }
}

// ! LOGOUT USER
export const logoutUser = () => async dispatch => {
  // remove cookies
  if (process.browser) {
    // can't remove if it is httpOnly
    // workaround: set a txp cookie to check on server side and destroy httpOnly cookie
    Cookies.set('txp', 'log_out')
    // deauthenticate
    dispatch({ type: DEAUTHENTICATE })
  }
}

// ! GET USER
export const getUser = (cookies = {}, ctx) => async dispatch => {
  try {
    // in case there is token
    const cookie = JSON.stringify(cookies)
    //typeof cookies = object
    const {
      data: {
        data: { data },
      },
    } = await axios.get(`${serverRuntimeConfig.BACKEND_URL}/users/me`, {
      headers: {
        Cookie: cookie,
      },
    })
    const currentUser = data
    return dispatch({ type: AUTHENTICATE, payload: currentUser })
  } catch (err) {
    // in case there is token but not valid (manipulated)
    console.log('User not found')
    nookies.destroy(ctx, 'token')
  }
}

// ! UPDATE USER PROFILE
export const updateUserProfile = profile => async dispatch => {
  try {
    const response = await axios.patch(
      `${process.env.BACKEND_URL}/users/updateMe`,
      profile
    )
    if (response.data.status === 'success') {
      setToast('پروفایل شما با موفقیت به روز رسانی شد', 'success')
    }
  } catch (err) {
    setToast('لطفا مجدد تلاش کنید.', 'fail')
  }
}

// ! CHANGE USER PASSWORD
export const changePassword = password => async dispatch => {
  try {
    const response = await axios.patch(
      `${process.env.BACKEND_URL}/users/updateMyPassword`,
      password
    )
    if (response.data.status === 'success') {
      setToast('رمز ورود شما با موفقیت تغییر یافت.', 'success', 3000)
    }
    setTimeout(() => {
      Router.push('/profile')
    }, 3000)
  } catch (err) {
    const error = err.response.data
    if (error.message === 'Your current password is wrong') {
      setToast('رمز ورود فعلی شما اشتباه است. مجدد وارد شوید.', 'fail')

      // remove cookies
      if (process.browser) {
        Cookies.set('txp', 'log_out')
        dispatch({ type: DEAUTHENTICATE })

        setTimeout(() => {
          Router.push('/login')
        }, 2000)
      }
    }
  }
}

// ! DEAUTHENTICATE
export const deauthenticate = () => dispatch => {
  // in case there is a token but manipulated:
  return dispatch({ type: DEAUTHENTICATE })
}

// ! CHECK SERVER SIDE COOKIE
export const checkServerSideCookie = async ctx => {
  // only checks server side cookie
  if (ctx.req) {
    const cookies = nookies.get(ctx)
    // after logging out to destroy httpOnly cookie
    // if user login immediately, there is still exp because the page is not refreshed
    // workaround: change login from <Link> to <a> tag to make a fresh page on every login
    if (cookies.txp) {
      nookies.destroy(ctx, 'token')
      nookies.destroy(ctx, 'txp')
    }

    if (cookies.token && !cookies.txp) {
      await ctx.store.dispatch(getUser(cookies, ctx))
      await ctx.store.dispatch(fetchMessages(cookies, ctx))
    } else {
      // no cookies.token || there is cookies.token & there is cookies.txp
      await ctx.store.dispatch(deauthenticate())
    }
  }
}
