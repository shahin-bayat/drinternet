import { useState } from 'react'
import styled from 'styled-components'
import setToast from '../../utils/toast'
import axios from 'axios'
import Container from '../../components/styles/Container'
import Grid from '../../components/styles/Grid'
import Button from '../../components/styles/Button'
import Form from '../../components/styles/Form'
import InputGroup from '../../components/styles/InputGroup'

import map from '../../images/map.png'
import ContactUsSVG from '../../images/SVG/phone.svg'
import EmailSVG from '../../images/SVG/email.svg'
import LocationSVG from '../../images/SVG/location.svg'

const ContactUs_ = styled.section`
  min-height: 100vh;

  h2 {
    font-size: 1.8rem;
    color: ${props => props.theme.color_grey};
    margin-top: 5rem;
  }
`
const ContactForm = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 2rem;
  border-left: 1px solid ${props => props.theme.color_grey_light};

  @media (max-width: ${props => props.theme.bp_medium}) {
    border-left: none;
  }
`
const Address = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 2rem;

  img.map {
    height: auto;
    width: 50rem;
    border: 1px solid ${props => props.theme.color_primary};

    @media (max-width: ${props => props.theme.bp_medium}) {
      width: 40rem;
    }
    @media (max-width: ${props => props.theme.bp_small}) {
      width: 100%;
    }
  }
`
const Feature = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  margin: 1rem 0;

  svg {
    width: 3rem;
    height: auto;
    margin-left: 1rem;
    fill: ${props => props.theme.color_primary};
  }
  p.feature {
    margin-left: 2rem;
  }

  p.feature__property {
    flex: 1;
  }
`

const ContactUs = () => {
  const [contact, setContact] = useState({
    name: '',
    phone: '',
    subject: '',
    message: '',
  })
  const { name, phone, subject, message } = contact
  const [loading, setLoading] = useState(false)

  const handleSubmit = async e => {
    try {
      e.preventDefault()
      setLoading(true)

      let additional = {
        subject,
        message,
      }
      additional = JSON.stringify(additional)

      const mail = {
        name,
        service: 'contact-us-page',
        phone,
        additional,
      }
      await axios.post(`${process.env.BACKEND_URL}/mails`, mail)
      setToast(
        'پیام شما با موفقیت ارسال شد. کارشناسان ما در اسرع وقت با شما تماس خواهند گرفت.',
        'success'
      )
      setLoading(false)
      setContact({
        name: '',
        phone: '',
        subject: '',
        message: '',
      })
    } catch (error) { }
  }
  const handleChange = e => {
    setContact({ ...contact, [e.target.name]: e.target.value })
  }
  return (
    <ContactUs_>
      <Container>
        <Grid
          templateColumns='1fr 1fr'
          style={{
            marginTop: '10rem',
            padding: '0 2rem',
            marginBottom: '4rem',
          }}
          alignItems='start'
        >
          {/* FORM */}
          <ContactForm>
            <h2>فرم ارسال پیام</h2>
            <Form onSubmit={handleSubmit}>
              <InputGroup>
                <input
                  type='text'
                  name='name'
                  id='name'
                  onChange={handleChange}
                  value={contact.name}
                  required
                />
                <label htmlFor='name'>نام و نام خانوادگی / نام شرکت</label>
              </InputGroup>
              <InputGroup direction='ltr'>
                <input
                  type='text'
                  name='phone'
                  id='phone'
                  onChange={handleChange}
                  value={contact.phone}
                  minLength={10}
                />
                <label htmlFor='phone'>
                  تلفن<span>(به همراه پیش شماره)</span>
                </label>
              </InputGroup>
              <InputGroup>
                <input
                  type='text'
                  name='subject'
                  id='subject'
                  onChange={handleChange}
                  value={contact.subject}
                  minLength={10}
                  required
                />
                <label htmlFor='subject'>موضوع</label>
              </InputGroup>
              <InputGroup minHeight='20rem'>
                <textarea
                  label='message'
                  name='message'
                  id='message'
                  onChange={handleChange}
                  value={contact.message}
                  required
                ></textarea>
                <label htmlFor='message'>متن</label>
              </InputGroup>

              <Button primary round disabled={loading}>
                ثبت درخواست
              </Button>
            </Form>
          </ContactForm>

          {/* CONTACT */}
          <Address>
            <h2>دکتر اینترنت</h2>
            <div>
              <Feature>
                <ContactUsSVG />
                <p className='feature'>تلفن:</p>
                <p className='feature__property'>021-91033501</p>
              </Feature>
              <Feature>
                <EmailSVG />
                <p className='feature'>ایمیل:</p>
                <p className='feature__property'>support@drinternet.ir</p>
              </Feature>
              <Feature>
                <LocationSVG />
                <p className='feature'>آدرس:</p>
                <p className='feature__property'>
                  تهران، شهرک غرب، بلوار فرحزادی، خیابان شهید حافظی، پلاک 10
                </p>
              </Feature>
            </div>
            <img className='map' src={map} />
          </Address>
        </Grid>
      </Container>
    </ContactUs_>
  )
}

export default ContactUs
