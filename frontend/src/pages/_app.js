import React from 'react'
import { Provider } from 'react-redux'
import { makeStore } from '../redux/store'
import withRedux from 'next-redux-wrapper'
import { checkServerSideCookie } from '../redux/actions/authActions'
import '../Global/fontiran.css'
// import "../Global/slick-theme.css"
// import "../Global/slick.css"
import '../Global/toast.css'
import '../Global/DatePick.css'
import '../Global/next-editor.css'
import '../Global/nprogress.css'

import '../../node_modules/react-toastify/dist/ReactToastify.css'
import '../../node_modules/react-modern-calendar-datepicker/lib/DatePicker.css'
import '../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css'

import Page from './../components/layout/Page'

const MyApp = ({ Component, pageProps, store, pagePath }) => {
  return (
    <Provider store={store}>
      <Page pagePath={pagePath}>
        <Component {...pageProps} />
      </Page>
    </Provider>
  )
}

MyApp.getInitialProps = async ({ Component, ctx }) => {
  await checkServerSideCookie(ctx)

  const pagePath = ctx.asPath

  const pageProps = Component.getInitialProps
    ? await Component.getInitialProps(ctx)
    : {}

  return { pageProps, pagePath }
}

export default withRedux(makeStore)(MyApp)
