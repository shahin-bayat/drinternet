import Head from 'next/head'
import styled from 'styled-components'
import { Fragment } from 'react'
import Router from 'next/router'

import Header from '../../components/styles/Header'
import Container from '../../components/styles/Container'
import Grid from '../../components/styles/Grid'
import Button from '../../components/styles/Button'
import Card from '../../components/styles/Card'
import TextBox from '../../components/styles/TextBoxServices'

import HomeSVG from '../../images/SVG/home.svg'
import SignalTowerSVG from '../../images/SVG/signal-tower.svg'
import AntennaSVG from '../../images/SVG/antenna.svg'
import BuildingSVG from '../../images/SVG/tower.svg'
import ShieldSVG from '../../images/SVG/shield.svg'
import CameraSVG from '../../images/SVG/camera.svg'
import WebSVG from '../../images/SVG/web-dev.svg'
import AppSVG from '../../images/SVG/app-dev.svg'
import TelephoneSVG from '../../images/SVG/telephone.svg'
import VoipSVG from '../../images/SVG/voip2.svg'
import HeroImageSVG from '../../images/SVG/hero_img.svg'

const Services_ = styled.section`
  background-color: ${props => props.theme.color_grey_light};
`
const Service = styled.div`
  margin: 3rem 0;
  padding: 0 2rem;

  div div h2 {
    @media (max-width: ${props => props.theme.bp_medium}) {
      align-self: center;
    }
  }

  div div p {
    @media (max-width: ${props => props.theme.bp_medium}) {
      text-align: center;
    }
  }
`

const Services = () => {
  return (
    <Fragment>
      <Head>
        <title>خدمات</title>
      </Head>

      {/* HEADER */}
      <Header light>
        <Container>
          <Grid
            templateColumns='1fr 60%'
            bp_medium='1fr'
            style={{ padding: '0 2rem' }}
          >
            <TextBox>
              <h1>خدمات</h1>
              <p>
                با خدمات ویژه‌ی وب سایت دکتر اینترنت می‌توانید گستره‌ی وسیعی از
                نیاز‌های خود را از ما با بهترین سرویس‌دهی، پشتیبانی و مناسب‌ترین
                قیمت تامین نمایید. تلاش ما بر این است که بتوانيم نقشي سازنده در
                زندگي روزمره IT شما عهده‌دار باشيم.
              </p>
              <Button
                secondary
                round
                shadow
                moving
                onClick={e => Router.push('/services#services')}
              >
                بیشتر بدانید
              </Button>
            </TextBox>
            <HeroImageSVG className='hero__image' />
          </Grid>
        </Container>
      </Header>

      {/* SERVICES */}
      <Services_ id='services'>
        <Container>
          <Grid templateColumns='1fr' gridGap='0'>
            {/* INTERNET */}
            <Service id='internet'>
              <Grid templateColumns='1fr 2fr' bp_medium='1fr'>
                <Card alignItems='start' noHover>
                  <h2>اینترنت و پهنای باند</h2>
                  <p>
                    ارائه‌ی خدمات ویژه‌ی اینترنت به کاربران خانگی و شرکت‌ها و
                    سازمان‌ها شامل سرویس های +ADSL2 ویژه‌ی کاربران خانگی،
                    سرویس‌های ویژه‌ی برج‌ها و مجتمع‌های مسکونی و تجاری، سرویس
                    های پهنای باند اینترنت اختصاصی ویژه‌ی شرکت‌ها و سازمان‌ها و
                    سرویس‌های ویژه‌ی برقراری ارتباط بین دفاتر و شعب سازمان ها و
                    شرکت ها(ترانزیت)
                  </p>
                </Card>
                <Grid templateColumns='1fr 1fr' alignContent='stretch'>
                  <Card
                    justifyContent='center'
                    onClick={e =>
                      Router.push('/services/internet/adsl').then(() =>
                        window.scrollTo(0, 0)
                      )
                    }
                  >
                    <HomeSVG />
                    <h3>اینترنت +ADSL2</h3>
                    <p className='flash-text'>
                      ارائه‌ی سرویس‌های ویژه به کاربران خانگی
                    </p>
                  </Card>
                  <Card
                    justifyContent='center'
                    onClick={e =>
                      Router.push('/services/internet/bandwidth').then(() =>
                        window.scrollTo(0, 0)
                      )
                    }
                  >
                    <AntennaSVG />
                    <h3>پهنای باند ویژه شرکت‌ها</h3>
                    <p className='flash-text'>
                      ارائه‌ی خدمات پهنای باند ویژه‌ی سازمان‌ها و شرکت‌ها
                    </p>
                  </Card>
                  <Card
                    justifyContent='center'
                    onClick={e => Router.push('/services/internet#towers')}
                  >
                    <BuildingSVG />
                    <h3>خدمات ويژه‌ی برج‌ها و مجتمع‌ها</h3>
                    <p className='flash-text'>
                      ارائه‌ی سرویس‌های ویژه به برج‌ها و مجتمع‌های مسکونی
                    </p>
                  </Card>
                  <Card
                    justifyContent='center'
                    onClick={e => Router.push('/services/internet#transit')}
                  >
                    <SignalTowerSVG />
                    <h3>ارتباط بین شعب (ترانزیت)</h3>
                    <p className='flash-text'>
                      ارائه‌ی خدمات ویژه‌ی ارتباط بین شعب به شرکت‌ها
                    </p>
                  </Card>
                </Grid>
              </Grid>
            </Service>
            {/* WEB DEV */}
            <Service id='web'>
              <Grid templateColumns='1fr 2fr' bp_medium='1fr'>
                <Card alignItems='start' noHover>
                  <h2>طراحی و توسعه وب سایت و اپلیکیشن موبایل</h2>
                  <p>
                    خدمات ویژه‌ی طراحی وب سایت و اپلیکیشن موبایل متناسب با نیاز
                    و بودجه‌ی شما با بهره‌گیری از بروز‌ترین روش‌ها و
                    تکنولوژی‌های حوزه‌ی وب
                  </p>
                </Card>
                <Grid templateColumns='1fr 1fr' alignContent='stretch'>
                  <Card
                    justifyContent='center'
                    onClick={e =>
                      Router.push('/services/development/website').then(() =>
                        window.scrollTo(0, 0)
                      )
                    }
                  >
                    <WebSVG />
                    <h3>طراحی وب سایت</h3>
                    <p className='flash-text'>
                      ساخت و طراحی وب سایت با بهره‌گیری از جدیدترین روش‌ها
                    </p>
                  </Card>
                  <Card
                    justifyContent='center'
                    onClick={e =>
                      Router.push('/services/development/app').then(() =>
                        window.scrollTo(0, 0)
                      )
                    }
                  >
                    <AppSVG />
                    <h3>طراحی اپلیکیشن</h3>
                    <p className='flash-text'>
                      ساخت و طراحی اپلیکیشن موبایل با بهره‌گیری از جدیدترین
                      روش‌ها
                    </p>
                  </Card>
                </Grid>
              </Grid>
            </Service>

            {/* SECURITY */}
            <Service id='security'>
              <Grid templateColumns='1fr 2fr' bp_medium='1fr'>
                <Card alignItems='start' noHover>
                  <h2>امنیت و نظارت</h2>
                  <p>
                    ارائه‌ی خدمات مشاوره و راه‌اندازی سیستم‌های امنیت شبکه و
                    مشاوره و نصب سیستم‌های نظارتی و دوربین‌های مداربسته به
                    شرکت‌ها و سازمان‌ها
                  </p>
                </Card>
                <Grid templateColumns='1fr 1fr' alignContent='stretch'>
                  <Card
                    justifyContent='center'
                    onClick={e => Router.push('/services/security#antivirus')}
                  >
                    <ShieldSVG />
                    <h3>مشاوره و راه‌اندازی امنیت شبکه</h3>
                    <p className='flash-text'>
                      بررسی شبکه از نظر آسیب‌پذیری‌های امنیتی و ارائه‌ی
                      راهکارهای متناسب
                    </p>
                  </Card>
                  <Card
                    justifyContent='center'
                    onClick={e =>
                      Router.push('/services/security#security_camera')
                    }
                  >
                    <CameraSVG />
                    <h3>سیستم‌های نظارتی و دوربین‌های مدار بسته</h3>
                    <p className='flash-text'>
                      طراحی، مشاوره، تأمین تجهیزات و راه‌اندازی دوربین‌های
                      امنیتی مدار بسته
                    </p>
                  </Card>
                </Grid>
              </Grid>
            </Service>
            {/* VOIP */}
            <Service id='phone'>
              <Grid templateColumns='1fr 2fr' bp_medium='1fr'>
                <Card alignItems='start' noHover>
                  <h2>تلفن اینترنتی</h2>
                  <p>
                    ارائه‌ی خطوط ویژه NGN با کیفیت و ویژگی‌های فنی مناسب به
                    شرکت‌ها و کاربران خانگی و سرویس‌های تماس بین‌الملل به عموم
                    شهروندان
                  </p>
                </Card>
                <Grid templateColumns='1fr 1fr' alignContent='stretch'>
                  <Card
                    justifyContent='center'
                    onClick={e => Router.push('/services/phone#voip')}
                  >
                    <TelephoneSVG />
                    <h3>تماس بین‌الملل</h3>
                    <p className='flash-text'>
                      ارائه‌ی سرویس مکالمه و تلفن بین‌الملل با کیفیت به مشتریان
                    </p>
                  </Card>
                  <Card
                    justifyContent='center'
                    onClick={e => Router.push('/services/phone#ngn')}
                  >
                    <VoipSVG />
                    <h3>تلفن ثابت (NGN)</h3>
                    <p className='flash-text'>
                      ارائه‌ی خطوط تلفن ثابت ویژه‌ی شرکت‌های بزرگ، برج‌ها و
                      مجتمع‌های مسکونی
                    </p>
                  </Card>
                </Grid>
              </Grid>
            </Service>
          </Grid>
        </Container>
      </Services_>
    </Fragment>
  )
}

Services.getInitialProps = async ctx => {}
export default Services
