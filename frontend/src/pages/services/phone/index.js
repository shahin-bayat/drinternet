import { Fragment } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { openModal, closeModal } from '../../../redux/actions/modalActions'
import NgnModal from '../../../components/NgnModal'
import PhoneModal from '../../../components/PhoneModal'
import TextBox from '../../../components/styles/TextBoxServices'
import Header from '../../../components/styles/Header'
import Container from '../../../components/styles/Container'
import Grid from '../../../components/styles/Grid'
import Callout from '../../../components/styles/Callout'
import Button from '../../../components/styles/Button'
import SectionHeader from '../../../components/styles/SectionHeader'
import img from '../../../images/blue-bg.png'
import img2 from '../../../images/green-bg.png'
import PhoneGlobeSVG from '../../../images/SVG/phone-globe.svg'
import NoWifiSVG from '../../../images/SVG/no-wifi.svg'
import QuickCallSVG from '../../../images/SVG/quick-call.svg'
import DiscountSVG from '../../../images/SVG/discount.svg'
import PinCodeSVG from '../../../images/SVG/pin-code.svg'
import MonitorSVG from '../../../images/SVG/monitor.svg'
import WarningSVG from '../../../images/SVG/warning.svg'
import TransferSVG from '../../../images/SVG/transfer.svg'
import CableSVG from '../../../images/SVG/cable.svg'
import VideoConferenceSVG from '../../../images/SVG/video-conference.svg'
import MicrophoneSVG from '../../../images/SVG/microphone.svg'
import NetworkCenterSVG from '../../../images/SVG/network-center.svg'
import NgnSVG from '../../../images/SVG/ngn.svg'

const index = ({ openModal, closeModal }) => {
  return (
    <Fragment>
      {/* VoIP */}
      <section id='voip'>
        <Header background='#fff' img={img} img2={img} light>
          <Container alignItems='center'>
            <Grid
              templateColumns='1fr 60%'
              bp_medium='1fr'
              style={{ padding: '0 4rem', marginTop: '7rem' }}
            >
              <TextBox>
                <h1>تماس بین‌الملل</h1>
                <p>
                  تماس با خارج از کشور را با ما و با بروز‌ترین تکنولوژی‌ها تجربه
                  نمایید. با خدمات تلفن بین‌الملل ما امکان تماس با کلیه نقاط
                  جهان با بالاترین کیفیت ممکن و مناسب‌ترین قیمت را خواهید داشت.
                  کافیست برای دریافت سرویس آزمایشی با ما در تماس باشید.
                </p>
                <Button
                  secondary
                  round
                  shadow
                  moving
                  onClick={e => openModal('phone')}
                >
                  درخواست اطلاعات اولیه
                </Button>
              </TextBox>
              <PhoneGlobeSVG className='hero__image' height='40rem' />
            </Grid>
          </Container>
        </Header>
        <div>
          <PhoneModal openModal={openModal} closeModal={closeModal} />
          <Container alignItems='center'>
            <SectionHeader>
              <h2>مزایا و ویژگی‌ها</h2>
              <Grid
                templateColumns='repeat(4, minmax(20rem, 1fr))'
                bp_medium='repeat(2, minmax(20rem, 1fr))'
                style={{ margin: '6rem 0' }}
              >
                <Callout>
                  <NoWifiSVG />
                  <p>
                    تماس بدون نیاز به اینترنت یا کامپیوتر و تنها با تلفن ثابت یا
                    موبایل
                  </p>
                </Callout>
                <Callout>
                  <QuickCallSVG />
                  <p>شماره گیری سریع با تنظیم شماره‌های 1 تا 19</p>
                </Callout>
                <Callout>
                  <DiscountSVG />
                  <p>تخفیف دریافت اعتیار زمانی بیشتر با خرید هر کارت جدید</p>
                </Callout>
                <Callout>
                  <PinCodeSVG />
                  <p>
                    افزودن شماره تماس همراه یا منزل خود به لیست ID Caller بدون
                    نیاز به پین‌کد
                  </p>
                </Callout>
              </Grid>
            </SectionHeader>
          </Container>
        </div>
      </section>

      {/* NGN */}
      <section id='ngn'>
        <Header background='#fff' img={img2} img2={img2} light>
          <Container alignItems='center'>
            <Grid
              templateColumns='1fr 60%'
              bp_medium='1fr'
              style={{ padding: '0 4rem', marginTop: '7rem' }}
            >
              <TextBox>
                <h1>تلفن ثابت (NGN)</h1>
                <p>
                  شبکه‌ نسل آینده (Next Generation Network) راه‌حلی برای کنترل
                  رقابت فشرده و افزايش حجم ترافيك ديتا در سرویس‌های مخابراتی
                  است. با بهره‌گیری از این امکان، مشکلاتی نظیر جابجایی خطوط تلفن
                  به دلیل تغییر محل به طور کامل مرتفع می‌شود و کاربران سازمانی و
                  خانگی می‌توانند فارغ از مکان فیزیکی محل کار و منزل، شماره‌ی
                  خود را به هر نقطه که مورد نظرشان است انتقال دهند.
                </p>
                <Button
                  secondary
                  round
                  shadow
                  moving
                  onClick={e => openModal('ngn')}
                >
                  درخواست بررسی اولیه
                </Button>
              </TextBox>
              <NgnSVG className='hero__image' height='40rem' />
            </Grid>
          </Container>
        </Header>
        <NgnModal openModal={openModal} closeModal={closeModal} />
        <div>
          <Container alignItems='center'>
            <SectionHeader>
              <h2>مزایا و ویژگی‌ها</h2>
              <Grid
                templateColumns='repeat(4, minmax(20rem, 1fr))'
                bp_medium='repeat(2, minmax(20rem, 1fr))'
                style={{ margin: '6rem 0' }}
              >
                <Callout>
                  <QuickCallSVG />
                  <p>
                    انتخاب شماره با پیش شماره‌های شهری و کشوری 9108xxxx 9168xxxx
                  </p>
                </Callout>
                <Callout>
                  <TransferSVG />
                  <p>
                    ایجاد کانال ورودی و خروجی به تعداد مورد نیاز برروی یک خط
                    تلفن NGN
                  </p>
                </Callout>
                <Callout>
                  <CableSVG />
                  <p>
                    قابلیت انتقال بدون وابستگی به مخابرات و شبکه‌ی مسی یا فیبر
                    نوری
                  </p>
                </Callout>
                <Callout>
                  <DiscountSVG />
                  <p>
                    کاهش هزینه‌های تماس بسته به میزان استفاده از 10 تا 20 درصد
                  </p>
                </Callout>
                <Callout>
                  <NetworkCenterSVG />
                  <p>
                    قابلیت راه‌اندازی خطوط بر روی مرکز تلفن تحت شبکه (Issabel)
                  </p>
                </Callout>
                <Callout>
                  <VideoConferenceSVG />
                  <p>امکان استفاده در کنفرانس‌های صوتی و تصویری</p>
                </Callout>
                <Callout>
                  <MicrophoneSVG />
                  <p>امکان ضبط مکالمات ورودی، خروجی و داخلی</p>
                </Callout>
                <Callout>
                  <MonitorSVG />
                  <p>قابلیت اتصال به CRM، مانیتورینگ خطوط و داخلی‌ها</p>
                </Callout>
              </Grid>
            </SectionHeader>
          </Container>
        </div>
      </section>
    </Fragment>
  )
}

index.propTypes = {
  openModal: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
}

export default connect(null, { openModal, closeModal })(index)
