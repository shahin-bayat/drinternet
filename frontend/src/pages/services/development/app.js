import { Fragment } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { openModal, closeModal } from '../../../redux/actions/modalActions'
import AppModal from '../../../components/AppModal'
import styled from 'styled-components'

import Header from '../../../components/styles/Header'
import Container from '../../../components/styles/Container'
import Grid from '../../../components/styles/Grid'
import Callout from '../../../components/styles/Callout'
import Button from '../../../components/styles/Button'
import SectionHeader from '../../../components/styles/SectionHeader'
import img from '../../../images/app-bg.png'
import AppSVG from '../../../images/SVG/mobile-app.svg'
import TextBox from '../../../components/styles/TextBoxServices'

import UiSVG from '../../../images/SVG/ui.svg'
import UxSVG from '../../../images/SVG/ux.svg'
import ApptechSVG from '../../../images/SVG/app-tech.svg'
import IosAndroidSVG from '../../../images/SVG/ios-android.svg'
import AppSiteSVG from '../../../images/SVG/app-site.svg'
import CodingSVG from '../../../images/SVG/coding.svg'
import MobileAppSVG from '../../../images/SVG/mobile-application.svg'
import AppWireframeSVG from '../../../images/SVG/app-wireframe.svg'
import AppDesignSVG from '../../../images/SVG/app-design.svg'
import AppImplementSVG from '../../../images/SVG/app-implement.svg'

const app = ({ openModal, closeModal }) => {
  return (
    <Fragment>
      <section id='app'>
        <Header background='#fff' img={img} img2={img} primary>
          <Container>
            <Grid
              templateColumns='1fr 60%'
              bp_medium='1fr'
              style={{ padding: '0 4rem', marginTop: '7rem' }}
            >
              <TextBox>
                <h1>طراحی اپلیکیشن</h1>
                <p>
                  کسب و کارهای مختلف همواره به دنبال راهی هستند تا بتوانند
                  ارتباطی پایدار با مشتریان خود داشته باشند. با گسترش استفاده از
                  تلفن‌های هوشمند، داشتن یک اپلیکیشن قدرتمند این امکان را به شما
                  می‌دهد تا یک پل ارتباطی قوی بین مشتریان و کسب و کارتان ایجاد
                  کنید. گروه توسعه دهندگان دکتر اینترنت این امکان را به شما
                  می‌دهند تا بتوانید با جدیدترین تکنولوژی‌های حوزه وب این مسیر
                  را هموار سازید.
                </p>
                <Button
                  secondary
                  round
                  shadow
                  moving
                  onClick={e => openModal('app')}
                >
                  تماس با متخصصین ما
                </Button>
              </TextBox>
              <AppSVG className='hero__image' />
            </Grid>
          </Container>
        </Header>
        <div style={{ borderBottom: '1px solid #e0e0e2' }}>
          <Container alignItems='center'>
            <SectionHeader>
              <h2>مزایا و ویژگی‌ها</h2>
              <p>
                تیم حرفه‌ای دکتر اینترنت شما را در تمام مراحل ساخت اپلیکیشن از
                ایده‌پردازی و دیجیتال مارکتینگ تا طراحی و اجرای اپلیکیشن یاری
                می‌کنند تا بتوانید تجربه‌ای دلچسب به مشتریان خود ارائه کنید. در
                زیر به پاره‌ای از این موارد اشاره می‌شود.
              </p>
              <FeaturesLayout>
                <Grid
                  templateColumns='repeat(3, minmax(20rem, 1fr))'
                  alignContent='center'
                  gridGap='4rem'
                >
                  <Callout big>
                    <UiSVG />
                    <p>طراحی زیبا با بهره‌گیری از طراحان با سابقه</p>
                  </Callout>
                  <Callout big>
                    <UxSVG />
                    <p>
                      ایجاد تجربه کاربری لذت بخش با بهره‌گیری از متخصصین ux
                      design
                    </p>
                  </Callout>
                  <Callout big>
                    <ApptechSVG />
                    <p>
                      استفاده از جدیدترین تکنولوژی‌های روز دنیا نظیر React
                      Native و Flutter
                    </p>
                  </Callout>
                  <Callout big>
                    <IosAndroidSVG />
                    <p>
                      امکان داشتن خروجی هم‌زمان اپلیکیشن در سیستم عامل ios و
                      android با یک کدنویسی
                    </p>
                  </Callout>
                  <Callout big>
                    <AppSiteSVG />
                    <p>ارتباط هماهنگ اپلیکیشن با وب سایت</p>
                  </Callout>
                  <Callout big>
                    <CodingSVG />
                    <p>توسعه اختصاصی api برای اپلیکیشن و وب سایت</p>
                  </Callout>
                </Grid>
                {/* <MobileAppSVG style={{ height: '30rem', width: 'auto' }} /> */}
              </FeaturesLayout>
            </SectionHeader>
          </Container>
        </div>
        <AppModal openModal={openModal} closeModal={closeModal} />
        <div style={{ margin: '5rem 0' }}>
          <Container alignItems='center'>
            <h2 style={{ fontSize: '2.5rem' }}>مراحل کار</h2>
            <p>ما در 3 مرحله اپلیکیشن شما را طراحی می‌کنیم.</p>

            <Grid
              templateColumns='repeat(3, minmax(20rem, 1fr))'
              style={{ margin: '3rem 0' }}
            >
              <Callout big>
                <AppWireframeSVG />
                <h4>1- ایجاد وایرفریم اولیه</h4>
                <p>
                  در این مرحله یک ماکت ابتدایی مطابق با استاندارد‌های طراحی بر
                  اساس ایده اولیه شما ایجاد می‌شود
                </p>
              </Callout>
              <Callout big>
                <AppDesignSVG />
                <h4>2- طراحی رابط کاربری</h4>
                <p>
                  در این مرحله بر اساس وایرفریم اولیه، رابط کاربری طراحی و بعد
                  از تأیید اولیه وارد مرحله کد‌نویسی می‌شود.
                </p>
              </Callout>
              <Callout big>
                <AppImplementSVG />
                <h4>3- پیاده‌سازی</h4>
                <p>
                  در این مرحله با داشتن طراحی گرافیکی و رابط کاربری، اپلیکیشن کد
                  نویسی و api لازم تهیه میشود.
                </p>
              </Callout>
            </Grid>
          </Container>
        </div>

        <div style={{ margin: '5rem 0' }}>
          <Container alignItems='center'>
            <Grid
              templateColumns='1fr 30%'
              gridGap='5rem'
              alignItems='center'
              bp_medium='1fr'
              style={{ padding: '0 4rem' }}
            >
              <TextBox
                justifyItems='start'
                alignContent='center'
                templateColumns='1fr'
                gridGap='3rem'
                nomedia
              >
                <h3 style={{ color: '#5a0fc8' }}>
                  آیا راهکار مقرون به صرفه و بروزتری هم وجود دارد؟
                </h3>
                <p style={{ textAlign: 'justify' }}>
                  بله! یکی از راهکار‌های گروه دکتر اینترنت برای کاهش هزینه‌ها و
                  همزمان افزایش بهره‌وری برای کسانی که به دنبال داشتن یک
                  اپلیکیشن برای وب سایت خود هستند، استفاده از تکنولوژی PWA است.
                  PWAها در‌واقع همان اپلیکیشن‌های سایت-محور (web app) هستند که
                  از api وب سایت شما استفاده می‌کنند تا یک تجربه‌ی کاربری مشابه
                  برنامه‌های بومی را به کاربر ارائه دهند. با این روش دیگر نیاز
                  به نصب اپلیکیشن و مشکلات تحریم استورها برطرف می‌شود.
                </p>
                <p>برای کسب اطلاعات بیشتر با متخصصین ما تماس بگیرید.</p>
              </TextBox>
              <img
                src={require('../../../images/pwa.png')}
                alt='progressive web apps'
                style={{ width: '100%', height: '15rem' }}
              />
            </Grid>
          </Container>
        </div>
      </section>
    </Fragment>
  )
}

const FeaturesLayout = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 4rem;
  align-content: start;
  margin: 6rem 0;

  & > svg {
    justify-self: center;
  }

  @media (max-width: ${props => props.theme.medium}) {
  }
`

app.propTypes = {
  openModal: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
}

export default connect(null, { openModal, closeModal })(app)
