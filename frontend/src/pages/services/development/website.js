import { Fragment } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { openModal, closeModal } from '../../../redux/actions/modalActions'
import WebsiteModal from '../../../components/WebsiteModal'
import styled from 'styled-components'

import Header from '../../../components/styles/Header'
import Container from '../../../components/styles/Container'
import Grid from '../../../components/styles/Grid'
import Callout from '../../../components/styles/Callout'
import Button from '../../../components/styles/Button'
import SectionHeader from '../../../components/styles/SectionHeader'
import img from '../../../images/web-bg.png'
import WebsiteSVG from '../../../images/SVG/website.svg'
import TextBox from '../../../components/styles/TextBoxServices'

import ResponsiveSVG from '../../../images/SVG/responsive.svg'
import SeoSVG from '../../../images/SVG/seo.svg'
import DedicatedSVG from '../../../images/SVG/dev-dedicated.svg'
import TechSVG from '../../../images/SVG/web-tech.svg'
import WebAppSVG from '../../../images/SVG/laptop.svg'
import AppWireframeSVG from '../../../images/SVG/app-wireframe.svg'
import AppDesignSVG from '../../../images/SVG/app-design.svg'
import WebImplementSVG from '../../../images/SVG/web-coding.svg'

const website = ({ openModal, closeModal }) => {
  return (
    <Fragment>
      <section id='app'>
        <Header background='#fff' img={img} img2={img} light>
          <Container>
            <Grid
              templateColumns='1fr 60%'
              bp_medium='1fr'
              style={{ padding: '0 4rem', marginTop: '7rem' }}
            >
              <TextBox>
                <h1>طراحی وب سایت</h1>
                <p>
                  داشتن یک وب سایت زیبا با امکانات فنی لازم و قابلیت توسعه‌پذیری
                  یکی از مهم‌ترین نیازهای هر کسب و کار در حال رشد است. تیم طراحی
                  وب سایت دکتر اینترنت با بهره‌گیری از جدیدترین تکنولوژی‌های روز
                  در حوزه‌ی برنامه‌نویسی وب سایت در کنار شماست تا تجربه‌ی داشتن
                  سایتی کارآمد را برایتان دلچسب کند.
                </p>
                <Button
                  secondary
                  round
                  shadow
                  moving
                  onClick={e => openModal('website')}
                >
                  مشاوره با متخصصین ما
                </Button>
              </TextBox>
              <WebsiteSVG className='hero__image' />
            </Grid>
          </Container>
        </Header>
        <div style={{ borderBottom: '1px solid #e0e0e2' }}>
          <Container alignItems='center'>
            <SectionHeader>
              <h2>مزایا و ویژگی‌ها</h2>
              <p>
                تیم حرفه‌ای دکتر اینترنت با بهره‌گیری از جدیدترین تکنولوژی‌های
                ساخت وب سایت شما را در تمام مراحل یاری می‌کنند تا بتوانید
                تجربه‌ای دلچسب به مشتریان خود ارائه کنید. در زیر به پاره‌ای از
                این موارد اشاره می‌شود.
              </p>
              <FeaturesLayout>
                <Grid
                  templateColumns='repeat(2, minmax(20rem, 1fr))'
                  alignContent='center'
                  gridGap='4rem'
                >
                  <Callout big>
                    <ResponsiveSVG />
                    <h4>طراحی واکنش‌گرا</h4>
                    <p>
                      وب سایت شما باید به گونه‌ای طراحی شود که در تمامی
                      دستگاه‌های قابل حمل مانند موبایل‌ها و تبلت‌ها به درستی
                      نمایش داده شود.
                    </p>
                  </Callout>
                  <Callout big>
                    <SeoSVG />
                    <h4>طراحی مبتنی بر استاندارد های سئو</h4>
                    <p>
                      استفاده از اصول توصیه شده شرکت گوگل در برنامه‌نویسی سایت
                      منجر به سریع‌تر شدن فرآیند جستجوی وب سایت شما در موتورهای
                      جستجو می‌شود.
                    </p>
                  </Callout>
                  <Callout big>
                    <DedicatedSVG />
                    <h4>توسعه‌ی اختصاصی</h4>
                    <p>
                      استفاده از قالب‌های آماده باعث کند شدن بارگذای وب سایت
                      خواهد شد. سایت شما بر اساس ماکت اولیه از ابتدا کد‌نویسی
                      می‌شود.
                    </p>
                  </Callout>
                  <Callout big>
                    <TechSVG />
                    <h4>بهره گیری از بروزترین تکنولوژی ها</h4>
                    <p>
                      سایت شما با بهره‌گیری از بهترین فریم‌ورک های زبان جاو‌‌ا
                      اسکریپت نظیر React در سمت کاربر و NodeJS در سمت سرور
                      برنامه نویسی می‌شود.
                    </p>
                  </Callout>
                </Grid>
                <WebAppSVG className='big-left-img' />
              </FeaturesLayout>
            </SectionHeader>
          </Container>
        </div>
        <WebsiteModal openModal={openModal} closeModal={closeModal} />
        <div style={{ margin: '5rem 0' }}>
          <Container alignItems='center'>
            <h2 style={{ fontSize: '2.5rem' }}>مراحل طراحی سایت اختصاصی</h2>
            <p>طراحی وب سایت اختصاصی شما در 3 مرحله انجام خواهد شد</p>

            <Grid
              templateColumns='repeat(3, minmax(20rem, 1fr))'
              style={{ margin: '3rem 0' }}
            >
              <Callout big>
                <AppWireframeSVG />
                <h4>1- ایجاد وایرفریم اولیه</h4>
                <p>
                  در این مرحله یک ماکت ابتدایی مطابق با استاندارد‌های طراحی بر
                  اساس ایده‌ی اولیه شما طراحی می‌شود.
                </p>
              </Callout>
              <Callout big>
                <AppDesignSVG />
                <h4>2- طراحی رابط کاربری</h4>
                <p>
                  در این مرحله بر اساس وایرفریم اولیه، رابط کاربری طراحی و بعد
                  از تأیید اولیه وارد مرحله کد نویسی می‌شود.
                </p>
              </Callout>
              <Callout big>
                <WebImplementSVG />
                <h4>3- پیاده سازی</h4>
                <p>
                  در این مرحله با داشتن طراحی گرافیکی و رابط کاربری، وب سایت کد
                  نویسی و api لازم تهیه می‌شود.
                </p>
              </Callout>
            </Grid>
          </Container>
        </div>
      </section>
    </Fragment>
  )
}

const FeaturesLayout = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 4rem;
  align-content: start;
  margin: 6rem 0;

  .big-left-img {
  }

  & > svg {
    width: auto;
    max-width: 60rem;
    height: auto;
    justify-self: center;

    /* @media (max-width: ${props => props.theme.medium}) {
      width: 45rem;
    }
    @media (max-width: ${props => props.theme.small}) {
      width: 40rem;
    }
    @media (max-width: ${props => props.theme.smallest}) {
      width: 35rem;
    } */
  }
`

website.propTypes = {
  openModal: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
}

export default connect(null, { openModal, closeModal })(website)
