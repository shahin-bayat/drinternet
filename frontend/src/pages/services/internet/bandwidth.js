import { Fragment, useState } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { openModal, closeModal } from '../../../redux/actions/modalActions'

import Header from '../../../components/styles/Header'
import Background from '../../../components/Background'
import Container from '../../../components/styles/Container'
import Grid from '../../../components/styles/Grid'
import Callout from '../../../components/styles/Callout'
import Button from '../../../components/styles/Button'
import SectionHeader from '../../../components/styles/SectionHeader'
import TariffCard from '../../../components/TariffCard'
import tariffs from '../../../data/dedicated-price.json'
import BandwidthModal from '../../../components/BandwidthModal'
import TextBox from '../../../components/styles/TextBoxServices'

import bg from '../../../images/internet-bandwidth.png'
import SpeedometerSVG from '../../../images/SVG/speedometer.svg'
import TransferSVG from '../../../images/SVG/transfer.svg'
import SupportSVG from '../../../images/SVG/support2.svg'
import MonitorSVG from '../../../images/SVG/monitor.svg'
import ContractSVG from '../../../images/SVG/contract.svg'
import SpeedSVG from '../../../images/SVG/speed.svg'

const Bandwidth = ({ openModal, closeModal }) => {
  const [service, setService] = useState('')
  return (
    <Fragment>
      <Header background='#f7f7f7' min-height='100vh' height='100%'>
        <Container alignItems='center'>
          <Grid
            templateColumns='70% 1fr'
            gridGap='0'
            bp_medium='1fr'
            style={{ padding: '0 4rem', marginTop: '7rem' }}
            gridGap='2rem'
          >
            <Background img={bg} backgroundPosition='bottom left' left />
            <TextBox>
              <h1 style={{ fontSize: '3rem' }}>
                پهنای باند اختصاصی ویژه‌ی شرکت‌ها
              </h1>
              <p>
                سرویس پهنای باند اختصاصی راهکاری است برای ایجاد یک ارتباط مطمئن
                و امن با پهنای باند بالا برای سازمان‌‌ها، شرکت‌­ها و کاربرانی که
                با مشکلات و محدودیت‌های اينترنت اشتراکی مواجه هستند.
              </p>
              <Button
                primary
                round
                moving
                shadowed
                onClick={async e => await openModal('bandwidth')}
              >
                مشاوره رایگان
              </Button>
            </TextBox>
          </Grid>
        </Container>
      </Header>
      <BandwidthModal
        service={service}
        setService={setService}
        closeModal={closeModal}
      />

      <section>
        <Container alignItems='center'>
          <SectionHeader>
            <h2>ویژگی‌های پهنای باند اختصاصی</h2>
            <Grid
              templateColumns='repeat(3, minmax(20rem, 1fr))'
              bp_medium='repeat(2, minmax(20rem, 1fr))'
              style={{ margin: '6rem 0' }}
            >
              <Callout>
                <SpeedometerSVG />
                <p>
                  پهنای باند کاملا اختصاصی بدون محدودیت ترافیک مصرفی (دانلود و
                  آپلود)
                </p>
              </Callout>
              <Callout>
                <SpeedSVG />
                <p>
                  ظرفیت بالا و امکان برقراری ارتباطی با کیفیت و نوسان بسیار کم
                </p>
              </Callout>
              <Callout>
                <TransferSVG />
                <p>ارائه‌ی پهنای باند متقارن/ نامتقارن بر اساس نیاز کاربر</p>
              </Callout>
              <Callout>
                <SupportSVG />
                <p>پشتیبانی شبانه‌روزی حتی در ایام تعطیل(7*24)</p>
              </Callout>
              <Callout>
                <MonitorSVG />
                <p>
                  ارائه نرم افزار مانیتورینگ به مشتریان جهت مشاهده‌ی میزان پهنای
                  باند مصرفی
                </p>
              </Callout>
              <Callout>
                <ContractSVG />
                <p>عقد قرارداد بر اساس توافقنامه سطح سرویس (SLA)</p>
              </Callout>
            </Grid>
          </SectionHeader>
        </Container>
      </section>
      <section style={{ marginBottom: '5rem' }}>
        <Container alignItems='center'>
          <Grid
            templateColumns='repeat(3,1fr)'
            gridGap='4rem'
            justifyContent='center'
            style={{ padding: '0 2rem' }}
            bp_medium='repeat(2, minmax(20rem, 1fr))'
          >
            {tariffs.map(tariff => (
              <TariffCard
                key={tariff.serviceName}
                tariff={tariff}
                setService={setService}
                openModal={openModal}
                page='bandwidth'
              />
            ))}
          </Grid>
        </Container>
      </section>
    </Fragment>
  )
}
Bandwidth.propTypes = {
  openModal: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
}

export default connect(null, { openModal, closeModal })(Bandwidth)
