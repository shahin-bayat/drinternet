import { Fragment } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { openModal, closeModal } from '../../../redux/actions/modalActions'
import TowersModal from '../../../components/TowersModal'
import TransitModal from '../../../components/TransitModal'
import styled from 'styled-components'

import Header from '../../../components/styles/Header'
import Background from '../../../components/Background'
import Container from '../../../components/styles/Container'
import Grid from '../../../components/styles/Grid'
import Callout from '../../../components/styles/Callout'
import Button from '../../../components/styles/Button'
import SectionHeader from '../../../components/styles/SectionHeader'
import SpeedometerSVG from '../../../images/SVG/speedometer.svg'
import bg2 from '../../../images/internet-tower.png'
import QuestionSVG from '../../../images/SVG/question.svg'
import PaperSVG from '../../../images/SVG/paper.svg'
import CheckSVG from '../../../images/SVG/check.svg'
import ExpressSVG from '../../../images/SVG/express.svg'
import CableSVG from '../../../images/SVG/cable.svg'
import PortSVG from '../../../images/SVG/port.svg'
import PhoneSVG from '../../../images/SVG/phone.svg'
import TextBox from '../../../components/styles/TextBoxServices'

import bg3 from '../../../images/internet-transit.png'
import NetworkSVG from '../../../images/SVG/network.svg'
import CCTVSVG from '../../../images/SVG/cctv.svg'
import GroupSVG from '../../../images/SVG/group.svg'

const index = ({ openModal, closeModal }) => {
  return (
    <Fragment>
      {/* TOWERS */}
      <section id='towers'>
        <Header background='#f7f7f7' height='100%'>
          <Container alignItems='center'>
            <Grid
              templateColumns='70% 1fr'
              bp_medium='1fr'
              style={{ padding: '0 4rem', marginTop: '7rem' }}
              gridGap='2rem'
            >
              <Background img={bg2} />
              <TextBox>
                <h1 style={{ fontSize: '3rem' }}>
                  خدمات ویژه‌ی برج‌ها و مجتمع‌های مسکونی
                </h1>
                <p>
                  اين سرويس راهکاريست برای کاربران ساکن در برج‌ها و مجتمع‌های
                  مسکونی و تجاری که با مشکل دریافت خدمات ADSL از جمله فیبر يا
                  PCM بودن خطوط تلفن و پر بودن پورت‌هاي مخابراتی منطقه مواجه
                  هستند. این سرویس در قالب ADSL يا VDSL قابل ارائه خواهد بود.
                </p>
                <Button
                  primary
                  round
                  moving
                  shadowed
                  onClick={async e => openModal('towers')}
                >
                  تکميل فرم درخواست بررسی اوليه
                </Button>
              </TextBox>
            </Grid>
            <Steps>
              <Callout flexDirection='row'>
                <QuestionSVG />
                <p>1- تکميل فرم درخواستی بررسی مجتمع</p>
              </Callout>
              <Callout flexDirection='row'>
                <PaperSVG />
                <p>2- بررسی فرم ارسالی توسط کارشناسان ما</p>
              </Callout>
              <Callout flexDirection='row'>
                <CheckSVG />
                <p>3- تماس کارشناسان، انجام مذاکرات و عقد قرارداد</p>
              </Callout>
            </Steps>
          </Container>
        </Header>
        <TowersModal closeModal={closeModal} />

        <section>
          <Container alignItems='center'>
            <SectionHeader>
              <h2>ویژگی‌های سرویس ویژه‌ی برج‌ها و مجتمع‌های مسکونی</h2>
              <Grid
                templateColumns='repeat(4, minmax(20rem, 1fr))'
                bp_medium='repeat(2, minmax(20rem, 1fr))'
                style={{ margin: '6rem 0' }}
              >
                <Callout>
                  <SpeedometerSVG />
                  <p>سرعت و کیفیت بسیار بالا</p>
                </Callout>
                <Callout>
                  <CableSVG />
                  <p>استفاده از زيرساخت موجود و عدم نياز به سيم‌کشي مجدد</p>
                </Callout>
                <Callout>
                  <PortSVG />
                  <p>عدم وابستگي به مراکز مخابراتي و پورت خالي</p>
                </Callout>
                <Callout>
                  <ExpressSVG />
                  <p>نصب و راه‌اندازی سریع سرویس</p>
                </Callout>
              </Grid>
            </SectionHeader>
          </Container>
        </section>
      </section>

      {/* TRANSIT */}
      <section id='transit'>
        <Header background='#ccf6ff' height='100%'>
          <Container alignItems='center'>
            <Grid
              templateColumns='70% 1fr'
              gridGap='0'
              bp_medium='1fr'
              style={{ padding: '0 4rem', marginTop: '7rem' }}
              gridGap='2rem'
            >
              <Background
                img={bg3}
                minHeight={'40rem'}
                backgroundPosition={'bottom center'}
              />

              <TextBox>
                <h1 style={{ fontSize: '3rem' }}>
                  سرویس ارتباط بین شعب (ترانزیت)
                </h1>
                <p>
                  این سرویس امکان برقراری ارتباط ميان نقاط مختلف جغرافيایی مراکز
                  و دفاتر مختلف سازمان‌ها و شرکت‌ها را با امنیت بالا فراهم
                  می‌کند.
                </p>
                <Button
                  primary
                  round
                  moving
                  shadowed
                  onClick={async e => openModal('transit')}
                >
                  درخواست مشاوره
                </Button>
              </TextBox>
            </Grid>
          </Container>
        </Header>
        <TransitModal closeModal={closeModal} />
        <section>
          <Container alignItems='center'>
            <SectionHeader>
              <h2>ویژگی‌های سرویس ترانزیت</h2>
              <Grid
                templateColumns='repeat(4, minmax(20rem, 1fr))'
                bp_medium='repeat(2, minmax(20rem, 1fr))'
                style={{ margin: '6rem 0' }}
              >
                <Callout>
                  <NetworkSVG />
                  <p>
                    امکان اشتراک‌گذاری اینترنت دفتر مرکزی بین شعب و دفاتر و
                    مدیریت بهینه‌تر آن
                  </p>
                </Callout>
                <Callout>
                  <GroupSVG />
                  <p>امکان استفاده از نرم افزار‌های اتوماسیون شامل CRM, ERP</p>
                </Callout>
                <Callout>
                  <PhoneSVG />
                  <p>
                    امکان استفاده از تلفن های تحت وب، سرورهای دسترسی فایل (FTP)
                    و نرم‌افزار‌های حسابداری
                  </p>
                </Callout>
                <Callout>
                  <CCTVSVG />
                  <p>
                    امکان استفاده از سیستم‌های نظارتی مانند دوربین‌های مداربسته
                    با کیفیت بالا و بدون نیاز به بستر اینترنت
                  </p>
                </Callout>
              </Grid>
            </SectionHeader>
          </Container>
        </section>
      </section>
    </Fragment>
  )
}

const Steps = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 0;
  justify-items: start;
  padding: 0 4rem;
  margin: 4rem 0;

  @media (max-width: ${props => props.theme.bp_small}) {
    grid-template-columns: 1fr;

    div {
      p {
        text-align: center;
      }
    }
  }
`

index.propTypes = {
  openModal: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
}

export default connect(null, { openModal, closeModal })(index)
