import { Fragment, useState } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { openModal, closeModal } from '../../../redux/actions/modalActions'
import styled from 'styled-components'

import Header from '../../../components/styles/Header'
import Background from '../../../components/Background'
import Container from '../../../components/styles/Container'
import Grid from '../../../components/styles/Grid'
import Callout from '../../../components/styles/Callout'
import Button from '../../../components/styles/Button'
import SectionHeader from '../../../components/styles/SectionHeader'
import TariffCard from '../../../components/TariffCard'
import AdslModal from '../../../components/AdslModal'
import TextBox from '../../../components/styles/TextBoxServices'

import bg from '../../../images/internet-adsl.png'
import QuestionSVG from '../../../images/SVG/question.svg'
import PaperSVG from '../../../images/SVG/paper.svg'
import CheckSVG from '../../../images/SVG/check.svg'
import SpeedometerSVG from '../../../images/SVG/speedometer.svg'
import FaxSVG from '../../../images/SVG/fax.svg'
import CableSVG from '../../../images/SVG/cable.svg'
import PhoneSVG from '../../../images/SVG/phone.svg'

import tariffs from '../../../data/adsl-price.json'

const Adsl = ({ openModal, closeModal }) => {
  const [service, setService] = useState('')

  const checkCoverage = async e => {
    e.preventDefault()
  }

  return (
    <Fragment>
      <Header background='#f7f7f7' height='100%'>
        <Container alignItems='center'>
          <Grid
            templateColumns='70% 1fr'
            gridGap='0'
            bp_medium='1fr'
            style={{ padding: '0 4rem', marginTop: '7rem' }}
            gridGap='2rem'
          >
            <Background img={bg} />
            <TextBox>
              <h1 style={{ fontSize: '3rem' }}>اینترنت +ADSL2</h1>
              <p>
                این سرویس برای برقراری ارتباط پرسرعت به اینترنت طراحی و در
                اختیار عموم قرارگرفته است. با بهره‌گيري از این فناوری کاربران
                قادر خواهند بود تنها با استفاده از یک خط تلفن، به صورت دائم و با
                سرعتی بالا به اینترنت متصل شوند.
              </p>
              <Button primary round moving shadowed onClick={checkCoverage}>
                بررسی پوشش ADSL
              </Button>
            </TextBox>
          </Grid>
          <Steps>
            <Callout flexDirection='row'>
              <QuestionSVG />
              <p>1- بررسی وضعیت خط تلفن برای ارائه‌ی سرویس</p>
            </Callout>
            <Callout flexDirection='row'>
              <PaperSVG />
              <p>2- ثبت درخواست و ارسال به کارشناسان ما</p>
            </Callout>
            <Callout flexDirection='row'>
              <CheckSVG />
              <p>3- تماس کارشناسان و عقد قرارداد و تحویل سرویس</p>
            </Callout>
          </Steps>
        </Container>
      </Header>

      <AdslModal
        service={service}
        setService={setService}
        tariffs={tariffs}
        closeModal={closeModal}
      />
      <section>
        <Container alignItems='center'>
          <SectionHeader>
            <h2>ویژگی‌های سرویس +ADSL2</h2>
            <Grid
              templateColumns='repeat(4, minmax(20rem, 1fr))'
              bp_medium='repeat(2, minmax(20rem, 1fr))'
              style={{ margin: '6rem 0' }}
            >
              <Callout>
                <SpeedometerSVG />
                <p>سرعت بالای اتصال به اینترنت</p>
              </Callout>
              <Callout>
                <CableSVG />
                <p>استفاده از سیم‌های تلفن فعلی (بدون نیاز به کابل‌کشی)</p>
              </Callout>
              <Callout>
                <PhoneSVG />
                <p>عدم اشغالی خط تلفن در حین استفاده از اینترنت</p>
              </Callout>
              <Callout>
                <FaxSVG />
                <p>استفاده‌ی همزمان از تلفن، فکس و اینترنت</p>
              </Callout>
            </Grid>
          </SectionHeader>
        </Container>
      </section>
      <section style={{ marginBottom: '5rem' }}>
        <Container alignItems='center'>
          <Grid
            templateColumns='repeat(3,1fr)'
            gridGap='4rem'
            justifyContent='center'
            style={{ padding: '0 2rem' }}
            bp_medium='repeat(2, minmax(20rem, 1fr))'
          >
            {tariffs.map(tariff => (
              <TariffCard
                key={tariff.serviceName}
                tariff={tariff}
                setService={setService}
                openModal={openModal}
                page='adsl'
              />
            ))}
          </Grid>
        </Container>
      </section>
    </Fragment>
  )
}

const Steps = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 0;
  justify-items: start;
  padding: 0 4rem;
  margin: 4rem 0;

  @media (max-width: ${props => props.theme.bp_small}) {
    grid-template-columns: 1fr;

    div {
      p {
        text-align: center;
      }
    }
  }
`

Adsl.propTypes = {
  openModal: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
}

export default connect(null, { openModal, closeModal })(Adsl)
