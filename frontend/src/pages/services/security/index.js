import { Fragment } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { openModal, closeModal } from '../../../redux/actions/modalActions'
import CameraModal from '../../../components/CameraModal'
import AntivirusModal from '../../../components/AntivirusModal'
import TextBox from '../../../components/styles/TextBoxServices'
import Header from '../../../components/styles/Header'
import Container from '../../../components/styles/Container'
import Grid from '../../../components/styles/Grid'
import Callout from '../../../components/styles/Callout'
import Button from '../../../components/styles/Button'
import SectionHeader from '../../../components/styles/SectionHeader'
import CCTVSVG from '../../../images/SVG/cctv.svg'
import img from '../../../images/blue-bg.png'
import img2 from '../../../images/green-bg.png'
import AntivirusSVG from '../../../images/SVG/antivirus.svg'
import SecurityCameraSVG from '../../../images/SVG/security-camera.svg'
import ServerSVG from '../../../images/SVG/server.svg'
import BugSVG from '../../../images/SVG/bug.svg'
import VirusShieldSVG from '../../../images/SVG/virus-shield.svg'
import CyberSecuritySVG from '../../../images/SVG/cyber-security.svg'
import NetworkSecuritySVG from '../../../images/SVG/network-security.svg'
import MonitorSVG from '../../../images/SVG/monitor.svg'
import PersonnelSVG from '../../../images/SVG/personnel.svg'
import MonitorStaffSVG from '../../../images/SVG/monitor-staff.svg'
import WarningSVG from '../../../images/SVG/warning.svg'

const index = ({ openModal, closeModal }) => {
  return (
    <Fragment>
      {/* Antivirus */}
      <section id='antivirus'>
        <Header background='#fff' img={img} img2={img} light>
          <Container alignItems='center'>
            <Grid
              templateColumns='1fr 60%'
              bp_medium='1fr'
              style={{ padding: '0 4rem', marginTop: '7rem' }}
            >
              <TextBox>
                <h1>مشاوره و راه‌اندازی امنیت شبکه</h1>
                <p>
                  امنيت يک فرآيند است نه يک رويداد. با کمک مشاوران ما در
                  حوزه‌های امنیت خانگی و امنیت سازمان‌ها و شرکت‌ها می توانید با
                  خیالی راحت مسیر IT پیش روی خود را طی کنید. کافی است با ما در
                  ارتباط باشید.
                </p>
                <Button
                  secondary
                  round
                  shadow
                  moving
                  onClick={e => openModal('antivirus')}
                >
                  تکمیل فرم ارتباط با ما
                </Button>
              </TextBox>
              <AntivirusSVG className='hero__image' />
            </Grid>
          </Container>
        </Header>
        <div>
          <AntivirusModal openModal={openModal} closeModal={closeModal} />
          <Container alignItems='center'>
            <SectionHeader>
              <h2>مزایا و ویژگی‌ها</h2>
              <Grid
                templateColumns='repeat(3, minmax(20rem, 1fr))'
                bp_medium='repeat(2, minmax(20rem, 1fr))'
                style={{ margin: '6rem 0' }}
              >
                <Callout>
                  <ServerSVG />
                  <p>
                    حصول اطمینان از ایمنی ساختار شبکه، اطلاعات، داده‌ها و
                    سیستم‌های نرم‌افزاری و سخت‌افزاری مجموعه
                  </p>
                </Callout>
                <Callout>
                  <BugSVG />
                  <p>
                    شناسایی ریسک‌های امنیتی موجود در بستر شبکه از قبیل ویروس‌ها
                    یا حمله‌ی هکرها
                  </p>
                </Callout>
                <Callout>
                  <VirusShieldSVG />
                  <p>
                    تهیه و تدوین راهکارهای مناسب به منظور جلوگیری از نفوذ و
                    انتشار ویروسهای کامپیوتری
                  </p>
                </Callout>
                <Callout>
                  <CyberSecuritySVG />
                  <p>
                    تهیه و تدوین راهکار‌های مناسب به منظور جلوگیری از نفوذ و
                    انتشار ویروس‌های کامپیوتری
                  </p>
                </Callout>
                <Callout>
                  <NetworkSecuritySVG />
                  <p>طراحی ساختار شبکه بر اساس استانداردهای امنیتی</p>
                </Callout>
                <Callout>
                  <MonitorSVG />
                  <p>
                    امکان ثبت لاگ‌های سیستمی و شبکه‌ای و آنالیز و بهره‌برداری
                    مناسب از آن
                  </p>
                </Callout>
              </Grid>
            </SectionHeader>
          </Container>
        </div>
      </section>

      {/* Camera */}
      <section id='security_camera'>
        <Header background='#fff' img={img2} img2={img2} light>
          <Container alignItems='center'>
            <Grid
              templateColumns='1fr 60%'
              bp_medium='1fr'
              style={{ padding: '0 4rem', marginTop: '7rem' }}
            >
              <TextBox>
                <h1>سیستم‌های نظارتی و دوربین‌های مداربسته</h1>
                <p>
                  با این سرویس می توانید خدمات طراحی، مشاوره، تأمین تجهیزات و
                  نصب دوربین‌هاي امنیتی مدار بسته را به ما بسپارید. برای دریافت
                  مشاوره لطفا فرم اطلاعات درخواستي را تکمیل فرمایید تا کارشناسان
                  ما با شما تماس بگیرند.
                </p>
                <Button
                  secondary
                  round
                  shadow
                  moving
                  onClick={e => openModal('camera')}
                >
                  تکمیل فرم درخواست بررسی اولیه
                </Button>
              </TextBox>
              <SecurityCameraSVG className='hero__image' />
            </Grid>
          </Container>
        </Header>
        <CameraModal openModal={openModal} closeModal={closeModal} />
        <div>
          <Container alignItems='center'>
            <SectionHeader>
              <h2>مزایا و ویژگی‌ها</h2>
              <Grid
                templateColumns='repeat(4, minmax(20rem, 1fr))'
                bp_medium='repeat(2, minmax(20rem, 1fr))'
                style={{ margin: '6rem 0' }}
              >
                <Callout>
                  <PersonnelSVG />
                  <p>
                    نظارت به فعالیت‌ها، تشویق به رفتارهای خوب و ایجاد نظم و
                    نظارت بین کارمندان
                  </p>
                </Callout>
                <Callout>
                  <MonitorStaffSVG />
                  <p>
                    کمک به اجرای قانون با استفاده از ثبت مدارک و وقایع، ویدیو‌ها
                    و صحنه‌های ضبط شده
                  </p>
                </Callout>
                <Callout>
                  <CCTVSVG />
                  <p>
                    مشاهده‌ و نظارت کامل بر محیط به صورت زنده از راه دور بر بستر
                    اینترنت یا اینترانت (ترانزیت)
                  </p>
                </Callout>
                <Callout>
                  <WarningSVG />
                  <p>
                    کاهش خطاهای انسانی تیم امنیتی شرکت‌ها با پوشش کامل و 100
                    درصدی محیط
                  </p>
                </Callout>
              </Grid>
            </SectionHeader>
          </Container>
        </div>
      </section>
    </Fragment>
  )
}

index.propTypes = {
  openModal: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
}

export default connect(null, { openModal, closeModal })(index)
