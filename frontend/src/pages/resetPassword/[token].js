// ! revised
import { useState } from 'react'
import { resetPassword } from '../../redux/actions/authActions'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import setToast from './../../utils/toast'

import Container from '../../components/styles/Container'
import Form from '../../components/styles/Form'
import AccountBox from '../../components/styles/AccountBox'
import InputGroup from '../../components/styles/InputGroup'
import Button from '../../components/styles/Button'

const ResetPassword_ = styled.div`
  width: 100%;
  height: 100vh;
  background: ${props => props.theme.color_grey_light};
`

const ResetPassword = ({ token, resetPassword }) => {
  const [user, setUser] = useState({
    password: '',
    password2: '',
  })

  const { password, password2 } = user

  const handleChange = e => {
    setUser({ ...user, [e.target.name]: e.target.value })
  }

  const handleSubmit = async e => {
    e.preventDefault()
    if (password !== password2) {
      setToast('رمزهای عبور متفاوت هستند', 'fail')
      setUser({ password: '', password2: '' })
    } else {
      resetPassword(password, token)
      setUser({ password: '', password2: '' })
    }
  }
  return (
    <ResetPassword_>
      <Container alignItems='center'>
        <AccountBox>
          <h3>تغییر رمز عبور</h3>
          <Form onSubmit={handleSubmit}>
            <InputGroup direction='ltr'>
              <input
                type='password'
                name='password'
                id='password'
                value={password}
                onChange={handleChange}
                minLength='6'
                maxLength='30'
                required
              />
              <label htmlFor='password'>رمز عبور</label>
            </InputGroup>
            <InputGroup direction='ltr'>
              <input
                type='password'
                name='password2'
                id='password2'
                value={password2}
                onChange={handleChange}
                minLength='6'
                maxLength='30'
                required
              />
              <label htmlFor='password'>تایید رمز عبور</label>
            </InputGroup>
            <Button square primary stretch>
              باز نشانی رمز عبور
            </Button>
          </Form>
        </AccountBox>
      </Container>
    </ResetPassword_>
  )
}

ResetPassword.propTypes = {
  resetPassword: PropTypes.func.isRequired,
}

ResetPassword.getInitialProps = async ctx => {
  const { token } = ctx.query

  return {
    token,
  }
}

export default connect(null, { resetPassword })(ResetPassword)
