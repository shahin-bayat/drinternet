// ! revised
import { useState } from "react"
import Link from "next/link"
import Router from "next/router"
import styled from "styled-components"
import { connect } from "react-redux"
import PropTypes from "prop-types"
import { loginUser } from "../redux/actions/authActions"

import Container from "../components/styles/Container"
import Form from "../components/styles/Form"
import AccountBox from "../components/styles/AccountBox"
import InputGroup from "../components/styles/InputGroup"
import Button from "../components/styles/Button"
import LoginSVG from "../images/SVG/login.svg"
import NProgress from "nprogress"

const Login_ = styled.div`
  width: 100%;
  height: 100vh;
  background: ${props => props.theme.color_grey_light};
`

const Login = ({ loginUser }) => {
  const [user, setUser] = useState({
    email: "",
    password: ""
  })
  const [loading, setLoading] = useState(false)
  const { email, password } = user

  const handleChange = e => {
    setUser({ ...user, [e.target.name]: e.target.value })
  }

  const handleSubmit = async e => {
    e.preventDefault()
    setLoading(true)
    NProgress.start()
    await loginUser(user)
    setUser({ email: "", password: "" })
    setLoading(false)
    NProgress.done()
  }

  return (
    <Login_>
      <Container alignItems='center'>
        <AccountBox>
          <h3>ورود به حساب کاربری</h3>
          <Form onSubmit={handleSubmit}>
            <InputGroup direction='ltr'>
              <input
                type='email'
                name='email'
                id='email'
                value={email}
                onChange={handleChange}
                placeholder='example@gmail.com'
                required
              />
              <label htmlFor='email'>ایمیل</label>
            </InputGroup>
            <InputGroup direction='ltr'>
              <input
                type='password'
                name='password'
                id='password'
                value={password}
                onChange={handleChange}
                required
              />
              <label htmlFor='password'>رمز عبور</label>
              <div className='account-box__subscript'>
                <Link href='/forgotPassword'>
                  <a>رمز عبور خود را فراموش کرده‌ام</a>
                </Link>
              </div>
            </InputGroup>
            <Button square primary stretch disabled={loading}>
              <LoginSVG fill='white' />
              وارد شوید
            </Button>
          </Form>
          <div className='account-box__footer'>
            <p>
              کاربر جدید هستید؟ &nbsp;
              <a href='/register'>ثبت نام در دکتر اینترنت</a>
            </p>
          </div>
        </AccountBox>
      </Container>
    </Login_>
  )
}

Login.propTypes = {
  loginUser: PropTypes.func.isRequired
}

Login.getInitialProps = async ctx => {
  if (ctx.store.getState().authentication.user) {
    if (ctx.req) {
      ctx.res
        .writeHead(302, {
          Location: "/"
        })
        .end()
    } else {
      Router.push("/")
    }
  }
}

export default connect(null, { loginUser })(Login)
