// ! revised
import React, { useState } from "react"
import { connect } from "react-redux"
import PropTypes from "prop-types"
import styled from "styled-components"
import { registerUser } from "../redux/actions/authActions"
import setToast from "../utils/toast"
import NProgress from "nprogress"

import Container from "../components/styles/Container"
import Form from "../components/styles/Form"
import AccountBox from "../components/styles/AccountBox"
import InputGroup from "../components/styles/InputGroup"
import Button from "../components/styles/Button"

const Register_ = styled.div`
  width: 100%;
  height: 100vh;
  background: ${props => props.theme.color_grey_light};
`

const Register = ({ registerUser }) => {
  const [user, setUser] = useState({
    name: "",
    email: "",
    password: "",
    password2: ""
  })
  const [loading, setLoading] = useState(false)

  const { name, email, password, password2 } = user

  const handleChange = e => {
    setUser({ ...user, [e.target.name]: e.target.value })
  }

  const handleSubmit = async e => {
    e.preventDefault()
    if (password !== password2) {
      setToast("رمزهای عبور متفاوت هستند", "fail")
      setUser({ ...user, password: "", password2: "" })
    } else {
      setLoading(true)
      NProgress.start()
      const newUser = {
        name: user.name,
        email: user.email,
        password: user.password
      }
      await registerUser(newUser)
      setLoading(false)
      NProgress.done()
      // setUser({ name: "", email: "", password: "", password2: "" })
    }
  }

  return (
    <Register_>
      <Container alignItems='center'>
        <AccountBox>
          <h3>ایجاد حساب کاربری</h3>
          <Form onSubmit={handleSubmit}>
            <InputGroup>
              <input
                type='text'
                name='name'
                id='name'
                value={name}
                onChange={handleChange}
                required
              />
              <label htmlFor='name'>نام و نام خانوادگی</label>
            </InputGroup>
            <InputGroup direction='ltr'>
              <input
                type='email'
                name='email'
                id='email'
                value={email}
                onChange={handleChange}
                placeholder='example@gmail.com'
                required
              />
              <label htmlFor='email'>ایمیل</label>
            </InputGroup>
            <InputGroup direction='ltr'>
              <input
                type='password'
                name='password'
                id='password'
                value={password}
                onChange={handleChange}
                minLength={6}
                maxLength={30}
                required
              />
              <label htmlFor='password'>رمز عبور</label>
            </InputGroup>
            <InputGroup direction='ltr'>
              <input
                type='password'
                name='password2'
                id='password2'
                value={password2}
                minLength={6}
                maxLength={30}
                onChange={handleChange}
                required
              />
              <label htmlFor='password2'>تایید رمز عبور</label>
            </InputGroup>
            <Button square primary stretch disabled={loading}>
              ثبت نام در دکتر اینترنت
            </Button>
          </Form>
          <div className='account-box__footer'>
            <p>
              قبلا ثبت نام کرده‌اید؟ &nbsp;
              <a href='/login'>ورود به سایت</a>
            </p>
          </div>
        </AccountBox>
      </Container>
    </Register_>
  )
}

Register.propTypes = {
  registerUser: PropTypes.func.isRequired
}

Register.getInitialProps = async ctx => {
  if (ctx.store.getState().authentication.user) {
    if (ctx.req) {
      ctx.res
        .writeHead(302, {
          Location: "/"
        })
        .end()
    } else {
      Router.push("/")
    }
  }
}
export default connect(state => state, { registerUser })(Register)
