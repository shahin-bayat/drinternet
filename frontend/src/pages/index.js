import { Fragment, useState } from 'react'
import Router from 'next/router'
import axios from 'axios'
import styled from 'styled-components'
import Link from 'next/link'
import changeTimeZone from '../utils/time'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { openModal, closeModal } from '../redux/actions/modalActions'

import Header from '../components/styles/Header'
import Container from '../components/styles/Container'
import Button from '../components/styles/Button'
import Card from '../components/styles/Card'
import Grid from '../components/styles/Grid'
import SectionHeader from '../components/styles/SectionHeader'
import Spacer from '../components/styles/Spacer'
import ContactModal from '../components/ContactModal'
import Callout from '../components/styles/Callout'
import Row from '../components/styles/Row'
import HR from '../components/styles/HR'
import TextBox from '../components/styles/TextBox'

import E404SVG from '../images/SVG/E404.svg'
import DisconnectSVG from '../images/SVG/disconnect.svg'
import HackerSVG from '../images/SVG/hacker.svg'
import NoPhoneSVG from '../images/SVG/phone-set.svg'
import OnlineShopSVG from '../images/SVG/online-shop.svg'
import img from '../images/hero-image-services.png'
import img2 from '../images/hero-image-services-2.png'

const Home = ({ posts, openModal }) => {
  return (
    <Fragment>
      {/* HERO */}
      <Header background='#fff' img={img} img2={img2}>
        <TextBox>
          <h1>
            نیازهای اینترنتی خود را به <em>دکتر اینترنت</em> بسپارید
          </h1>
          <Button
            primary
            round
            shadow
            moving
            onClick={e => Router.push('/services')}
          >
            بیشتر بدانید
          </Button>
        </TextBox>
      </Header>

      {/* SERVICES */}
      <Services>
        <Container alignItems='center'>
          <SectionHeader>
            <h2>خدمات</h2>
            <p>
              دکتر اینترنت با هدف برآورده کردن نیاز‌های روزمره‌ی کاربران خانگی و
              سازمان‌ها، خدمات ویژه‌ای در بخش‌های زیر ارائه می‌کند. برای کسب
              اطلاعات بیشتر می‌توانید افزون بر بررسی موارد زیر با مشاوران ما
              تماس بگیرید.
            </p>
          </SectionHeader>
          <Grid
            templateColumns='repeat(4, minmax(20rem, 1fr))'
            bp_medium='repeat(2, minmax(20rem, 1fr))'
          >
            {/* INTERNET */}
            <Card onClick={e => Router.push('/services#internet')}>
              <img
                src={require('../images/internet.png')}
                alt='خدمات اینترنت'
              />
              <h3>اینترنت و پهنای باند</h3>
              <p>
                ارائه‌ی خدمات ویژه به کاربران خانگی، سازمان‌ها و شرکت‌ها (شامل
                اینترنت اختصاصی و سرویس ویژه ارتباط بین شعب و دفاتر)
              </p>
              <Spacer />
              <Button round secondary>
                اطلاعات بیشتر
              </Button>
            </Card>
            {/* VOIP */}
            <Card onClick={e => Router.push('/services#phone')}>
              <img src={require('../images/phone.png')} alt='خدمات تلفن' />
              <h3>تلفن ثابت و VoIP</h3>
              <p>
                ارائه‌ی سرویس‌های تماس تلفن بین‌المللی و خطوط تلفن ثابت (NGN)
                ویژه‌ی شرکت‌‎های بزرگ، برج‌ها و مجتمع‌های مسکونی
              </p>
              <Spacer />
              <Button round secondary>
                اطلاعات بیشتر
              </Button>
            </Card>
            {/* SECURITY */}
            <Card onClick={e => Router.push('/services#security')}>
              <img src={require('../images/shield.png')} alt='امنیت و نظارت' />
              <h3>امنیت شبکه و نظارت</h3>
              <p>
                بررسی شبکه‌ی سازمان‌ها و شرکت‌ها از نظر آسیب پذیری‌های امنیتی و
                ارائه‌ی راهکار‌های متناسب با نیاز سازمان
              </p>
              <Spacer />
              <Button round secondary>
                اطلاعات بیشتر
              </Button>
            </Card>
            {/* WEB DEV */}
            <Card onClick={e => Router.push('/services#web')}>
              <img src={require('../images/web_dev.png')} alt='طراحی وب سایت' />
              <h3>طراحی وب سایت و اپلیکیشن</h3>
              <p>
                طراحی و توسعه‌ی وب سایت و اپلیکیشن موبایل، ساخت رابط کاربری و
                دیجیتال مارکتینگ با بهره‌گیری از جدیدترین تکنولوژی‌های روز دنیا
              </p>
              <Spacer />
              <Button round secondary>
                اطلاعات بیشتر
              </Button>
            </Card>
          </Grid>
          <Button
            primary
            round
            shadowed
            moving
            onClick={async e => openModal('contactUs')}
            style={{ margin: '4rem 0' }}
          >
            <img
              src={require('../images/customer-service.png')}
              style={{ width: '2.5rem' }}
              alt='طراحی وب سایت'
            />
            دریافت مشاوره
          </Button>
          <ContactModal />
        </Container>
      </Services>

      {/* SERVICES2 */}
      <Services2>
        <Row>
          <Container alignItems='center'>
            <Grid
              templateColumns='repeat(4, minmax(20rem, 1fr))'
              bp_medium='repeat(2, minmax(20rem, 1fr))'
              gridGap='2.5rem'
            >
              <Callout>
                <DisconnectSVG />
                <p>از قطعی و اختلال مداوم در سرویس اینترنت خود رنج می‌برید؟</p>
              </Callout>
              <Callout>
                <HackerSVG />
                <p>نگران امنیت شبکه یا هک شدن سیستم‌های خود هستید؟</p>
              </Callout>
              <Callout>
                <NoPhoneSVG />
                <p>به دلیل جابجایی نگران از دست دادن خطوط تلفن خود هستید؟</p>
              </Callout>
              <Callout>
                <OnlineShopSVG />
                <p>وب سایتی مناسب برای معرفی خدمات و محصولات خود نیاز دارید؟</p>
              </Callout>
            </Grid>
          </Container>
        </Row>
        <Container justifyContent='flex-start' style={{ padding: '0 2rem' }}>
          <Grid templateColumns='1fr 1fr'>
            <E404SVG
              width='100%'
              max-width='40rem'
              style={{ marginBottom: '4rem' }}
            />
            <TextBox2>
              <h3>
                با مشاوران ما در دکتر اینترنت می‌توانید از پس تمامی این مشکلات
                بر بیایید. کافیست با ما در ارتباط باشید.
              </h3>
              <Button
                primary
                round
                moving
                shadowed
                onClick={e => Router.push('/services#internet')}
              >
                همین حالا شروع کنید!
              </Button>
            </TextBox2>
          </Grid>
        </Container>
      </Services2>

      {/* PRODUCTS */}
      {/*       <section style={{ backgroundColor: "#f5f5f5" }}>
        <Container>
          <SectionHeader>
            <h2>جدیترین محصولات دکتر اینترنت</h2>
          </SectionHeader>
          <div>
            <ProductsSlide products={products} />
          </div>
          <Button primary round moving shadowed>
            فروشگاه دکتر اینترنت
          </Button>
        </Container>
      </section> */}

      {/*
      // TODO: remove products or not ?
      */}
      {/* BLOG */}
      <Blog>
        <Container alignItems='center'>
          <SectionHeader white>
            <h2>مجله الکترونیکی</h2>
            <p>
              جدیدترین اخبار و مقالات دنیای اینترنت و تکنولوژی را در مجله
              اینترنتی دکتر اینترنت دنبال کنید.
            </p>
          </SectionHeader>
          <Grid
            templateColumns='repeat(4, minmax(20rem, 1fr))'
            bp_medium='repeat(2, minmax(20rem, 1fr))'
            style={{ marginBottom: '3rem' }}
          >
            {posts
              ? posts.map(post => {
                  const { jDate, tehranTime } = changeTimeZone(post.createdAt)
                  return (
                    <Card key={post._id}>
                      <img
                        className='post-image'
                        src={post.postImage}
                        alt='post-image'
                      />
                      <Link href={`/blog/[slug]`} as={`/blog/${post.slug}`}>
                        <h3 className='continue-reading'>{post.title}</h3>
                      </Link>
                      <p>
                        {post.excerpt}...
                        <Link href={`/blog/[slug]`} as={`/blog/${post.slug}`}>
                          <a className='continue-reading'>ادامه مطلب</a>
                        </Link>
                      </p>
                      <Spacer />
                      <HR />
                      <p>
                        <span>
                          منتشر شده در {jDate} | {tehranTime}
                        </span>
                      </p>
                    </Card>
                  )
                })
              : ''}
          </Grid>

          <Button
            secondary
            round
            shadowed
            moving
            onClick={e =>
              Router.push('/blog').then(() => window.scrollTo(0, 0))
            }
          >
            مطالب بیشتر...
          </Button>
        </Container>
      </Blog>
    </Fragment>
  )
}

const Services = styled.section`
  padding: 0 2rem;
`
const Services2 = styled.section`
  background-color: ${props => props.theme.color_grey_light};
`

const TextBox2 = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  align-content: center;
  justify-items: start;
  padding: 0 2rem;

  h3 {
    text-align: justify;
  }

  @media (max-width: ${props => props.theme.bp_small}) {
    grid-template-columns: 1fr;
    justify-items: center;
    h3 {
      text-align: center;
      margin: 2rem 0;
    }

    button {
      margin-bottom: 3rem;
    }
  }
`

const Blog = styled.section`
  background: linear-gradient(20deg, #288ed9 0%, #137fcf 100%);
  padding: 0 2rem;
`

Home.getInitialProps = async ctx => {
  try {
    const postsRes = axios.get(
      `${process.env.BACKEND_URL}/posts/top-5-highlights?limit=4`
    )

    // const productsRes = axios.get(
    //   `${process.env.BACKEND_URL}/products?subcategory=security`
    // )

    // const [data1, data2] = await Promise.all([postsRes, productsRes])
    // const posts = data1.data.data.posts
    // const products = data2.data.data.products

    const { data } = await postsRes
    const posts = data.data.posts
    return { posts }
  } catch (error) {
    return {}
  }
}

Home.propTypes = {
  openModal: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
}

export default connect(state => state, { openModal, closeModal })(Home)
