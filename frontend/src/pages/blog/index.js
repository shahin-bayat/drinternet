import axios from 'axios'
import styled from 'styled-components'

import Container from '../../components/styles/Container'
import BlogHeader from '../../components/BlogHeader'
import BlogList from '../../components/BlogList'
import Head from 'next/head'
import getConfig from 'next/config'
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig()

const Blog_ = styled.section`
  background: ${props => props.theme.color_grey_light};
  padding: 0 2rem;
  padding-top: 10rem;
  min-height: 100vh;
`

const Blog = ({ posts, highlights, numberOfPages }) => {
  return (
    <Blog_>
      <Head>
        <title>مجله الکترونیکی دکتر اینترنت</title>
        <meta
          name='description'
          content='مجله اینترنتی دکتر اینترنت - جدیدترین مطالب و اخبار حوزه‌ی تکنولوژی، آموزش، اینترنت و نقد و بررسی محصولات'
        />

        <meta property='og:type' content='blog' key='og_type' />
        <meta
          property='og:title'
          content='مجله الکترونیکی دکتر اینترنت'
          key='og_title'
        />
        <meta
          property='og:description'
          content='مجله اینترنتی دکتر اینترنت - جدیدترین مطالب و اخبار حوزه‌ی تکنولوژی، آموزش، اینترنت و نقد و بررسی محصولات'
          key='og_description'
        />
      </Head>
      <Container alignItems='center'>
        <BlogHeader highlights={highlights} />
        <BlogList posts={posts} numberOfPages={numberOfPages} />
      </Container>
    </Blog_>
  )
}
Blog.getInitialProps = async ctx => {
  try {
    let posts, highlights, numberOfPages

    if (ctx.req) {
      const res1 = axios.get(
        `${serverRuntimeConfig.BACKEND_URL}/posts?page=1&published=true`
      )

      const res2 = axios.get(
        `${serverRuntimeConfig.BACKEND_URL}/posts/top-5-highlights`
      )

      const [data1, data2] = await Promise.all([res1, res2])

      highlights = data2.data.data.posts
      posts = data1.data.data.posts
      numberOfPages = data1.data.data.numberOfPages

      return { posts, highlights, numberOfPages }
    } else {
      const res1 = axios.get(
        `${process.env.BACKEND_URL}/posts?page=1&published=true`
      )

      const res2 = axios.get(
        `${process.env.BACKEND_URL}/posts/top-5-highlights`
      )

      const [data1, data2] = await Promise.all([res1, res2])

      highlights = data2.data.data.posts
      posts = data1.data.data.posts
      numberOfPages = data1.data.data.numberOfPages

      return { posts, highlights, numberOfPages }
    }
  } catch (error) {}
}

export default Blog
