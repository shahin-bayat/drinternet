import axios from 'axios'
import styled from 'styled-components'

import Container from '../../../components/styles/Container'
import BlogList from '../../../components/BlogList'
import HR from '../../../components/styles/HR'

const Category_ = styled.section`
  background: ${props => props.theme.color_grey_light};
  padding: 0 2rem;
  padding-top: 10rem;
  min-height: 100vh;
`
const SectionCategory = styled.div`
  h1 {
    font-size: 2rem;
    padding: 0 2rem;
    margin: 2rem 0;
    color: ${props => props.theme.color_primary};
  }

  p {
    padding: 2rem;
  }
`

const Category = ({ data, category }) => {
  let text

  switch (category) {
    case 'tech':
      text = 'تازه‌ترین اخبار تکنولوژی'
      break
    case 'news':
      text = 'تازه‌ترین اخبار'
      break
    case 'education':
      text = 'تازه‌ترین مطالب آموزشی'
      break
    case 'review':
      text = 'تازه‌ترین نقد و بررسی‌ها'
      break
    default:
      text = 'تازه ترین مطالب'
  }

  return (
    <Category_>
      <Container>
        <SectionCategory>
          <h1>{text}</h1>
          <HR />
        </SectionCategory>
        {<BlogList posts={data.posts} numberOfPages={data.numberOfPages} />}
      </Container>
    </Category_>
  )
}

Category.getInitialProps = async ctx => {
  const { category } = ctx.query
  const path = `${process.env.BACKEND_URL}/posts?category=${category}`
  const {
    data: { data },
  } = await axios.get(path)
  return { data, category }
}
export default Category
