// ! revised
import { useState } from 'react'
import styled from 'styled-components'
import { forgotPassword } from '../redux/actions/authActions'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import Container from '../components/styles/Container'
import Form from '../components/styles/Form'
import AccountBox from '../components/styles/AccountBox'
import InputGroup from '../components/styles/InputGroup'
import Button from '../components/styles/Button'

const ForgotPassword_ = styled.div`
  width: 100%;
  height: 100vh;
  background: ${props => props.theme.color_grey_light};
`

const ForgotPassword = ({ forgotPassword }) => {
  const [email, setEmail] = useState('')

  const handleChange = e => {
    setEmail(e.target.value)
  }

  const handleSubmit = async e => {
    e.preventDefault()
    forgotPassword(email)
    setEmail('')
  }

  return (
    <ForgotPassword_>
      <Container alignItems='center'>
        <AccountBox>
          <h3>یادآوری رمز عبور</h3>
          <Form onSubmit={handleSubmit}>
            <InputGroup direction='ltr'>
              <input
                type='email'
                name='email'
                id='email'
                value={email}
                onChange={handleChange}
                required
              />
              <label htmlFor='email'>ایمیل</label>
            </InputGroup>
            <Button square primary stretch>
              یادآوری رمز عبور
            </Button>
          </Form>
        </AccountBox>
      </Container>
    </ForgotPassword_>
  )
}

ForgotPassword.propTypes = {
  forgotPassword: PropTypes.func.isRequired,
}

export default connect(null, { forgotPassword })(ForgotPassword)
