import { isAuthorized } from '../../../utils/auth'
import Container from '../../../components/styles/Container'
import Pagination from '../../../components/layout/Pagination'
import Grid from '../../../components/styles/Grid'
import SideNavUser from '../../../components/layout/SideNavUser'
import ProfileDetails from '../../../components/styles/ProfileDetails'
import UserProfile from '../../../components/styles/UserProfile'

const Cart = () => {
  return (
    <UserProfile>
      <Container justifyContent='flex-start'>
        <Grid gridGap='1rem' templateColumns='1fr 75%'>
          <SideNavUser />
          <ProfileDetails>
            <p style={{ padding: '2rem', fontSize: '1.2rem' }}>
              در حال حاضر سبد خرید شما خالی است.
            </p>
          </ProfileDetails>
        </Grid>
      </Container>
    </UserProfile>
  )
}

Cart.getInitialProps = async ctx => {
  isAuthorized(['user'], ctx)
}

export default Cart
