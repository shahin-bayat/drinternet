import { useState } from 'react'
import { isAuthorized } from '../../utils/auth'
import { updateUserProfile } from '../../redux/actions/authActions'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import moment from 'moment-jalaali'
import dynamic from 'next/dynamic'
const DatePicker = dynamic(() => import('../../components/DatePick'), {
  ssr: false,
})

import Container from '../../components/styles/Container'
import Form from '../../components/styles/Form'
import Grid from '../../components/styles/Grid'
import InputGroup from '../../components/styles/InputGroup'
import Button from '../../components/styles/Button'
import SideNavUser from '../../components/layout/SideNavUser'
import ProfileDetails from '../../components/styles/ProfileDetails'
import UserProfile from '../../components/styles/UserProfile'

const DatePickerGroup = styled.div`
  position: relative;
  width: 100%;
  margin: 1.5rem 0;

  label {
    position: absolute;
    right: 1.25rem;
    top: 0;
    transform: translateY(-50%);
    padding: 0 0.75rem;
    background-color: #fff;
    color: ${props => props.theme.color_grey};
    z-index: 100;
  }
`

const EditProfile = ({ user, updateUserProfile }) => {
  // user from db
  const { name, email, phone, birthday } = user
  // sent down to datepicker

  const [date, setDate] = useState(null)
  const [profile, setProfile] = useState({
    name,
    email,
    phone: phone ? phone : '',
  })
  const [loading, setLoading] = useState(false)
  let dateObj = null

  if (birthday) {
    const jDate = moment(birthday, 'YYYY-M-D').format('jYYYY/jM/jD')
    let [year, month, day] = jDate.split('/')
    year = Number(year)
    month = Number(month)
    day = Number(day)
    dateObj = { year, month, day }
  }

  const handleSubmit = async e => {
    e.preventDefault()
    setLoading(true)
    let gDate = null
    if (date) {
      const { day, month, year } = date
      const jDate = `${year}/${month}/${day}`
      gDate = moment(jDate, 'jYYYY/jM/jD').format('YYYY-M-D')
    }

    const newProfile = {
      name: profile.name,
      email: profile.email,
      phone: profile.phone ? profile.phone : '',
      birthday: gDate ? gDate : undefined,
    }

    await updateUserProfile(newProfile)

    setLoading(false)
  }

  const handleChange = e => {
    setProfile({ ...profile, [e.target.name]: e.target.value })
  }

  return (
    <UserProfile>
      <Container alignItems='center'>
        <Grid gridGap='1rem' templateColumns='1fr 75%'>
          <SideNavUser />
          <ProfileDetails>
            <Form onSubmit={handleSubmit}>
              <div className='profile__title'>
                <span>ویرایش اطلاعات شخصی:</span>
              </div>
              <InputGroup>
                <input
                  type='text'
                  id='name'
                  name='name'
                  value={profile.name}
                  onChange={handleChange}
                />
                <label htmlFor='name'>نام و نام خانوادگی</label>
              </InputGroup>

              <InputGroup direction='ltr'>
                <input
                  type='email'
                  id='email'
                  name='email'
                  value={profile.email}
                  onChange={handleChange}
                />
                <label htmlFor='email'>ایمیل</label>
              </InputGroup>

              <InputGroup direction='ltr'>
                <input
                  type='text'
                  id='phone'
                  value={profile.phone}
                  placeholder='مثال: 09121234567'
                  name='phone'
                  onChange={handleChange}
                  minLength={11}
                />
                <label htmlFor='phone'>شماره موبایل</label>
              </InputGroup>

              <DatePickerGroup>
                <DatePicker
                  id='birthday'
                  setBirthday={setDate}
                  current={dateObj}
                />
                <label htmlFor='birthday'>تاریخ تولد</label>
              </DatePickerGroup>
              {/* <input type='submit' href='/profile' value='به روز رسانی' /> */}

              <Button primary square disabled={loading}>
                به روز رسانی
              </Button>
            </Form>
          </ProfileDetails>
        </Grid>
      </Container>
    </UserProfile>
  )
}

EditProfile.getInitialProps = async ctx => {
  isAuthorized(['user'], ctx)
  const { user } = ctx.store.getState().authentication
  return { user }
}

EditProfile.propTypes = {
  updateUserProfile: PropTypes.func.isRequired,
}

export default connect(state => state, { updateUserProfile })(EditProfile)
