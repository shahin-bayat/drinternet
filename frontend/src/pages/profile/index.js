import styled from 'styled-components'
import axios from 'axios'
import { connect } from 'react-redux'
import moment from 'moment-jalaali'
import Router from 'next/router'
import PropTypes from 'prop-types'
import { logoutUser } from '../../redux/actions/authActions'
import { isAuthorized } from '../../utils/auth'
import UserProfile from '../../components/styles/UserProfile'

import Grid from '../../components/styles/Grid'
import SideProfile from '../../components/layout/SideProfile'
import SideNavUser from '../../components/layout/SideNavUser'
import NavTitle from '../../components/styles/NavTitle'
import Container from '../../components/styles/Container'
import ProfileDetails from '../../components/styles/ProfileDetails'
import PencilSVG from '../../images/SVG/pencil.svg'

axios.defaults.withCredentials = true

const GridItems = styled.div`
  padding: 2rem 3rem;
  :last-child {
    grid-column: 1 / -1;
    background-color: ${props => props.theme.color_green};
    display: flex;
    justify-content: center;
  }
  p {
    display: flex;
    align-items: center;

    > a {
      margin-right: 1rem;
    }
  }
  a {
    text-decoration: none;
    color: ${props => props.theme.color_primary};
    border-bottom: 1px dashed ${props => props.theme.color_primary};
    font-size: 1.5rem;
  }
  span {
    font-size: 1.5rem;
    color: ${props => props.theme.color_black};
  }
  :nth-child(odd) {
    border-left: ${props => props.theme.border};
  }
  :not(:last-child) {
    border-bottom: ${props => props.theme.border};
  }
  :last-child {
    border-left: none;
  }
`

const Profile = ({ user, logoutUser }) => {
  moment.loadPersian({ dialect: 'persian' })
  const jDate = moment(user.birthday, 'YYYY-M-D').format('jYYYY/jM/jD')

  const handleLogout = e => {
    logoutUser()
    Router.push('/')
  }

  return (
    <UserProfile>
      <Container alignItems='center' justifyContent='flex-start'>
        <Grid gridGap='1rem' templateColumns='1fr 75%'>
          <SideProfile user={user} handleLogout={handleLogout} />
          <ProfileDetails>
            <Grid templateColumns='1fr 1fr' gridGap='0'>
              <GridItems>
                <p>نام و نام خانوادگی:</p>
                <span>{user.name}</span>
              </GridItems>
              <GridItems>
                <p>ایمیل:</p>
                <span>{user.email}</span>
              </GridItems>
              <GridItems>
                <p>شماره تلفن همراه:</p>
                <span>{user.phone}</span>
              </GridItems>
              <GridItems>
                <p>تاریخ تولد:</p>
                <span>{user.birthday && jDate}</span>
              </GridItems>
              <GridItems>
                <p>
                  <PencilSVG />
                  <a href='/profile/edit-profile'>ویرایش اطلاعات شخصی</a>
                </p>
              </GridItems>
            </Grid>
          </ProfileDetails>

          <SideNavUser />

          <ProfileDetails>
            <NavTitle>
              <span>آخرین سفارش ها</span>
            </NavTitle>
            <p style={{ padding: '2rem', fontSize: '1.2rem' }}>
              در حال حاضر هیچ سفارش فعالی وجود ندارد
            </p>
          </ProfileDetails>
        </Grid>
      </Container>
    </UserProfile>
  )
}

Profile.getInitialProps = async ctx => {
  isAuthorized(['user'], ctx)
  const { user } = ctx.store.getState().authentication
  return { user }
}

Profile.propTypes = {
  logoutUser: PropTypes.func.isRequired,
}

export default connect(state => state, { logoutUser })(Profile)
