import styled from 'styled-components'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { deleteMessage } from '../../../redux/actions/messageActions'
import Router from 'next/router'
import { isAuthorized } from '../../../utils/auth'

import Container from '../../../components/styles/Container'
import Grid from '../../../components/styles/Grid'
import SideNavUser from '../../../components/layout/SideNavUser'
import ProfileDetails from '../../../components/styles/ProfileDetails'
import InfoGroup from '../../../components/styles/InfoGroup'
import Button from '../../../components/styles/Button'
import UserProfile from '../../../components/styles/UserProfile'

const InfoBox = styled.div`
  margin: 2rem 0;
  padding: 0 2rem;
  display: flex;
  flex-direction: column;
  min-height: 40rem;
`

const message = ({ message, deleteMessage }) => {
  const handleDelete = e => {
    e.preventDefault()
    deleteMessage(message._id)
    Router.push('/profile/messages')
  }
  return (
    <UserProfile>
      <Container justifyContent='flex-start'>
        <Grid gridGap='1rem' templateColumns='1fr 75%'>
          <SideNavUser />
          <ProfileDetails>
            <InfoBox>
              <InfoGroup>
                <p>{message.subject}</p>
                <span>عنوان پیام</span>
              </InfoGroup>
              <InfoGroup>
                <textarea direction='rtl' value={message.body} readOnly />
                <span>متن پیام</span>
              </InfoGroup>
              <Button onClick={handleDelete} warning square>
                پاک کردن پیام
              </Button>
            </InfoBox>
          </ProfileDetails>
        </Grid>
      </Container>
    </UserProfile>
  )
}

message.getInitialProps = async ctx => {
  isAuthorized(['user'], ctx)
  const { messages } = ctx.store.getState().message
  const message = messages.filter(msg => msg._id === ctx.query.messageId)

  return { message: message[0] }
}

message.propTypes = {
  deleteMessage: PropTypes.func.isRequired,
  message: PropTypes.object.isRequired,
}

export default connect(null, { deleteMessage })(message)
