import { useState } from 'react'
import { sendTicket } from '../../redux/actions/messageActions'
import { connect } from 'react-redux'
import { isAuthorized } from '../../utils/auth'
import PropTypes from 'prop-types'
import UserProfile from '../../components/styles/UserProfile'

import Container from '../../components/styles/Container'
import Grid from '../../components/styles/Grid'
import Form from '../../components/styles/Form'
import InputGroup from '../../components/styles/InputGroup'
import Button from '../../components/styles/Button'
import SideNavUser from '../../components/layout/SideNavUser'
import ProfileDetails from '../../components/styles/ProfileDetails'

const Ticket = ({ sendTicket, user }) => {
  const [ticket, setTicket] = useState({
    subject: '',
    ticketBody: '',
  })
  const [loading, setLoading] = useState(false)

  const handleChange = e => {
    setTicket({ ...ticket, [e.target.name]: e.target.value })
  }

  const handleSubmit = async e => {
    e.preventDefault()
    setLoading(true)
    await sendTicket(ticket)
    setLoading(false)
  }
  return (
    <UserProfile>
      <Container justifyContent='flex-start'>
        <Grid gridGap='1rem' templateColumns='1fr 75%' nomedia>
          <SideNavUser />
          <ProfileDetails>
            <Form onSubmit={handleSubmit}>
              <div className='profile__title'>
                <span>ارسال تیکت پشتیبانی:</span>
              </div>
              <InputGroup>
                <input
                  type='text'
                  id='subject'
                  name='subject'
                  value={ticket.subject}
                  onChange={handleChange}
                  maxLength={60}
                />
                <label htmlFor='subject'>موضوع</label>
              </InputGroup>
              <InputGroup>
                <textarea
                  id='ticketBody'
                  name='ticketBody'
                  onChange={handleChange}
                />
                <label htmlFor='ticketBody'>متن درخواست</label>
              </InputGroup>
              <Button primary square disabled={loading}>
                ارسال تیکت
              </Button>
            </Form>
          </ProfileDetails>
        </Grid>
      </Container>
    </UserProfile>
  )
}

Ticket.propTypes = {
  sendTicket: PropTypes.func.isRequired,
}

Ticket.getInitialProps = async ctx => {
  isAuthorized(['user'], ctx)
  const { user } = ctx.store.getState().authentication
  return { user }
}

export default connect(state => state, { sendTicket })(Ticket)
