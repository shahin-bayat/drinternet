import React, { useState } from "react"
import { connect } from "react-redux"
import PropTypes from "prop-types"
import styled from "styled-components"
import setToast from "../../utils/toast"
import { changePassword } from "../../redux/actions/authActions"
import { isAuthorized } from "../../utils/auth"

import Container from "../../components/styles/Container"
import Form from "../../components/styles/Form"
import AccountBox from "../../components/styles/AccountBox"
import InputGroup from "../../components/styles/InputGroup"
import Button from "../../components/styles/Button"

const ChangePassword_ = styled.div`
  width: 100%;
  height: 100vh;
  background: ${props => props.theme.color_grey_light};
`

const ChangePassword = ({ changePassword }) => {
  const [user, setUser] = useState({
    passwordCurrent: "",
    password: "",
    password2: ""
  })

  const { passwordCurrent, password, password2 } = user

  const handleChange = e => {
    setUser({ ...user, [e.target.name]: e.target.value })
  }

  const handleSubmit = async e => {
    e.preventDefault()
    if (password !== password2) {
      setToast("رمزهای عبور متفاوت هستند", "fail")
      setUser({ ...user, password: "", password2: "" })
    } else {
      const newPassword = {
        passwordCurrent: user.passwordCurrent,
        password: user.password
      }
      // change password
      changePassword(newPassword)
      // setUser({ name: "", email: "", password: "", password2: "" })
    }
  }

  return (
    <ChangePassword_>
      <Container alignItems='center'>
        <AccountBox>
          <h3>تغییر رمز عبور</h3>
          <Form onSubmit={handleSubmit}>
            <InputGroup direction='ltr'>
              <input
                type='password'
                name='passwordCurrent'
                id='passwordCurrent'
                value={passwordCurrent}
                onChange={handleChange}
                required
              />
              <label htmlFor='passwordCurrent'>رمز عبور فعلی</label>
            </InputGroup>
            <InputGroup direction='ltr'>
              <input
                type='password'
                name='password'
                id='password'
                value={password}
                onChange={handleChange}
                minLength={6}
                maxLength={30}
                required
              />
              <label htmlFor='password'>رمز عبور جدید</label>
            </InputGroup>
            <InputGroup direction='ltr'>
              <input
                type='password'
                name='password2'
                id='password2'
                value={password2}
                minLength={6}
                maxLength={30}
                onChange={handleChange}
                required
              />
              <label htmlFor='password2'>تایید رمز عبور جدید</label>
            </InputGroup>
            <Button square primary stretch>
              تغییر رمز
            </Button>
          </Form>
        </AccountBox>
      </Container>
    </ChangePassword_>
  )
}

ChangePassword.propTypes = {
  changePassword: PropTypes.func.isRequired
}

ChangePassword.getInitialProps = async ctx => {
  isAuthorized(["user"], ctx)
}
export default connect(state => state, { changePassword })(ChangePassword)
