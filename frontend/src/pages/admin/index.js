import { isAuthorized } from '../../utils/auth'

import SideNavAdmin from '../../components/layout/SideNavAdmin'
import Container from '../../components/styles/Container'
import Grid from '../../components/styles/Grid'
import ProfileDetails from '../../components/styles/ProfileDetails'
import UserProfile from '../../components/styles/UserProfile'

const Admin = () => {
  return (
    <UserProfile>
      <Container justifyContent='flex-start'>
        <Grid gridGap='1rem' templateColumns='1fr 75%'>
          <SideNavAdmin />
          <ProfileDetails>
            <div className='profile__title' style={{ margin: '1.5rem 2rem' }}>
              <span>آمار:</span>
            </div>
          </ProfileDetails>
        </Grid>
      </Container>
    </UserProfile>
  )
}

Admin.getInitialProps = async ctx => {
  isAuthorized(['admin'], ctx)
}

export default Admin
