import React from 'react'
import { isAuthorized } from '../../utils/auth'

import SideNavAdmin from '../../components/layout/SideNavAdmin'
import Grid from '../../components/styles/Grid'
import Container from '../../components/styles/Container'
import ProfileDetails from '../../components/styles/ProfileDetails'
import UserProfile from '../../components/styles/UserProfile'

const Orders = () => {
  return (
    <UserProfile>
      <Container justifyContent='flex-start'>
        <Grid gridGap='1rem' templateColumns='1fr 75%'>
          <SideNavAdmin />
          <ProfileDetails></ProfileDetails>
        </Grid>
      </Container>
    </UserProfile>
  )
}

Orders.getInitialProps = async ctx => {
  isAuthorized(['admin'], ctx)
}
export default Orders
