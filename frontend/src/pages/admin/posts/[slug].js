import axios from "axios"
import PostLayout from "../../../components/PostLayout"
import { isAuthorized } from "../../../utils/auth"
import getConfig from 'next/config'
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig()

const Post = ({ data }) => {
  return <PostLayout data={data} />
}

Post.getInitialProps = async ctx => {
  isAuthorized(["admin"], ctx)
  const { slug } = ctx.query
  const decoded = encodeURI(slug)
  let post
  if (ctx.req) {
    const { data } = await axios.get(
      `${serverRuntimeConfig.BACKEND_URL}/posts/${decoded}`
    )
    post = data.data
  } else {
    const { data } = await axios.get(
      `${process.env.BACKEND_URL}/posts/${decoded}`
    )
    post = data.data
  }

  return {
    data: post,
  }
}
export default Post
