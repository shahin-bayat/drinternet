import { useState } from 'react'
import styled from 'styled-components'
import Router from 'next/router'
import { isAuthorized } from '../../../../utils/auth'
import setToast from '../../../../utils/toast'
import axios from 'axios'
import dynamic from 'next/dynamic'
const Editor = dynamic(() => import('../../../../components/NextEditorEdit'), {
  ssr: false,
})
import UserProfile from '../../../../components/styles/UserProfile'
import Container from '../../../../components/styles/Container'
import Form from '../../../../components/styles/Form'
import InputGroup from '../../../../components/styles/InputGroup'
import Button from '../../../../components/styles/Button'
import Table from '../../../../components/styles/Table'
import CheckBoxGroup from '../../../../components/styles/CheckBoxGroup'

axios.defaults.withCredentials = true

const Edit = ({ data }) => {
  const [post, setPost] = useState({
    title: data.title,
    body: '',
    fileName: data.fileName,
    postImage: data.postImage,
    excerpt: data.excerpt,
    tags: data.tags.join(),
    isHighlighted: data.isHighlighted,
  })

  const [category, setCategory] = useState(data.category)
  const [subcategory, setSubcategory] = useState(data.subcategory)

  const [files, setFiles] = useState([])
  const [htmlBody, setHtmlBody] = useState('')
  // uploaded images:
  const [images, setImages] = useState([])

  const { title, fileName, postImage, tags, excerpt, isHighlighted } = post

  const handleChange = e => {
    if (e.target.name === 'category') {
      setCategory(e.target.value)
    } else if (e.target.name === 'subcategory') {
      setSubcategory(e.target.value)
    } else {
      setPost({ ...post, [e.target.name]: e.target.value })
    }
  }

  const handleFileChange = e => {
    setFiles(e.target.files)
  }

  const handleSubmit = async e => {
    e.preventDefault()
    const editedPost = {
      title,
      fileName,
      excerpt,
      postImage,
      body: htmlBody,
      category,
      subcategory,
      tags,
      isHighlighted,
    }

    await axios.patch(
      `${process.env.BACKEND_URL}/posts/${data._id}`,
      editedPost
    )
    setToast('پست با موفقیت ویرایش شد', 'success', 2000)
    setTimeout(() => {
      Router.push('/admin/posts')
    }, 2000)
  }

  const handleUpload = async e => {
    e.preventDefault()
    const formData = new FormData()
    const sanitizeFileName = fileName => {
      const [name, extension] = fileName.split('.')
      return `${name.replace(/[^a-z0-9]/gi, '_').toLowerCase()}.${extension}`
    }
    for (let i = 0; i < files.length; i++) {
      formData.append('image', files[i], sanitizeFileName(files[i].name))
    }
    const res = await axios.post(
      `${process.env.BACKEND_URL}/posts/images`,
      formData,
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      }
    )
    // console.log(res.request.responseURL)
    setImages(res.data.items)
  }

  const handleCheckboxChange = e => {
    setPost({ ...post, isHighlighted: e.target.checked })
  }

  return (
    <UserProfile>
      <Container justifyContent='flex-start' alignItems='stretch'>
        <Form onSubmit={handleUpload}>
          <InputGroup dir='ltr'>
            <input
              type='file'
              multiple
              onChange={handleFileChange}
              accept='image/*'
              id='file'
            />
            <label htmlFor='file'>بارگزاری عکس‌ها</label>
          </InputGroup>
          <Button primary square>
            بارگزاری
          </Button>
        </Form>
        <div>
          <Table>
            {images.map(image => (
              <li key={image.filename} className='table__items'>
                {/* FIXME: EDIT LINK href */}
                <a
                  style={{
                    textDecoration: 'none',
                    fontFamily: 'Tahoma',
                    color: '#288ED9',
                  }}
                  href={`http://localhost:5000/posts/${image.filename}`}
                >
                  {image.filename}
                </a>
              </li>
            ))}
          </Table>
        </div>
        <Form onSubmit={handleSubmit} style={{ width: '100%' }}>
          <InputGroup>
            <input
              type='text'
              name='title'
              id='title'
              value={title}
              onChange={handleChange}
              required
            />
            <label htmlFor='title'>عنوان پست</label>
          </InputGroup>
          <InputGroup>
            <input
              type='text'
              name='excerpt'
              id='excerpt'
              value={excerpt}
              maxLength={125}
              onChange={handleChange}
              required
            />
            <label htmlFor='except'>متن کوتاه معرفی</label>
          </InputGroup>
          <InputGroup dir='ltr'>
            <input
              type='text'
              name='postImage'
              id='postImage'
              value={postImage}
              onChange={handleChange}
              style={{ fontFamily: 'tahoma' }}
              required
            />
            <label htmlFor='postImage'>لینک تصویر اصلی پست</label>
          </InputGroup>
          <InputGroup>
            <input
              type='text'
              name='fileName'
              id='fileName'
              value={fileName}
              onChange={handleChange}
              required
              disabled // to prevent changing file name on server and losing data
            />
            <label htmlFor='fileName'>عنوان فایل پست</label>
          </InputGroup>

          <InputGroup>
            <select
              type='text'
              name='category'
              id='category'
              value={category}
              onChange={handleChange}
              required
            >
              <option value='تکنولوژی'>تکنولوژی</option>
              <option value='آموزش'>آموزش</option>
              <option value='نقد و بررسی'>نقد و بررسی</option>
              <option value='اخبار'>اخبار</option>
            </select>
            <label htmlFor='category'>شاخه اصلی</label>
          </InputGroup>

          <InputGroup>
            <select
              type='text'
              name='subcategory'
              id='subcategory'
              value={subcategory}
              onChange={handleChange}
              required
            >
              <option value='نامشخص'>نامشخص</option>
              <option value='موبایل و گجت'>موبایل و گجت</option>
              <option value='دوربین'>دوربین</option>
              <option value='اینترنت'>اینترنت</option>
              <option value='سخت افزار'>سخت افزار</option>
              <option value='نرم‌افزار و اپلیکیشن'>نرم‌افزار و اپلیکیشن</option>
              <option value='صوتی و تصویری'>صوتی و تصویری</option>
              <option value='برنامه‌نویسی'>برنامه‌نویسی</option>
            </select>
            <label htmlFor='subcategory'>زیر شاخه</label>
          </InputGroup>

          <InputGroup>
            <input
              type='text'
              name='tags'
              id='tags'
              value={tags}
              onChange={handleChange}
              required
            />
            <label htmlFor='tags'>تگ‌ها</label>
          </InputGroup>

          <CheckBoxGroup style={{ marginBottom: '2rem' }}>
            <input
              type='checkbox'
              id='isHighlighted'
              checked={isHighlighted}
              onChange={handleCheckboxChange}
            />
            <label htmlFor='isHighlighted'>
              <span />
              در هایلایت‌ها ظاهر شود؟{' '}
            </label>
          </CheckBoxGroup>

          <label
            htmlFor='Editor'
            style={{ marginBottom: '2rem', color: '#616161' }}
          >
            متن پست:
          </label>

          <Editor id='Editor' setHtmlBody={setHtmlBody} html={data.body} />

          <textarea
            style={{
              width: '100%',
              marginTop: '1.5rem',
              resize: 'none',
              minHeight: '20rem',
            }}
            value={htmlBody}
            disabled
          />
          <Button primary square>
            ویرایش پست
          </Button>
        </Form>
      </Container>
    </UserProfile>
  )
}

const Post_ = styled.div`
  height: calc(100vh - 6.2rem);
`

Edit.getInitialProps = async ctx => {
  isAuthorized(['admin'], ctx)

  const { slug } = ctx.query
  const decoded = encodeURI(slug)

  const {
    data: { data },
  } = await axios.get(`${process.env.BACKEND_URL}/posts/${decoded}`)

  return {
    data,
  }
}

export default Edit
