import { useState, useEffect } from 'react'
import { isAuthorized } from '../../../utils/auth'
import Link from 'next/link'
import Router from 'next/router'
import axios from 'axios'
import styled from 'styled-components'
import changeTimeZone from '../../../utils/time'
import UserProfile from '../../../components/styles/UserProfile'
import Container from '../../../components/styles/Container'
import Grid from '../../../components/styles/Grid'
import ProfileDetails from '../../../components/styles/ProfileDetails'
import Button from '../../../components/styles/Button'
import SideNav from '../../../components/layout/SideNavAdmin'
import Table from '../../../components/styles/Table'
import UpArrowSVG from '../../../images/SVG/up-arrow.svg'
import DownArrowSVG from '../../../images/SVG/down-arrow.svg'
import EditSVG from '../../../images/SVG/pen.svg'
import DeleteSVG from '../../../images/SVG/trash.svg'

import getConfig from 'next/config'
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig()

axios.defaults.withCredentials = true

const PostTitle = ({ title, slug }) => {
  return (
    <Link href={`posts/[slug]`} as={`posts/${slug}`}>
      <a>
        {title.substr(0, 30)}
        {title.length > 30 && '...'}
      </a>
    </Link>
  )
}

const PostDate = ({ date }) => {
  const postDate = changeTimeZone(date)
  return (
    <div>
      <span>{postDate.jDate}</span>&nbsp;
      <span>ساعت </span>
      <span>{postDate.tehranTime}</span>
    </div>
  )
}

const Actions = styled.div`
  display: flex;
  align-items: center;

  & > * {
    margin: 0 0.5rem;
  }

  .status {
    font-size: 1.3rem;
    display: flex;
    justify-content: space-between;
    width: 60%;

    span {
      display: inline-block;
    }

    .spacer {
      flex: 1;
    }

    .status__title {
    }

    .status--published {
      color: green;
      font-style: italic;
      font-size: 1.3rem;
    }
    .status--not-published {
      color: red;
      font-style: italic;
      font-size: 1.3rem;
    }
  }
`
const PostActions = ({ post, publishPost, deletePost }) => {
  return (
    <Actions>
      {post.published === true ? (
        <DownArrowSVG
          className='unpublish'
          width='25px'
          onClick={e => publishPost(e, post)}
        />
      ) : (
        <UpArrowSVG
          className='publish'
          width='25px'
          onClick={e => publishPost(e, post)}
        />
      )}

      <EditSVG
        width='25px'
        onClick={e =>
          Router.push(
            '/admin/posts/edit/[slug]',
            `/admin/posts/edit/${post.slug}`
          )
        }
      />

      <DeleteSVG width='25px' onClick={e => deletePost(e, post)} />
      <div className='status'>
        <span className='status__title'>وضعیت:</span>
        <div className='spacer' />
        {post.published === true ? (
          <span className='status--published'>انتشار یافته</span>
        ) : (
          <span className='status--not-published'>در انتظار بازبینی</span>
        )}
      </div>
    </Actions>
  )
}

const Posts = ({ posts }) => {
  const [loading, setLoading] = useState(false)
  const [articles, setArticles] = useState(posts ?? [])

  const publishPost = async (e, post) => {
    try {
      setLoading(true)

      await axios.patch(`${process.env.BACKEND_URL}/posts/${post._id}`, {
        published: !post.published,
      })

      const a = articles
        .filter(article => article._id === post._id)
        .map(article => {
          article.published = !article.published
          return article
        })
      setArticles(
        articles.map(article => {
          if (article._id === a._id) {
            return a
          }
          return article
        })
      )

      setLoading(false)
    } catch (err) {
      console.log(err.response)
    }
  }

  const deletePost = async (e, post) => {
    try {
      if (confirm('از پاک کردن مطمئن هستید؟')) {
        setLoading(true)
        await axios.delete(`${process.env.BACKEND_URL}/posts/${post._id}`)
        setArticles(articles.filter(article => article._id !== post._id))

        setLoading(false)
      }
    } catch (err) {
      console.log(err.response)
    }
  }

  return (
    <UserProfile>
      <Container justifyContent='flex-start'>
        <Grid gridGap='1rem' templateColumns='1fr 75%'>
          <SideNav />
          <ProfileDetails>
            <div className='profile__title' style={{ margin: '1.5rem 2rem' }}>
              <span>پست‌ها:</span>
            </div>
            <div
              style={{
                margin: '.5rem 2rem',
                display: 'flex',
                flexDirection: 'row-reverse',
              }}
            >
              <Button
                confirm
                square
                href='/admin/posts/create'
                onClick={e => Router.push('/admin/posts/create')}
              >
                <a>پست جدید</a>
              </Button>
            </div>
            <Table columns='40% 25% 1fr'>
              <li className='table__first-row'>
                <span>عنوان پست</span>
                <span>تاریخ انتشار</span>
                <span>عملیات</span>
              </li>
              {articles.map(article => (
                <li className='table__items not-bold' key={article.title}>
                  <PostTitle title={article.title} slug={article.slug} />
                  <PostDate date={article.createdAt} />
                  <PostActions
                    post={article}
                    publishPost={publishPost}
                    deletePost={deletePost}
                  />
                </li>
              ))}
            </Table>
          </ProfileDetails>
        </Grid>
      </Container>
    </UserProfile>
  )
}

Posts.getInitialProps = async ctx => {
  try {
    isAuthorized(['admin'], ctx)
    let posts
    if (ctx.req) {
      const {
        data: { data },
      } = await axios.get(`${serverRuntimeConfig.BACKEND_URL}/posts`)
      posts = data.posts
    } else {
      const {
        data: { data },
      } = await axios.get(`${process.env.BACKEND_URL}/posts`)
      posts = data.posts
    }

    return {
      posts,
    }
  } catch (error) {}
}

export default Posts
