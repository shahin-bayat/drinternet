import styled from 'styled-components'
import Router from 'next/router'
import { isAuthorized } from '../../../utils/auth'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { deleteMessage } from '../../../redux/actions/messageActions'
import UserProfile from '../../../components/styles/UserProfile'
import Container from '../../../components/styles/Container'
import Grid from '../../../components/styles/Grid'
import SideNavAdmin from '../../../components/layout/SideNavAdmin'
import ProfileDetails from '../../../components/styles/ProfileDetails'
import InfoGroup from '../../../components/styles/InfoGroup'
import Button from '../../../components/styles/Button'

const InfoBox = styled.div`
  margin: 2rem 0;
  padding: 0 2rem;
  display: flex;
  flex-direction: column;
  min-height: 40rem;
`

const ButtonGroup = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(15rem, 1fr));
  grid-gap: 2rem;
`

const message = ({ message, deleteMessage }) => {
  const handleDelete = e => {
    e.preventDefault()
    deleteMessage(message._id)
    Router.push('/admin/messages')
  }

  const handleReply = from => {
    Router.push(`/admin/messages/reply/[to]`, `/admin/messages/reply/${from}`)
  }

  return (
    <UserProfile>
      <Container justifyContent='flex-start'>
        <Grid gridGap='1rem' templateColumns='1fr 75%'>
          <SideNavAdmin />
          <ProfileDetails>
            <InfoBox>
              <InfoGroup>
                <p>{message.subject}</p>
                <span>عنوان پیام</span>
              </InfoGroup>
              <InfoGroup>
                <textarea direction='rtl' value={message.body} readOnly />
                <span>متن پیام</span>
              </InfoGroup>
              <ButtonGroup>
                <Button onClick={e => handleReply(message.from)} primary square>
                  پاسخ به پیام
                </Button>

                <Button onClick={handleDelete} warning square>
                  پاک کردن پیام
                </Button>
              </ButtonGroup>
            </InfoBox>
          </ProfileDetails>
        </Grid>
      </Container>
    </UserProfile>
  )
}

message.getInitialProps = async ctx => {
  isAuthorized(['admin'], ctx)
  const { messages } = ctx.store.getState().message
  const message = messages.filter(msg => msg._id === ctx.query.messageId)

  return { message: message[0] }
}

message.propTypes = {
  deleteMessage: PropTypes.func.isRequired,
  message: PropTypes.object.isRequired,
}

export default connect(null, { deleteMessage })(message)
