import { useState } from 'react'
import styled from 'styled-components'
import { sendGroupMessage } from '../../../redux/actions/messageActions'
import { connect } from 'react-redux'
import { isAuthorized } from '../../../utils/auth'
import PropTypes from 'prop-types'
import UserProfile from '../../../components/styles/UserProfile'
import Container from '../../../components/styles/Container'
import Grid from '../../../components/styles/Grid'
import Form from '../../../components/styles/Form'
import InputGroup from '../../../components/styles/InputGroup'
import Button from '../../../components/styles/Button'
import SideNavAdmin from '../../../components/layout/SideNavAdmin'
import ProfileDetails from '../../../components/styles/ProfileDetails'
import CheckBoxGroup from '../../../components/styles/CheckBoxGroup'

const GroupMessage = ({ sendGroupMessage }) => {
  const [message, setMessage] = useState({
    subject: '',
    body: '',
    groupEmail: false,
  })

  const [loading, setLoading] = useState(false)

  const { subject, body, groupEmail } = message

  const handleChange = e => {
    setMessage({ ...message, [e.target.name]: e.target.value })
  }

  const handleCheckboxChange = e =>
    setMessage({ ...message, groupEmail: e.target.checked })

  const handleSubmit = async e => {
    e.preventDefault()
    setLoading(true)
    await sendGroupMessage({ subject, body, groupEmail })
    setLoading(false)
  }
  return (
    <UserProfile>
      <Container justifyContent='flex-start'>
        <Grid gridGap='1rem' templateColumns='1fr 75%'>
          <SideNavAdmin />

          <ProfileDetails>
            <Form onSubmit={handleSubmit}>
              <div className='profile__title'>
                <span>ارسال پیام گروهی :</span>
              </div>

              <InputGroup>
                <input
                  type='text'
                  id='subject'
                  name='subject'
                  value={subject}
                  onChange={handleChange}
                  maxLength={60}
                />
                <label htmlFor='subject'>موضوع</label>
              </InputGroup>

              <InputGroup>
                <textarea
                  id='body'
                  name='body'
                  onChange={handleChange}
                  value={body}
                />
                <label htmlFor='body'>متن پیام</label>
              </InputGroup>

              <CheckBoxGroup>
                <input
                  type='checkbox'
                  id='groupEmail'
                  checked={groupEmail}
                  onChange={handleCheckboxChange}
                />
                <label htmlFor='groupEmail'>
                  <span />
                  به ایمیل هم ارسال شود؟{' '}
                </label>
              </CheckBoxGroup>
              <Button primary square disabled={loading}>
                ارسال پیام گروهی
              </Button>
            </Form>
          </ProfileDetails>
        </Grid>
      </Container>
    </UserProfile>
  )
}

GroupMessage.getInitialProps = async ctx => {
  isAuthorized(['admin'], ctx)
}

GroupMessage.propTypes = {
  sendGroupMessage: PropTypes.func.isRequired,
}

export default connect(null, { sendGroupMessage })(GroupMessage)
