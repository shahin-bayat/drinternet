import { useState, useEffect } from 'react'
import { sendMessageAdmin } from '../../../../redux/actions/messageActions'
import { connect } from 'react-redux'
import { isAuthorized } from '../../../../utils/auth'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import axios from 'axios'
import UserProfile from '../../../../components/styles/UserProfile'
import Container from '../../../../components/styles/Container'
import Form from '../../../../components/styles/Form'
import InputGroup from '../../../../components/styles/InputGroup'
import Button from '../../../../components/styles/Button'
import Grid from '../../../../components/styles/Grid'
import SideNavAdmin from '../../../../components/layout/SideNavAdmin'
import ProfileDetails from '../../../../components/styles/ProfileDetails'

const Reply = ({ userId, currentMessage, sendMessageAdmin }) => {
  const [ticket, setTicket] = useState({
    subject: `پاسخ به <${currentMessage.subject}>`,
    ticketBody: '',
    to: `${userId}`,
    sender: '',
  })
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    const fetcher = async () => {
      const {
        data: {
          data: { data },
        },
      } = await axios.get(`${process.env.BACKEND_URL}/users/${userId}`)
      setTicket({ ...ticket, sender: data })
    }
    fetcher()
  }, [])

  const { subject, ticketBody, to, sender } = ticket

  const handleChange = e => {
    setTicket({ ...ticket, [e.target.name]: e.target.value })
  }

  const handleSubmit = async e => {
    e.preventDefault()
    setLoading(true)
    await sendMessageAdmin({ subject, body: ticketBody, to })
    setLoading(false)
  }
  return (
    <UserProfile>
      <Container justifyContent='flex-start'>
        <Grid gridGap='1rem' templateColumns='1fr 75%'>
          <SideNavAdmin />
          <ProfileDetails>
            <Form onSubmit={handleSubmit}>
              <div className='profile__title'>
                <span>ارسال پاسخ :</span>
              </div>
              <InputGroup dir='rtl'>
                <input
                  type='text'
                  id='to'
                  name='to'
                  value={sender.name}
                  onChange={handleChange}
                  disabled
                />
                <label htmlFor='to'>نام دریافت کننده</label>
              </InputGroup>
              <InputGroup>
                <input
                  type='text'
                  id='subject'
                  name='subject'
                  value={ticket.subject}
                  onChange={handleChange}
                  maxLength={60}
                />
                <label htmlFor='subject'>موضوع</label>
              </InputGroup>
              <InputGroup>
                <textarea
                  id='ticketBody'
                  name='ticketBody'
                  onChange={handleChange}
                  value={ticket.ticketBody}
                />
                <label htmlFor='ticketBody'>متن پاسخ</label>
              </InputGroup>
              <Button primary square disabled={loading}>
                ارسال پاسخ
              </Button>
            </Form>
          </ProfileDetails>
        </Grid>
      </Container>
    </UserProfile>
  )
}

Reply.propTypes = {
  sendMessageAdmin: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  currentMessage: state.message.currentMessage,
})

Reply.getInitialProps = async ctx => {
  isAuthorized(['admin'], ctx)
  const userId = ctx.query.to
  return { userId }
}

export default connect(mapStateToProps, { sendMessageAdmin })(Reply)
