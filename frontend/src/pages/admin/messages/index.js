import styled from 'styled-components'
import { useState } from 'react'
import { isAuthorized } from '../../../utils/auth'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import UserProfile from '../../../components/styles/UserProfile'
import Container from '../../../components/styles/Container'
import Grid from '../../../components/styles/Grid'
import Pagination from '../../../components/layout/Pagination'
import MessageTable from '../../../components/MessageTable'
import SideNavAdmin from '../../../components/layout/SideNavAdmin'
import ProfileDetails from '../../../components/styles/ProfileDetails'

const Messages = ({ messages }) => {
  const [currentPage, setCurrentPage] = useState(1)
  const [postsPerPage, setPostsPerPage] = useState(10)

  const indexOfLastPost = currentPage * postsPerPage
  const indexOfFirstPost = indexOfLastPost - postsPerPage
  const currentPosts = messages.slice(indexOfFirstPost, indexOfLastPost)

  const numberOfPages = Math.ceil(messages.length / postsPerPage)

  return (
    <UserProfile>
      <Container justifyContent='flex-start'>
        <Grid gridGap='1rem' templateColumns='1fr 75%'>
          <SideNavAdmin />
          <ProfileDetails>
            <div className='profile__title' style={{ margin: '1.5rem 2rem' }}>
              <span>پیام‌ها:</span>
            </div>
            <MessageTable results={currentPosts} path='admin' />
            <Pagination
              numberOfPages={numberOfPages}
              currentPage={currentPage}
              setCurrentPage={setCurrentPage}
            />
          </ProfileDetails>
        </Grid>
      </Container>
    </UserProfile>
  )
}

Messages.getInitialProps = async ctx => {
  isAuthorized(['admin'], ctx)
}

Messages.propTypes = {
  messages: PropTypes.array,
}

const mapStateToProps = state => ({
  messages: state.message.messages,
})

export default connect(mapStateToProps)(Messages)
