import styled from 'styled-components'
import Router from 'next/router'
import { isAuthorized } from '../../../utils/auth'

import SideNav from '../../../components/layout/SideNavAdmin'
import Container from '../../../components/styles/Container'
import Grid from '../../../components/styles/Grid'
import ProfileDetails from '../../../components/styles/ProfileDetails'
import Button from '../../../components/styles/Button'
import UserProfile from '../../../components/styles/UserProfile'

const Products = () => {
  return (
    <UserProfile>
      <Container justifyContent='flex-start'>
        <Grid gridGap='1rem' templateColumns='1fr 75%'>
          <SideNav />
          <ProfileDetails>
            <div className='profile__title' style={{ margin: '1.5rem 2rem' }}>
              <span>محصولات:</span>
            </div>
            <div
              style={{
                margin: '.5rem 2rem',
                display: 'flex',
                flexDirection: 'row-reverse',
              }}
            >
              <Button
                confirm
                square
                href='/admin/posts/create'
                onClick={e => Router.push('/admin/products/create')}
              >
                <a>محصول جدید</a>
              </Button>
            </div>
          </ProfileDetails>
        </Grid>
      </Container>
    </UserProfile>
  )
}

Products.getInitialProps = async ctx => {
  isAuthorized(['admin'], ctx)
}

export default Products
