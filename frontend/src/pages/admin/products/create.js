import { useState, useEffect } from 'react'
import styled from 'styled-components'
import Router from 'next/router'
import { isAuthorized } from '../../../utils/auth'
import setToast from '../../../utils/toast'
import axios from 'axios'
import { v4 as uuidv4 } from 'uuid'

import Container from '../../../components/styles/Container'
import Form from '../../../components/styles/Form'
import InputGroup from '../../../components/styles/InputGroup'
import InputGroup2 from '../../../components/styles/InputGroup2'
import Button from '../../../components/styles/Button'
import UserProfile from '../../../components/styles/UserProfile'
axios.defaults.withCredentials = true

const AttributeList = styled.ul`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(10rem, 1fr));
  align-items: center;
  list-style: none;
  color: ${props => props.theme.color_grey_light_3};
  font-size: 1.3rem;
  margin: 2rem 1rem;

  span.attribute-list__delete {
    font-size: 2rem;
    color: ${props => props.theme.color_warning};
    cursor: pointer;
  }

  span.attribute-list__name {
    padding: 1rem;
  }

  span.attribute-list__value {
    padding: 1rem;
  }
`

const Tagslist = styled(AttributeList)``
const ImagesList = styled(AttributeList)``

const Product = ({ categories }) => {
  const [product, setProduct] = useState({
    name: '',
    description: '',
    price: '',
    quantity: '',
    attributes: [],
    tags: [],
    images: [],
  })
  const [category, setCategory] = useState()
  const [subcategory, setSubcategory] = useState()
  const { name, description, price, quantity } = product

  // to set all subcategories based on selected category:
  const [subcategories, setSubcategories] = useState([])
  const [attribute, setAttribute] = useState({ name: '', value: '', id: '' })
  const [tag, setTag] = useState('')
  const [image, setImage] = useState('')

  useEffect(() => {
    const fetcher = async () => {
      const {
        data: {
          data: { subcategories },
        },
      } = await axios.get(
        `${process.env.BACKEND_URL}/products/category/${category}/subcategory`
      )
      setSubcategories(subcategories)
    }
    fetcher()
  }, [category])

  const handleChange = e => {
    setProduct({ ...product, [e.target.name]: e.target.value })
  }

  const handleCategoryChange = e => {
    const cat = categories.filter(cat => cat.name === e.target.value)
    setCategory(cat[0]._id)
  }

  const handleSubcategoryChange = e => {
    const cat = subcategories.filter(cat => cat.name === e.target.value)
    setSubcategory(cat[0]._id)
  }

  const handleAttributeChange = e => {
    setAttribute({ ...attribute, [e.target.name]: e.target.value })
  }

  const handleAddAttribute = e => {
    e.preventDefault()
    if (attribute.name !== '' && attribute.value !== '') {
      attribute.id = uuidv4()
      setProduct({ ...product, attributes: [...product.attributes, attribute] })
    }
    setAttribute({ name: '', value: '' })
  }

  const handleDeleteAttribute = (e, attId) => {
    e.preventDefault()
    const filteredAttributesArray = product.attributes.filter(
      att => attId != att.id
    )
    setProduct({ ...product, attributes: filteredAttributesArray })
  }

  const handleTagChange = e => {
    setTag(e.target.value)
  }

  const handleAddTag = e => {
    e.preventDefault()
    if (tag !== '') {
      const tagCorrect = tag.split(' ').join('_')
      setProduct({ ...product, tags: [...product.tags, tagCorrect] })
    }
    setTag('')
  }

  const handleImageChange = e => {
    setImage(e.target.value)
  }

  const handleAddImage = e => {
    e.preventDefault()
    if (image !== '') {
      setProduct({ ...product, images: [...product.images, image] })
    }
    setImage('')
  }

  const handleSubmit = async e => {
    e.preventDefault()
    // post to backend
    console.log(product)
    const { data } = await axios.post(
      `${process.env.BACKEND_URL}/products`,
      product
    )
    console.log(data)
    setProduct({
      name: '',
      description: '',
      price: '',
      quantity: '',
      attributes: [],
      tags: [],
      images: [],
    })
  }

  return (
    <UserProfile>
      <Container justifyContent='flex-start'>
        <Form onSubmit={handleSubmit} style={{ width: '100%' }}>
          <InputGroup>
            <input
              type='text'
              name='name'
              id='name'
              value={name}
              onChange={handleChange}
              required
            />
            <label htmlFor='name'>نام محصول</label>
          </InputGroup>
          <InputGroup>
            <textarea
              name='description'
              id='description'
              value={description}
              onChange={handleChange}
              required
            />
            <label htmlFor='description'>متن کوتاه معرفی محصول</label>
          </InputGroup>
          <InputGroup dir='ltr'>
            <input
              type='text'
              name='price'
              id='price'
              value={price}
              onChange={handleChange}
              required
            />
            <label htmlFor='price'>قیمت</label>
          </InputGroup>
          <InputGroup dir='ltr'>
            <input
              type='text'
              name='quantity'
              id='quantity'
              onChange={handleChange}
              required
            />
            <label htmlFor='quantity'>تعداد محصول موجود</label>
          </InputGroup>

          <InputGroup>
            <select
              type='text'
              name='category'
              id='category'
              onChange={handleCategoryChange}
              defaultValue={'DEFAULT'}
              required
            >
              <option value='DEFAULT' disabled>
                انتخاب شاخه اصلی...
              </option>
              {categories.map(cat => (
                <option key={cat._id} value={cat.name}>
                  {cat.name}
                </option>
              ))}
            </select>
            <label htmlFor='category'>شاخه اصلی</label>
          </InputGroup>

          <InputGroup>
            <select
              type='text'
              name='subcategory'
              id='subcategory'
              onChange={handleSubcategoryChange}
              defaultValue={'DEFAULT'}
              required
            >
              <option value='DEFAULT' disabled>
                انتخاب زیرشاخه...
              </option>
              {subcategories.map(cat => (
                <option key={cat._id} value={cat.name}>
                  {cat.name}
                </option>
              ))}
            </select>
            <label htmlFor='subcategory'>زیر شاخه</label>
          </InputGroup>

          <InputGroup2>
            <input
              type='text'
              name='name'
              onChange={handleAttributeChange}
              placeholder='عنوان ویژگی'
              value={attribute.name}
            />
            <input
              type='text'
              name='value'
              onChange={handleAttributeChange}
              placeholder='مقدار ویژگی'
              value={attribute.value}
            />
            <button onClick={handleAddAttribute}>اضافه کن</button>
          </InputGroup2>
          <AttributeList>
            {product.attributes.length > 0 &&
              product.attributes.map(att => (
                <li key={att.id}>
                  <span
                    className='attribute-list__delete'
                    value={att.id}
                    onClick={e => handleDeleteAttribute(e, att.id)}
                  >
                    &#215;
                  </span>
                  <span className='attribute-list__name'>{att.name}</span>
                  {':'}
                  <span className='attribute-list__value'> {att.value}</span>
                </li>
              ))}
          </AttributeList>

          <InputGroup2 two>
            <input
              type='text'
              value={tag}
              id='tags'
              onChange={handleTagChange}
              placeholder='تگ'
            />
            <button onClick={handleAddTag}>اضافه کن</button>
          </InputGroup2>
          <Tagslist>
            {product.tags.length > 0 &&
              product.tags.map((tag, idx) => (
                <li key={idx}>
                  <span className='attribute-list__name'>{tag}</span>
                </li>
              ))}
          </Tagslist>

          <InputGroup2 two>
            <input
              type='text'
              value={image}
              id='images'
              onChange={handleImageChange}
              placeholder='عکس'
            />
            <button onClick={handleAddImage}>اضافه کن</button>
          </InputGroup2>
          <ImagesList>
            {product.images.length > 0 &&
              product.images.map((image, idx) => (
                <li key={idx}>
                  <span className='attribute-list__name'>{image}</span>
                </li>
              ))}
          </ImagesList>

          <Button confirm square>
            ارسال محصول
          </Button>
        </Form>
      </Container>
    </UserProfile>
  )
}

Product.getInitialProps = async ctx => {
  isAuthorized(['admin'], ctx)
  const {
    data: {
      data: { categories },
    },
  } = await axios.get(`${process.env.BACKEND_URL}/products/category`)

  return { categories }
}

export default Product
