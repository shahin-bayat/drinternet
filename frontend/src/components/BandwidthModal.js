// ! revised
import { useState, useEffect } from 'react'
import styled from 'styled-components'
import axios from 'axios'
import setToast from '../utils/toast'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import Modal from './styles/Modal'
import Form from './styles/Form'
import InputGroup from '../components/styles/InputGroup'
import Button from '../components/styles/Button'
import CloseButton from './styles/CloseButton'
import Grid from './styles/Grid'

const BandwidthModal_ = styled.div`
  h3 {
    font-size: 2.5rem;
    margin-top: 7.5rem;
    margin-bottom: 2rem;
  }

  p {
    margin-bottom: 2rem;
  }
`

const BandwidthModal = ({ modal, service, setService, closeModal }) => {
  const [contact, setContact] = useState({
    name: '',
    phone: '',
    interface_person: '',
    province: '',
    city: '',
    address: '',
    mobile: '',
    email: '',
    description: '',
  })

  const {
    name,
    phone,
    interface_person,
    province,
    city,
    address,
    mobile,
    email,
    description,
  } = contact

  const [loading, setLoading] = useState(false)
  const handleChange = e => {
    if (e.target.name === 'service') {
      setService(e.target.value)
    } else {
      setContact({ ...contact, [e.target.name]: e.target.value })
    }
  }

  const handleSubmit = async e => {
    try {
      e.preventDefault()
      setLoading(true)

      let additional = {
        interface_person,
        province,
        city,
        address,
        mobile,
        email,
        description,
      }
      additional = JSON.stringify(additional)

      const mail = {
        name,
        service,
        phone,
        additional,
      }
      await axios.post(`${process.env.BACKEND_URL}/mails`, mail)
      setToast(
        'پیام شما با موفقیت ارسال شد. کارشناسان ما در اسرع وقت با شما تماس خواهند گرفت.',
        'success'
      )
      await closeModal()
      // TODO: Remove Fields
      setLoading(false)
    } catch (error) {}
  }

  return (
    <BandwidthModal_>
      <Modal show={modal} minWidth='60%'>
        <div className='modal__content'>
          <CloseButton />
          <h3>درخواست سرویس پهنای باند</h3>

          <Form onSubmit={handleSubmit}>
            {/* نام . نام خانوادگی */}
            <InputGroup>
              <input
                type='text'
                name='name'
                id='name'
                onChange={handleChange}
                value={contact.name}
                required
              />
              <label htmlFor='name'>نام و نام خانوادگی / نام شرکت</label>
            </InputGroup>

            {/* شهر شهرستان */}
            <Grid templateColumns='1fr 1fr' gridGap='0.5rem'>
              <InputGroup>
                <input
                  type='text'
                  name='province'
                  id='province'
                  onChange={handleChange}
                  value={contact.province}
                  required
                />
                <label htmlFor='province'>استان</label>
              </InputGroup>
              <InputGroup>
                <input
                  type='text'
                  name='city'
                  id='city'
                  onChange={handleChange}
                  value={contact.city}
                  required
                />
                <label htmlFor='city'>شهر / شهرستان</label>
              </InputGroup>
            </Grid>

            {/* آدرس */}
            <InputGroup>
              <input
                type='text'
                name='address'
                id='address'
                onChange={handleChange}
                value={contact.address}
                required
              />
              <label htmlFor='address'>آدرس پستی</label>
            </InputGroup>

            {/* phone - mobile */}
            <Grid templateColumns='1fr 1fr' gridGap='0.5rem'>
              <InputGroup direction='ltr'>
                <input
                  type='text'
                  name='phone'
                  id='phone'
                  onChange={handleChange}
                  value={contact.phone}
                  minLength={10}
                  required
                />
                <label htmlFor='phone'>
                  تلفن<span>(به همراه پیش شماره)</span>
                </label>
              </InputGroup>
              <InputGroup direction='ltr'>
                <input
                  type='text'
                  name='mobile'
                  id='mobile'
                  onChange={handleChange}
                  value={contact.mobile}
                  minLength={10}
                  required
                />
                <label htmlFor='mobile'>موبایل</label>
              </InputGroup>
            </Grid>

            {/* سرویس  */}
            <InputGroup>
              <select
                type='text'
                name='service'
                id='service'
                value={service}
                onChange={handleChange}
                required
              >
                <option value='Ded-99-110'>1 تا 10 Mbps</option>
                <option value='Ded-99-102'>10 تا 20 Mbps</option>
                <option value='Ded-99-203'>20 تا 30 Mbps</option>
                <option value='Ded-99-304'>30 تا 40 Mbps</option>
                <option value='Ded-99-408'>40 تا 80 Mbps</option>
                <option value='Ded-99-800'>80 Mbps به بالا</option>
              </select>
              <label htmlFor='service'>انتخاب سرویس</label>
            </InputGroup>

            <Grid templateColumns='repeat(2, 1fr)'>
              {/* رابط */}
              <InputGroup>
                <input
                  type='text'
                  name='interface_person'
                  id='interface_person'
                  onChange={handleChange}
                  value={contact.interface_person}
                  required
                />
                <label htmlFor='interface_person'>نام شخص رابط</label>
              </InputGroup>

              {/* email*/}
              <InputGroup direction='ltr'>
                <input
                  type='email'
                  name='email'
                  id='email'
                  onChange={handleChange}
                  value={contact.email}
                />
                <label htmlFor='email'>
                  ایمیل<span>(اختیاری)</span>
                </label>
              </InputGroup>
            </Grid>

            <InputGroup minHeight='2rem'>
              <textarea
                label='description'
                name='description'
                id='description'
                onChange={handleChange}
                value={contact.description}
              ></textarea>
              <label htmlFor='description'>
                توضیحات<span>(اختیاری)</span>
              </label>
            </InputGroup>

            <Button primary round moving disabled={loading}>
              ثبت درخواست
            </Button>
          </Form>
        </div>
      </Modal>
    </BandwidthModal_>
  )
}

const mapStateToProps = state => {
  return {
    modal: state.modal.filter(_ => _.page === 'bandwidth')[0].modal,
  }
}

BandwidthModal.propTypes = {
  modal: PropTypes.bool.isRequired,
  // closeModal: PropTypes.func.isRequired,
}

export default connect(mapStateToProps)(BandwidthModal)
