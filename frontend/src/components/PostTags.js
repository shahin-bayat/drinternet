// ! revised
import styled from "styled-components"

const PostTags_ = styled.div`
  font-size: 1.4rem;
  color: ${props => props.theme.color_primary};
  margin: 2rem 0;

  .tag {
    display: inline-block;
    background-color: ${props => props.theme.color_primary};
    color: ${props => props.theme.color_grey_light};
    padding: 0.5rem 1rem;
    font-size: 1.2rem;
    border-radius: 10rem;
    margin-right: 0.5rem;
  }
`

const PostTags = ({ tags }) => {
  return (
    <PostTags_>
      برچسب‌ها:
      {tags.map((tag, idx) => {
        return (
          <span className='tag' key={idx}>
            {tag}
          </span>
        )
      })}
    </PostTags_>
  )
}

export default PostTags
