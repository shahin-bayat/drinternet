// ! revised
import { useState, useEffect } from "react"
import styled from "styled-components"
import axios from "axios"

import Pagination from "../components/layout/Pagination"
import PostItem from "./PostItem"

const BlogList = ({ posts, numberOfPages }) => {
  const [currentPage, setCurrentPage] = useState(1)
  const [newPosts, setNewPosts] = useState()

  useEffect(() => {
    const fetcher = async () => {
      const {
        data: {
          data: { posts }
        }
      } = await axios.get(
        `${process.env.BACKEND_URL}/posts?page=${currentPage}`
      )
      setNewPosts(posts)
    }

    fetcher()
  }, [currentPage])

  let postsList
  if (currentPage === 1) {
    postsList = posts.map(post => <PostItem key={post._id} post={post} />)
  } else {
    postsList = newPosts.map(post => <PostItem key={post._id} post={post} />)
  }

  return (
    <BlogList_>
      <div className='blog-list__title'>
        <p>آخرین مطالب</p>
      </div>
      {postsList}
      <Pagination
        numberOfPages={numberOfPages}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
      />
    </BlogList_>
  )
}

const BlogList_ = styled.div`
  padding: 0 2rem;
  display: flex;
  flex-direction: column;

  .blog-list__title {
    padding: 2rem;
    background: ${props => props.theme.color_grey_light};

    p {
      color: ${props => props.theme.color_black};
      font-size: 1.6rem;
    }
  }
`

export default BlogList
