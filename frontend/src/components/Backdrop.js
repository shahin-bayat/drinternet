// ! revised
// not used
import styled from "styled-components"

const Backdrop_ = styled.div`
  position: fixed;
  top: 6rem;
  right: 0;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.7);
  overflow-y: hidden;
`

const Backdrop = ({ setIsDrawerOpen }) => {
  const closeSideDrawer = () => {
    setIsDrawerOpen(false)
  }
  return <Backdrop_ onClick={closeSideDrawer} />
}

export default Backdrop
