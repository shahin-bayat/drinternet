// ! revised
import styled from 'styled-components'

const SectionHeader = styled.header`
  text-align: ${props => props.textAlign || 'center'};
  margin: 4rem 0;
  padding: 0 2rem;

  h2,
  h3 {
    margin: 2rem 0;
    color: ${props => props.white && props.theme.color_white};
  }
  p {
    text-align: center;
    color: ${props => props.white && props.theme.color_white};
  }

  @media (max-width: ${props => props.theme.bp_medium}) {
    p {
      width: 100%;
    }
  }
`

export default SectionHeader
