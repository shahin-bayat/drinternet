// ! revised
import styled from 'styled-components'

const Table = styled.ul`
  list-style: none;
  display: grid;
  grid-template-columns: 1fr;
  border: ${props => props.theme.border};
  margin: 2rem;

  li.table__first-row {
    width: 100%;
    display: grid;
    align-items: center;
    grid-template-columns: ${props =>
      props.columns ? props.columns : '30% 50% 1fr'};
    border-bottom: ${props => props.theme.border};
    padding: 1rem 2rem;

    span {
      font-size: 1.4rem;
      color: ${props => props.theme.color_black};
    }
  }

  li.table__items {
    width: 100%;
    display: grid;
    align-items: center;
    grid-template-columns: ${props =>
      props.columns ? props.columns : '30% 50% 1fr'};
    padding: 1rem 2rem;
    border-bottom: ${props => props.theme.border};
    cursor: pointer;
    margin: 0;
    background-color: ${props => props.theme.color_grey_light};

    &:last-child {
      border-bottom: none;
    }

    &.not-read {
      background-color: ${props => props.theme.color_white};
      font-weight: bold;
    }

    &.not-bold {
      background-color: ${props => props.theme.color_white};
    }

    span {
      font-size: 1.4rem;
      color: ${props => props.theme.color_black};
    }

    a {
      text-decoration: none;
      color: ${props => props.theme.color_black};
    }
  }
`

export default Table
