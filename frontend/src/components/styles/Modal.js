// ! revised
// props: show
import styled from 'styled-components'

const Modal = styled.div.attrs(props => ({
  minWidth: props.minWidth || '',
}))`
  /* dark background */
  display: ${props => (props.show ? 'block' : 'none')};
  position: fixed;
  top: 0;
  right: 0;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.7);
  z-index: 10;
  overflow: hidden;
  .modal__content {
    /* main window */
    min-width: ${props => props.minWidth};
    display: block;
    position: absolute;
    top: 50%;
    right: 50%;
    transform: translate(50%, -50%);
    padding: 0 3rem;
    background: ${props => props.theme.color_white};
    border-radius: ${props => props.theme.border_radius};
    box-shadow: ${props => props.theme.box_shadow};
    z-index: 20;
    overflow: hidden;
    @media (max-width: ${props => props.theme.bp_medium}) {
      min-width: 70%;
    }

    @media (max-width: ${props => props.theme.bp_small}) {
      min-width: 90%;
      overflow-y: scroll;
      height: ${props => props.bpSmallHeight || '100%'};
    }
  }
`

export default Modal
