import styled from 'styled-components'

const Profile_ = styled.section`
  background: ${props => props.theme.color_grey_light};
  padding: 0 2rem;
  padding-top: 10rem;
  min-height: 100vh;
`

export default Profile_
