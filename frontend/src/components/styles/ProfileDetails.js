// ! revised
import styled from "styled-components"

const ProfileDetails = styled.div`
  background: ${props => props.theme.color_white};
  border: ${props => props.theme.border};
  padding: 0;
  box-shadow: ${props => props.theme.box_shadow_light};

  div.profile__title {
    width: 100%;
    /* padding: 1rem 0; */
    margin: 1.5rem 0;

    span {
      font-size: 1.5rem;
      color: ${props => props.theme.color_grey};
    }
  }
`

export default ProfileDetails
