// ! revised
// side-by-side inputs in product create
import styled from "styled-components"

const InputGroup2 = styled.div`
  display: grid;
  grid-template-columns: ${props => (props.two ? "4fr 1fr" : "2fr 2fr 1fr")};
  grid-gap: 1rem;
  color: ${props => props.theme.color_grey};

  input {
    direction: ${props => props.direction};
    width: 100%;
    padding: 1.5rem 1rem;
    border: ${props => props.theme.border};
    transition: all 0.2s ease;
    font-family: inherit;
    resize: none;

    &:focus {
      outline: none;
      border: 1px solid ${props => props.theme.color_primary};
    }
    ::placeholder {
      direction: ${props => props.direction};
      color: ${props => props.theme.color_grey};
      opacity: 0.5;
    }
  }

  button {
    border: none;
    cursor: pointer;
    padding: 1rem 3rem;
    font-family: inherit;
  }
`

export default InputGroup2
