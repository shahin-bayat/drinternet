// ! revised
import styled from "styled-components"

const InfoGroup = styled.div`
  position: relative;
  margin: 1.5rem 0;
  color: ${props => props.theme.color_grey};

  p,
  textarea {
    direction: ${props => props.direction};
    width: 100%;
    padding: 1.5rem 1rem;
    border: ${props => props.theme.border};
    font-family: inherit;
    line-height: 1.6;
  }

  span {
    display: block;
    position: absolute;
    right: 1.25rem;
    top: 0;
    transform: translateY(-50%);
    padding: 0 0.75rem;
    background-color: #fff;
    color: ${props => props.theme.color_grey};
  }

  textarea {
    min-height: 30rem;
    resize: none;
  }
`

export default InfoGroup
