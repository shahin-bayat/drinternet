// ! revised
import styled from 'styled-components'
// props.noHover => no hover effect

const Card = styled.div.attrs(props => ({
  alignItems: props.alignItems || 'center',
  justifyContent: props.justifyContent || 'flex-start',
  padding: props.padding || '2rem',
}))`
  display: flex;
  flex-direction: column;
  align-items: ${props => props.alignItems};
  justify-content: ${props => props.justifyContent};
  color: ${props => props.theme.color_grey};
  background: ${props => props.theme.color_white};
  border: ${props => props.theme.border};
  border-radius: ${props => props.theme.border_radius};
  transition: all 0.2s ease-in;
  padding: ${props => props.padding};

  @media (max-width: ${props => props.theme.bp_smallest}) {
    padding: 2rem 4rem;
  }

  h2 {
    font-size: 2.5rem;
    line-height: 1.6;
    color: ${props => props.theme.color_grey};
  }

  h3 {
    font-size: 1.6rem;
    line-height: 1.6;
    color: ${props => props.theme.color_grey};
  }
  h3.continue-reading {
    text-align: justify;
  }

  svg {
    height: 6rem;
    width: auto;
    margin-bottom: 1rem;
    fill: ${props => props.theme.color_grey};
  }

  p.flash-text {
    transform: translate(0, 2rem);
    text-align: center;
    opacity: 0;
    transition: all 0.5s;
    backface-visibility: hidden;
    margin: 0;
    margin-top: 0.5rem;
  }

  :hover {
    box-shadow: ${props => (props.noHover ? '' : props.theme.box_shadow)};
    cursor: ${props => (props.noHover ? '' : 'pointer')};

    p.flash-text {
      opacity: 1;
      transform: translate(0, 0);
    }

    h2,
    h3 {
      color: ${props => (props.noHover ? '' : props.theme.color_primary)};
    }

    button {
      color: ${props => (props.noHover ? '' : props.theme.color_primary)};
    }
  }

  img {
    height: 8rem;
    width: 8rem;
    margin: 2rem 0;
  }

  p {
    width: 100%;
    text-align: justify;
    text-justify: auto;
    margin: 1.5rem 0;
    font-size: 1.3rem;
  }

  span {
    color: ${props => props.theme.color_grey_light_3};
  }

  img.post-image {
    border: ${props => props.theme.border};
    height: auto;
    width: 100%;
  }

  a.continue-reading {
    text-decoration: none;
    color: ${props => props.theme.color_primary};
    font-size: 1.4rem;
    font-weight: bold;
  }
`

export default Card
