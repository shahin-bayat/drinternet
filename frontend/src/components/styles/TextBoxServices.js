import styled from 'styled-components'

const TextBox = styled.div`
  display: grid;
  justify-items: start;
  align-items: start;
  grid-template-columns: 1fr;
  grid-gap: 3rem;
  align-content: center;
  padding: 2rem 0;

  @media (max-width: ${props => props.theme.bp_medium}) {
    justify-items: center;
    grid-gap: 1.5rem;

    h1 {
      text-align: center;
    }

    p {
      width: 80%;
      text-align: center;
    }
  }

  @media (max-width: ${props => props.theme.bp_smallest}) {
    margin-bottom: 3rem;
    p {
      width: 100%;
      text-align: justify;
    }
  }
`

export default TextBox
