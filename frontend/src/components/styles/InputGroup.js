// ! revised
import styled from 'styled-components'

const InputGroup = styled.div.attrs(props => ({
  minHeight: props.minHeight || '30rem',
}))`
  position: relative;
  margin: 1.5rem 0;
  color: ${props => props.theme.color_grey};

  input,
  textarea,
  select {
    direction: ${props => props.direction};
    width: 100%;
    padding: 1.5rem 1rem;
    border: ${props => props.theme.border};
    transition: all 0.2s ease;
    font-family: inherit;
    resize: none;

    &:focus {
      outline: none;
      border: 1px solid ${props => props.theme.color_primary};

      & ~ label {
        color: ${props => props.theme.color_primary};
      }
    }

    &:focus:invalid {
      border: 1px solid ${props => props.theme.color_warning};

      & ~ label {
        color: ${props => props.theme.color_warning};
      }
    }

    &:focus:valid {
      border: 1px solid ${props => props.theme.color_valid};

      & ~ label {
        color: ${props => props.theme.color_valid};
      }
    }

    ::placeholder {
      direction: ${props => props.direction};
      color: ${props => props.theme.color_grey};
      opacity: 0.5;
    }

    :disabled {
      background-color: ${props => props.theme.color_white};
      color: ${props => props.theme.color_grey};
      font-style: italic;
    }
  }

  textarea {
    min-height: ${props => props.minHeight};
  }

  label {
    position: absolute;
    right: 1.25rem;
    top: 0;
    transform: translateY(-50%);
    padding: 0 0.75rem;
    background-color: #fff;
    color: ${props => props.theme.color_grey};

    span {
      font-size: 1rem;
    }
  }
`

export default InputGroup
