import styled from 'styled-components'

const Callout = styled.div.attrs(props => ({
  flexDirection: props.flexDirection || 'column',
}))`
  display: flex;
  align-items: center;
  flex-direction: ${props => props.flexDirection};
  svg {
    width: ${props => (props.xl ? '15rem' : '5rem')};
    width: ${props => props.big && '8rem'};
    height: auto;
    margin: ${props => (props.flexDirection === 'row' ? '1rem' : '0')};
  }

  p {
    width: 85%;
    font-size: 1.4rem;
    margin: ${props => (props.flexDirection === 'column' ? '2rem 0' : '0')};
    text-align: center;
  }

  h4 {
    font-size: 1.8rem;
    margin-top: 2rem;
    color: ${props => props.theme.color_grey};
  }
`

export default Callout
