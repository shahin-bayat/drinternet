// ! revised
// fixed, main page
import styled from 'styled-components'

const TextBox = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;

  width: 45%;
  position: absolute;
  top: 13%;
  left: 10%;
  padding: 2rem;
  border-radius: 5rem;

  @media (max-width: ${props => props.theme.bp_largest}) {
    width: 100%;
    top: 20%;
    left: 50%;
    transform: translateX(-50%);
    align-items: center;
    h1 {
      text-align: center;
    }
  }

  @media (max-width: ${props => props.theme.bp_smallest}) {
    top: 30%;
  }

  h1 {
    font-weight: 400;
    color: ${props => props.theme.color_primary};
    margin-bottom: 2rem;
  }
`

export default TextBox
