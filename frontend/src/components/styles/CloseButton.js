// ! revised
import styled from 'styled-components'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { closeModal } from '../../redux/actions/modalActions'

const Close = styled.div`
  position: absolute;
  left: 2rem;
  top: 2rem;
  width: 5rem;
  height: 5rem;
  cursor: pointer;

  :hover .leftright {
    transform: rotate(-45deg);
    background-color: ${props => props.theme.color_warning};
  }
  :hover .rightleft {
    transform: rotate(45deg);
    background-color: ${props => props.theme.color_warning};
  }
  :hover label {
    opacity: 1;
  }

  .leftright {
    height: 3px;
    width: 4rem;
    position: absolute;
    margin-top: 2.4rem;
    background-color: ${props => props.theme.color_primary};
    border-radius: 2px;
    transform: rotate(45deg);
    transition: all 0.3s ease-in;
  }

  .rightleft {
    height: 3px;
    width: 4rem;
    position: absolute;
    margin-top: 2.4rem;
    background-color: ${props => props.theme.color_primary};
    border-radius: 2px;
    transform: rotate(-45deg);
    transition: all 0.3s ease-in;
  }
`

const CloseButton = ({ closeModal }) => {
  return (
    <Close className='CloseButton' onClick={async e => closeModal()}>
      <div className='CloseButton leftright'></div>
      <div className='CloseButton rightleft'></div>
    </Close>
  )
}

CloseButton.propTypes = {
  closeModal: PropTypes.func.isRequired,
}

export default connect(state => state, { closeModal })(CloseButton)
