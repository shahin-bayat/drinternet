// ! revised
import styled from "styled-components"

const CheckBoxGroup = styled.div`
  display: flex;
  align-items: center;

  label {
    margin-right: 1rem;
    color: ${props => props.theme.color_warning};
  }
`

export default CheckBoxGroup
