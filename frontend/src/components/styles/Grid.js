// ! revised
// nomedia: no media queries
import styled from 'styled-components'

const Grid = styled.div.attrs(props => ({
  templateColumns: props.templateColumns,
  gridGap: props.gridGap || '2.5rem',
  justifyItems: props.justifyItems || 'stretch',
  alignItems: props.alignItems || 'stretch',
  alignContent: props.alignContent || 'start',
  justifyContent: props.justifyContent || 'start',
  bp_medium: props.bp_medium || '',
  bp_small_row: props.bp_small_row || '',
  width: props.width || '100%',
}))`
  display: grid;
  grid-template-columns: ${props => props.templateColumns};
  justify-items: ${props => props.justifyItems};
  align-items: ${props => props.alignItems};
  align-content: ${props => props.alignContent};
  justify-content: ${props => props.justifyContent};
  grid-gap: ${props => props.gridGap};
  width: ${props => props.width};

  @media (max-width: ${props => props.theme.bp_medium}) {
    grid-template-columns: ${props => !props.nomedia && props.bp_medium};
  }
  @media (max-width: ${props => props.theme.bp_small}) {
    grid-template-columns: ${props => !props.nomedia && '1fr'};
    grid-template-rows: ${props => props.bp_small_row};
  }
`

export default Grid
