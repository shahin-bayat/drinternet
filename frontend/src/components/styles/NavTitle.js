// ! revised
import styled from 'styled-components'

const NavTitle = styled.div`
  width: 100%;
  padding: 1rem 2rem;
  border-bottom: ${props => props.theme.border};

  @media (max-width: ${props => props.theme.bp_medium}) {
    display: ${props => (props.invisible ? 'none' : 'block')};
  }

  span {
    font-size: 1.5rem;
    color: ${props => props.theme.color_grey};
  }

  .nav-title--big {
    color: ${props => props.theme.color_primary};
    font-size: 1.6rem;
  }
`

export default NavTitle
