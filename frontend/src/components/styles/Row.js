import styled from 'styled-components'

const Row = styled.div`
  padding: 3rem 2rem;
  background-color: ${props => props.theme.color_grey_light};
  margin-bottom: 4rem;
`

export default Row
