// ! revised
import styled from "styled-components"

const SideNav = styled.nav`
  display: flex;
  flex-direction: column;
  background: ${props => props.theme.color_white};
  border: ${props => props.theme.border};
  box-shadow: ${props => props.theme.box_shadow_light};

  .side-nav__messages {
    color: ${props => props.theme.color_primary};
    font-weight: bold;
    float: left;
  }
`

export default SideNav
