// ! revised
import styled from "styled-components"

const HR = styled.hr`
  width: 100%;
  border: 0.5px solid #e0e0e2;
`

export default HR
