// ! revised
import styled from "styled-components"

const AccountBox = styled.div`
  width: 40rem;
  background-color: ${props => props.theme.color_white};
  color: ${props => props.theme.color_grey};
  border: ${props => props.theme.border};
  box-shadow: ${props => props.theme.box_shadow_light};

  h3 {
    margin: 3rem 2rem;
    font-size: 1.7rem;
  }

  a {
    text-decoration: none;
    color: ${props => props.theme.color_primary};
    border-bottom: 1px dashed ${props => props.theme.color_primary};
  }

  div.account-box__subscript {
    display: flex;
    align-items: center;
    justify-content: flex-end;
    margin: 1rem 0;

    a {
      font-size: 1.3rem;
    }
  }

  div.account-box__footer {
    padding: 3rem 1rem;
    background-color: ${props => props.theme.color_green};
    text-align: center;
  }
`

export default AccountBox
