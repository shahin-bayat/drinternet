// ! revised
import styled from "styled-components"

const RadioGroup = styled.div`
  display: inline-block;

  input[type="radio"] {
    display: none;

    &:checked ~ label span::after {
      opacity: 1;
    }
  }
  label {
    cursor: pointer;
    position: relative;
    padding-right: 4.5rem;
    display: flex;
    align-items: center;
    display: flex;
    align-items: center;
    color: ${props => props.theme.color_grey};

    span {
      height: 3rem;
      width: 3rem;
      border: 4px solid ${props => props.theme.color_primary};
      border-radius: 50%;
      display: inline-block;
      position: absolute;
      right: 0;
      top: -0.4rem;

      &::after {
        content: "";
        display: block;
        height: 1.3rem;
        width: 1.3rem;
        border-radius: 50%;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        background-color: ${props => props.theme.color_primary};
        opacity: 0;
        transition: opacity 0.2s;
      }
    }
  }
`

export default RadioGroup
