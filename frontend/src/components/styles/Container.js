// ! revised
import styled from "styled-components"

const Container = styled.div.attrs(props => ({
  justifyContent: props.justifyContent || "center",
  alignItems: props.alignItems || "flex-start"
}))`
  display: flex;
  flex-direction: column;
  justify-content: ${props => props.justifyContent};
  align-items: ${props => props.alignItems};
  max-width: ${props => props.theme.maxWidth};
  margin: 0 auto;
  height: 100%;
`

export default Container
