// ! revised
// Page Headers
import styled from 'styled-components'

const Header = styled.header.attrs(props => ({
  background:
    props.background ||
    'linear-gradient(90deg,#396afc 0%,#2948ff 33%,#2948ff 66%,#396afc)',
  height: props.height || '100vh',
  backgroundImage: props.img || '',
  backgroundImage2: props.img2 || '',
  minHeight: props.minHeight,
}))`
  min-height: ${props => props.minHeight};
  height: ${props => props.height};
  margin: 0 auto;
  position: relative;
  background: ${props =>
    props.img ? `url(${props.backgroundImage})` : props.background};
  background-size: cover;
  background-repeat: no-repeat;
  background-position: bottom left;

  @media (max-width: ${props => props.theme.bp_smallest}) {
    display: flex;
    align-items: center;
    justify-content: center;
    background: ${props =>
      props.img2 ? `url(${props.backgroundImage2})` : props.background};
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center center;
  }

  h1,
  h2 {
    color: ${props =>
      props.light ? props.theme.color_white : props.theme.color_grey};
    min-width: 0;
    overflow: hidden;
    font-weight: 600;
  }
  p {
    color: ${props =>
      props.light ? props.theme.color_white : props.theme.color_grey};
    min-width: 0;
    overflow: hidden;
    text-align: justify;
  }

  button {
    margin: 1rem 0;
  }

  .hero__image {
    width: 100%;
    justify-self: center;
    @media (max-width: ${props => props.theme.bp_medium}) {
      width: 75%;
    }

    @media (max-width: ${props => props.theme.bp_small}) {
      width: 100%;
    }
  }
`

export default Header
