// ! revised
import styled from "styled-components"
/*
background-color: primary, secondary, info, transparent, warning, confirm
color: primary, secondary, info, warning
border-radius: round, square
box-shadow: shadowed
translateY : fixed, moving
scale: fixed, moving,
*/

const Button = styled.button`
  &,
  &:link,
  &:visited {
    display: flex;
    position: relative;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    margin: 2rem 0;
    padding: 1rem 3rem;
    font-family: inherit;
    cursor: pointer;
    transition: all 0.2s ease;
    border: none;

    background-color: ${props => {
      if (props.primary) return props.theme.color_primary
      if (props.secondary || props.info) return props.theme.color_grey_light
      if (props.confirm) return props.theme.color_confirm
      if (props.warning) return props.theme.color_warning
      if (props.transparent) return "transparent"
    }};
    color: ${props => {
      if (props.primary || props.warning || props.confirm)
        return props.theme.color_white
      if (props.secondary || props.info) return props.theme.color_black
    }};
    border-radius: ${props => {
      if (props.round) return "10rem"
      if (props.square) return "5px"
    }};

    > * {
      margin-left: 1rem;
    }
  }

  &:disabled {
    background-color: ${props => props.theme.color_grey_light_2};
    color: ${props => props.theme.color_warning};
    cursor: auto;
  }

  &:active,
  &:focus {
    outline: none;
    transform: ${props => {
      if (props.fixed) return "undefined"
      if (props.moving) return "translateY(-1px)"
    }};
    box-shadow: ${props => {
      if (props.shadowed) return props.theme.box_shadow
    }};
  }

  &::after {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    z-index: -1;
    transition: all 0.4s;
    height: 100%;
    width: 100%;
    background-color: ${props => {
      if (props.primary) return props.theme.color_primary
      if (props.secondary) return props.theme.color_grey_light
    }};
    display: ${props => {
      if (props.fixed) return "none"
      if (props.moving) return "inline-block;"
    }};
    border-radius: ${props => {
      if (props.round) return "10rem"
      if (props.square) return "1rem"
    }};
  }

  &:hover {
    transform: ${props => {
      if (props.fixed) return "undefined"
      if (props.moving) return "translateY(-1px)"
    }};
    box-shadow: ${props => {
      if (props.shadowed) return props.theme.box_shadow
    }};

    &::after {
      transform: ${props => {
        if (props.fixed) return "undefined"
        if (props.moving) return "scaleX(1.4) scaleY(1.6)"
      }};
      opacity: 0;
    }
  }
`

export default Button
