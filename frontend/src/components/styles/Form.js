// ! revised
import styled from "styled-components"
const Form = styled.form`
  padding: 0 2rem;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`

export default Form
