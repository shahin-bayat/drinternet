import { useState, useEffect } from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import axios from 'axios'
import setToast from '../utils/toast'
import Router from 'next/router'

import Comment from './Comment'
import Form from './styles/Form'
import InputGroup from './styles/InputGroup'
import Button from './styles/Button'
import LoginModal from './LoginModal'

axios.defaults.withCredentials = true

const Comments_ = styled.div`
  margin: 2rem 0;
  form.comments__compose,
  form.comments-toggle {
    padding: 0;
  }

  .comments__title {
    color: ${props => props.theme.color_grey};
    font-size: 2.5rem;
  }

  .comments__count {
    font-size: 1.6rem;
    font-weight: bold;
    margin-top: 1.5rem;
    padding: 0 2rem;

    span {
      font-size: 2rem;
      color: ${props => props.theme.color_valid};
    }
  }
`
const Comments = ({ comments, user, post }) => {
  const [comment, setComment] = useState({
    body: '',
    post: '',
  })

  const [showComments, setShowComments] = useState(false)
  const [showLogin, setShowLogin] = useState(false)

  const closeModal = e => {
    if (
      e.target.className.startsWith('Modal') ||
      e.target.className.startsWith('CloseButton')
    ) {
      setShowLogin(false)
    }
  }

  useEffect(() => {
    setComment({ ...comment, post: post._id })
  }, [])

  const handleChange = e => {
    setComment({ ...comment, [e.target.name]: e.target.value })
  }

  const toggleComments = e => {
    e.preventDefault()
    setShowComments(!showComments)
  }

  const handleSubmit = async e => {
    e.preventDefault()

    if (!user) {
      // open modal for login
      console.log('you are not logged in')
      setShowLogin(true)
    } else {
      if (!comment.body) {
        setToast('لطفا دیدگاه خود را وارد نمایید', 'fail', 3000)
      } else {
        const data = await axios.post(
          `${process.env.BACKEND_URL}/comments`,
          comment
        )
        setComment({ body: '', post: '' })
        setToast('دیدگاه شما با موفقیت ثبت شد', 'success', 1000)
        setTimeout(() => {
          Router.push(`/blog/${post.slug}`).then(() => window.scrollTo(0, 0))
        }, 1000)
      }
    }
  }
  return (
    <Comments_>
      <h2 className='comments__title'>دیدگاه‌ها:</h2>
      <p className='comments__count'>
        <span>{post.numComments}</span> دیدگاه
      </p>
      <Form className='comments__compose' onSubmit={handleSubmit}>
        <InputGroup dir='rtl'>
          <textarea
            type='text'
            name='body'
            id='comment'
            value={comment.body}
            onChange={handleChange}
            placeholder='دیدگاه خود را وارد کنید...'
            style={{ minHeight: '20rem' }}
            maxLength={1500}
          />
        </InputGroup>
        <Button primary square stretch>
          ارسال دیدگاه
        </Button>
      </Form>
      <LoginModal
        showLogin={showLogin}
        onClick={closeModal}
        setShowLogin={setShowLogin}
      />
      <Form className='comments-toggle'>
        <Button secondary square stretch onClick={toggleComments}>
          نمایش دیدگاه‌ها
        </Button>
      </Form>
      {showComments && (
        <div className='comments__list'>
          {comments.map(comment => (
            <Comment comment={comment} key={comment._id} />
          ))}
        </div>
      )}
    </Comments_>
  )
}

Comments.propTypes = {
  user: PropTypes.object,
}

const mapStateToProps = state => ({
  user: state.authentication.user,
})

export default connect(mapStateToProps)(Comments)
