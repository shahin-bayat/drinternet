// ! revised
import { useState, useEffect } from 'react'
import DatePicker from 'react-modern-calendar-datepicker'

const DatePick = ({ setBirthday, current = null }) => {
  const [date, setDate] = useState(current)
  useEffect(() => {
    // setting parent birthday
    setBirthday(date)
  }, [date])

  return (
    <DatePicker
      value={date}
      onChange={setDate}
      shouldHighlightWeekends
      inputPlaceholder='انتخاب...'
      locale='fa'
      calendarClassName='calendar'
      inputClassName='custom-input'
      wrapperClassName='custom-wrapper'
    />
  )
}

function dateObjectToString(dateObject) {
  const { day, month, year } = dateObject
  return `${year}/${month}/${day} 00:00`
}

export default DatePick
