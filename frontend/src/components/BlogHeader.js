// ! revised
import styled from 'styled-components'
import SideNavBlog from './layout/SideNavBlog'
import Router from 'next/router'

const BlogHeader = ({ highlights }) => {
  const handleClick = slug => {
    Router.push(`/blog/[slug]`, `/blog/${slug}`)
  }
  const thumbnails = highlights.map((post, idx) => (
    <div
      className={`box box-${idx}`}
      onClick={e => handleClick(post.slug)}
      key={post._id}
    >
      <img className='box-image' src={post.postImage} alt={post.title} />
      <div className='box-overlay' />
      <div className='box-title'>
        <p>{post.title}</p>
      </div>
    </div>
  ))

  return (
    <BlogHeader_>
      <SideNavBlog />
      <BlogHighlights>{thumbnails}</BlogHighlights>
    </BlogHeader_>
  )
}

const BlogHeader_ = styled.header`
  display: grid;
  grid-gap: 1rem;
  grid-template-columns: 25% 1fr;
  padding: 2rem 2rem;

  @media (max-width: ${props => props.theme.bp_medium}) {
    grid-template-columns: 1fr;
  }
`

const BlogHighlights = styled.div`
  display: grid;
  grid-template-columns: repeat(3, minmax(20rem, 1fr));
  grid-template-areas:
    'box-0 box-0 box-1'
    'box-0 box-0 box-2'
    'box-3 box-4 box-2';
  background: ${props => props.theme.color_white};
  border: ${props => props.theme.border};
  padding: 0;
  grid-gap: 1px;
  box-shadow: ${props => props.theme.box_shadow_light};
  min-height: 60vh;

  @media (max-width: ${props => props.theme.bp_small}) {
    grid-template-columns: 1fr;
    grid-template-areas:
      'box-0'
      'box-1'
      'box-2'
      'box-3'
      'box-4';
  }

  .box {
    position: relative;
    :hover {
      cursor: pointer;

      .box-overlay {
        opacity: 0;
      }

      .box-title {
        p {
          opacity: 1;
          color: ${props => props.theme.color_primary};
        }
      }
    }
  }

  .box-overlay {
    position: absolute;
    height: 100%;
    width: 100%;
    background: ${props => props.theme.color_black};
    opacity: 0.4;
    top: 0;
    right: 0;
    transition: all 0.4s ease;
  }

  .box-image {
    width: 100%;
    height: 100%;
    display: block;
    object-fit: cover;
  }

  .box-title {
    position: absolute;
    max-width: 80%;
    bottom: 5px;
    right: 0;

    p {
      background: ${props => props.theme.color_white};
      color: ${props => props.theme.color_black};
      opacity: 0.8;
      padding: 1rem;
      font-weight: bold;
    }
  }

  .box-0 {
    grid-area: box-0;
  }
  .box-1 {
    grid-area: box-1;
  }
  .box-2 {
    grid-area: box-2;
  }
  .box-3 {
    grid-area: box-3;
  }
  .box-4 {
    grid-area: box-4;
  }
`

export default BlogHeader
