// ! revised
import { useState } from 'react'
import styled from 'styled-components'
import axios from 'axios'
import setToast from '../utils/toast'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { closeModal } from '../redux/actions/modalActions'

import Modal from './styles/Modal'
import Form from './styles/Form'
import InputGroup from '../components/styles/InputGroup'
import RadioGroup from '../components/RadioGroup'
import Grid from '../components/styles/Grid'
import Button from '../components/styles/Button'
import CloseButton from './styles/CloseButton'

const ContactModal_ = styled.div`
  h3 {
    font-size: 2.5rem;
    margin-top: 7.5rem;
    margin-bottom: 2rem;
  }

  p {
    margin-bottom: 2rem;
  }

  .items-group {
    position: relative;
    margin: 3rem 0;

    .frame {
      padding: 3rem 2rem;
      border: ${props => props.theme.border};
    }

    .frame-label {
      position: absolute;
      right: 1.25rem;
      top: 0;
      transform: translateY(-50%);
      padding: 0 0.75rem;
      background-color: #fff;
      color: ${props => props.theme.color_grey};
    }
  }
`

const ContactModal = ({ modal, closeModal }) => {
  const [contact, setContact] = useState({
    name: '',
    phone_number: '',
    service: '',
  })
  const [loading, setLoading] = useState(false)

  const handleRadioButton = e => {
    setContact({ ...contact, service: e.target.value })
  }

  const handleChange = e => {
    setContact({ ...contact, [e.target.name]: e.target.value })
  }

  const handleSubmit = async e => {
    try {
      e.preventDefault()
      setLoading(true)

      const mail = {
        name: contact.name,
        service: contact.service,
        phone: contact.phone_number,
      }
      await axios.post(`${process.env.BACKEND_URL}/mails`, mail)
      setToast(
        'پیام شما با موفقیت ارسال شد. کارشناسان ما در اسرع وقت با شما تماس خواهند گرفت.',
        'success'
      )
      setContact({ name: '', phone_number: '', service: '' })
      await closeModal()
      setLoading(false)
    } catch (error) {}
  }

  return (
    <ContactModal_>
      <Modal show={modal}>
        <div className='modal__content'>
          <CloseButton></CloseButton>
          <h3>دریافت مشاوره تخصصی رایگان</h3>
          <p>
            لطفا اطلاعات زیر را وارد نمایید تا کارشناسان ما در اسرع وقت با شما
            تماس بگیرند.
          </p>
          <Form onSubmit={handleSubmit}>
            <InputGroup>
              <input
                type='text'
                name='name'
                id='name'
                onChange={handleChange}
                value={contact.name}
                required
              />
              <label htmlFor='name'>نام / سازمان / شرکت</label>
            </InputGroup>
            <InputGroup direction='ltr'>
              <input
                type='text'
                name='phone_number'
                id='phone_number'
                onChange={handleChange}
                value={contact.phone_number}
                minLength='7'
                required
              />
              <label htmlFor='phone_number'>تلفن</label>
            </InputGroup>
            <div className='items-group'>
              <label className='frame-label' htmlFor='subjects'>
                موضوع مورد درخواست
              </label>
              <div className='frame' id='subjects'>
                <Grid templateColumns='1fr'>
                  <RadioGroup
                    label='اینترنت و پهنای باند'
                    id='internet'
                    value='internet'
                    checked={contact.service === 'internet'}
                    onChange={handleRadioButton}
                  />
                  <RadioGroup
                    label='تلفن ثابت و VoIP'
                    id='phone'
                    value='phone'
                    checked={contact.service === 'phone'}
                    onChange={handleRadioButton}
                  />
                  <RadioGroup
                    label='امنیت شبکه و نظارت'
                    id='security'
                    value='security'
                    checked={contact.service === 'security'}
                    onChange={handleRadioButton}
                  />
                  <RadioGroup
                    label='طراحی وب سایت'
                    id='web'
                    value='web'
                    checked={contact.service === 'web'}
                    onChange={handleRadioButton}
                  />
                </Grid>
              </div>
            </div>
            <Button primary round moving disabled={loading}>
              ثبت درخواست
            </Button>
          </Form>
        </div>
      </Modal>
    </ContactModal_>
  )
}

const mapStateToProps = state => {
  return {
    modal: state.modal.filter(_ => _.page === 'contactUs')[0].modal,
  }
}

ContactModal.propTypes = {
  modal: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
}

ContactModal.propTypes
export default connect(mapStateToProps, { closeModal })(ContactModal)
