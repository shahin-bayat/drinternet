// ! revised
import { useState, useEffect } from 'react'
import styled from 'styled-components'
import axios from 'axios'
import setToast from '../utils/toast'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import Modal from './styles/Modal'
import Form from './styles/Form'
import InputGroup from '../components/styles/InputGroup'
import Button from '../components/styles/Button'
import CloseButton from './styles/CloseButton'
import Grid from './styles/Grid'

const AdslModal_ = styled.div`
  h3 {
    font-size: 2.5rem;
    margin-top: 7.5rem;
    margin-bottom: 2rem;
  }

  p {
    margin-bottom: 2rem;
  }
`

const AdslModal = ({ modal, service, setService, tariffs, closeModal }) => {
  const [contact, setContact] = useState({
    name: '',
    phone: '',
    id_num: '',
    province: '',
    city: '',
    address: '',
    service_phone: '',
    mobile: '',
  })

  const {
    name,
    phone,
    id_num,
    province,
    city,
    address,
    service_phone,
    mobile,
  } = contact
  const [period, setPeriod] = useState('1M')
  const [price, setPrice] = useState('')

  const [loading, setLoading] = useState(false)
  useEffect(() => {
    service && calculatePrice(service, period)
  }, [service, period])

  const calculatePrice = (service, period) => {
    const servicePrice = tariffs
      .filter(item => item.serviceName === service)[0]
      .prices.filter(item => item.period === period)[0].price
    setPrice(numberWithCommas(servicePrice / 10))
  }

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
  }
  const handleChange = e => {
    if (e.target.name === 'service') {
      setService(e.target.value)
    } else if (e.target.name === 'period') {
      setPeriod(e.target.value)
    } else {
      setContact({ ...contact, [e.target.name]: e.target.value })
    }
  }

  const handleSubmit = async e => {
    try {
      e.preventDefault()
      setLoading(true)

      let additional = {
        id_num,
        province,
        city,
        address,
        service_phone,
        mobile,
        period,
        price,
      }
      additional = JSON.stringify(additional)

      const mail = {
        name,
        service,
        phone,
        additional,
      }
      await axios.post(`${process.env.BACKEND_URL}/mails`, mail)
      setToast(
        'پیام شما با موفقیت ارسال شد. کارشناسان ما در اسرع وقت با شما تماس خواهند گرفت.',
        'success'
      )
      await closeModal()
      // TODO: Remove Fields
      setLoading(false)
    } catch (error) {}
  }

  return (
    <AdslModal_>
      <Modal show={modal}>
        <div className='modal__content'>
          <CloseButton />
          <h3>درخواست سرویس +ADSL2</h3>

          <Form onSubmit={handleSubmit}>
            {/* نام . نام خانوادگی */}
            <InputGroup>
              <input
                type='text'
                name='name'
                id='name'
                onChange={handleChange}
                value={contact.name}
                required
              />
              <label htmlFor='name'>نام و نام خانوادگی</label>
            </InputGroup>

            {/* کد ملی */}
            <InputGroup direction='ltr'>
              <input
                type='text'
                name='id_num'
                id='id_num'
                onChange={handleChange}
                value={contact.id_num}
              />
              <label htmlFor='id_num'>
                کد ملی <span>(اختیاری)</span>
              </label>
            </InputGroup>

            {/* شماره تلفن رانژه */}
            <InputGroup direction='ltr'>
              <input
                type='text'
                name='service_phone'
                id='service_phone'
                onChange={handleChange}
                value={contact.service_phone}
                minLength='10'
                required
              />
              <label htmlFor='service_phone'>
                شماره تلفن مختص سرویس<span>(به همراه پیش شماره)</span>
              </label>
            </InputGroup>

            {/* سرویس، دوره و قیمت */}
            <Grid templateColumns='repeat(3, 1fr)' gridGap='0.5rem'>
              <InputGroup>
                <select
                  type='text'
                  name='service'
                  id='service'
                  value={service}
                  onChange={handleChange}
                  required
                >
                  <option value='Sef-96-512'>512 کیلوبیت بر ثانیه</option>
                  <option value='Sef-96-1024'>1 مگابیت بر ثانیه</option>
                  <option value='Sef-96-2048'>2 مگابیت بر ثانیه</option>
                  <option value='Sef-96-3072'>3 مگابیت بر ثانیه</option>
                  <option value='Sef-96-4096'>4 مگابیت بر ثانیه</option>
                  <option value='Sef-96-8192'>8 مگابیت بر ثانیه</option>
                </select>
                <label htmlFor='service'>انتخاب سرویس</label>
              </InputGroup>
              <InputGroup>
                <select
                  type='text'
                  name='period'
                  id='period'
                  value={period}
                  onChange={handleChange}
                  required
                >
                  <option value='1M'>1 ماهه</option>
                  <option value='3M'>3 ماهه</option>
                  <option value='6M'>6 ماهه</option>
                  <option value='12M'>1 ساله</option>
                </select>
                <label htmlFor='period'>دوره زمانی پرداخت</label>
              </InputGroup>

              <InputGroup direction='rtl'>
                <input
                  type='text'
                  name='price'
                  id='price'
                  style={{
                    fontWeight: 'bold',
                    color: '#00B700',
                  }}
                  value={`${price} تومان` || ''}
                  disabled
                />
                <label htmlFor='price'>قیمت دوره</label>
              </InputGroup>
            </Grid>
            <Grid templateColumns='1fr 1fr' gridGap='0.5rem'>
              <InputGroup>
                <input
                  type='text'
                  name='province'
                  id='province'
                  onChange={handleChange}
                  value={contact.province}
                  required
                />
                <label htmlFor='province'>استان</label>
              </InputGroup>
              <InputGroup>
                <input
                  type='text'
                  name='city'
                  id='city'
                  onChange={handleChange}
                  value={contact.city}
                  required
                />
                <label htmlFor='city'>شهر / شهرستان</label>
              </InputGroup>
            </Grid>
            <InputGroup>
              <input
                type='text'
                name='address'
                id='address'
                onChange={handleChange}
                value={contact.address}
                required
              />
              <label htmlFor='address'>آدرس پستی</label>
            </InputGroup>
            <Grid templateColumns='1fr 1fr' gridGap='0.5rem'>
              <InputGroup direction='ltr'>
                <input
                  type='text'
                  name='phone'
                  id='phone'
                  onChange={handleChange}
                  minLength={10}
                  value={contact.phone}
                  required
                />
                <label htmlFor='phone'>
                  تلفن<span>(به همراه پیش شماره)</span>
                </label>
              </InputGroup>
              <InputGroup direction='ltr'>
                <input
                  type='text'
                  name='mobile'
                  id='mobile'
                  onChange={handleChange}
                  value={contact.mobile}
                  minLength={10}
                  required
                />
                <label htmlFor='mobile'>موبایل</label>
              </InputGroup>
            </Grid>

            <Button primary round moving disabled={loading}>
              ثبت درخواست
            </Button>
          </Form>
        </div>
      </Modal>
    </AdslModal_>
  )
}

const mapStateToProps = state => {
  return {
    modal: state.modal.filter(_ => _.page === 'adsl')[0].modal,
  }
}

AdslModal.propTypes = {
  modal: PropTypes.bool.isRequired,
  // closeModal: PropTypes.func.isRequired,
}

export default connect(mapStateToProps)(AdslModal)
