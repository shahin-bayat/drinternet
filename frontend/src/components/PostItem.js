// ! revised
import styled from 'styled-components'
import Link from 'next/link'
import changeTimeZone from '../utils/time'

const Post_ = styled.div`
  display: grid;
  grid-template-columns: 20% 1fr;
  grid-gap: 1.5rem;
  background: ${props => props.theme.color_white};
  margin-bottom: 1rem;
  padding: 2rem;
  box-shadow: ${props => props.theme.box_shadow_light};

  @media (max-width: ${props => props.theme.bp_smallest}) {
    grid-template-columns: 40% 1fr;
  }

  img.post__image--medium {
    border: ${props => props.theme.border};
    width: 100%;
  }

  article {
    display: flex;
    flex-direction: column;
    h3.title {
      cursor: pointer;
      transition: all 0.2s ease;
      text-align: justify;
      text-justify: auto;

      @media (max-width: ${props => props.theme.bp_smallest}) {
        font-size: 1.4rem;
      }

      :hover {
        color: ${props => props.theme.color_primary};
      }
    }

    p.excerpt {
      @media (max-width: ${props => props.theme.bp_smallest}) {
        display: none;
      }
    }

    p.date {
      margin-top: 0.5rem;
      font-size: 1.2rem;
      margin-bottom: 2rem;
      color: ${props => props.theme.color_grey_light_3};
    }

    a.link {
      text-decoration: none;
      color: ${props => props.theme.color_primary};
      font-weight: bold;
      font-size: 1.3rem;
    }
  }
`
const Post = ({ post }) => {
  const { jDate, tehranTime } = changeTimeZone(post.createdAt)
  return (
    <Post_>
      <img
        className='post__image--medium'
        src={post.postImage}
        alt={post.title}
      />
      <article>
        <p className='subcategory'>
          {post.subcategory === 'نامشخص' ? post.category : post.subcategory}
        </p>
        <Link href={`/blog/[slug]`} as={`/blog/${post.slug}`}>
          <h3 className='title'>{post.title}</h3>
        </Link>
        <p className='date'>
          {jDate} | {tehranTime}
        </p>
        <p className='excerpt'>
          {post.excerpt}...
          <Link href={`/blog/[slug]`} as={`/blog/${post.slug}`}>
            <a className='link'>ادامه مطلب</a>
          </Link>
        </p>
      </article>
    </Post_>
  )
}

export default Post
