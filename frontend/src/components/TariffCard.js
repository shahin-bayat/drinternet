import styled from 'styled-components'
import Card from './styles/Card'
import Button from './styles/Button'

import ChevronSVG from '../images/SVG/chevron.svg'

const Speed = styled.h3`
  width: 100%;
  text-align: center;
  padding: 1.5rem;
`

const Traffic = styled.div`
  width: 100%;
  margin-bottom: 2rem;
  h3 {
    padding: 1rem;
    background-color: ${props => props.theme.color_primary};
    color: ${props => props.theme.color_white};
    text-align: center;
  }

  span {
    color: ${props => props.theme.color_grey_light_2};
  }
`

const Features = styled.ul`
  list-style: none;
  width: 100%;
  padding: 0 2rem;
  margin-bottom: 6rem;

  li {
    padding: 0.5rem 0;
    display: flex;
    align-items: center;
    font-size: 1.3rem;

    p {
      margin: 0;
    }

    svg {
      width: 1rem;
      height: 1rem;
      margin: 0;
      margin-left: 1rem;
    }

    span {
      font-weight: bold;
      font-size: 1.3rem;
      color: ${props => props.theme.color_primary};
    }
  }
`

const Price = styled.h3`
  width: 100%;
  text-align: center;
  padding: 1rem;
  background-color: ${props => props.theme.color_grey_light};
  color: ${props => props.theme.color_white};
`

const TariffCard = ({ tariff, setService, openModal, page }) => {
  let speed
  if (Array.isArray(tariff.speed)) {
    speed = `${tariff.speed[0]} تا ${
      typeof tariff.speed[1] === 'string' ? 'به بالا' : tariff.speed[1]
    } Mbps`

    if (typeof tariff.speed[1] === 'string') {
      speed = `${tariff.speed[0]} Mbps به بالا`
    }
  } else {
    speed =
      tariff.speed / 1024 < 1
        ? `${512} کیلوبیت بر ثانیه`
        : `${tariff.speed / 1024} مگابیت بر ثانیه`
  }

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
  }

  let price
  if (tariff.prices) {
    price = (
      <Price>قیمت {numberWithCommas(tariff.prices[0].price / 10)} تومان</Price>
    )
  } else if (tariff.price && Number.isInteger(tariff.price.price)) {
    price = (
      <Price>
        قیمت {numberWithCommas(tariff.price.price / 10)} تومان{' '}
        <span>به ازای مگابیت در ماه</span>
      </Price>
    )
  } else {
    price = <Price>مذاکره تلفنی</Price>
  }

  const selectService = async e => {
    e.preventDefault()
    setService(tariff.serviceName)
    await openModal(page)
  }

  return (
    <Card padding='0' noHover>
      <Speed>{speed}</Speed>
      {tariff.traffic ? (
        <Traffic>
          <h3>
            {tariff.traffic} گیگابایت <span>ترافیک بین‌الملل</span>
          </h3>
        </Traffic>
      ) : (
        <Traffic>
          <h3>{tariff.burst}% burst</h3>
        </Traffic>
      )}

      {tariff.traffic ? (
        <Features>
          <li>
            <ChevronSVG />
            <p>
              <span>{tariff.traffic * 3} گیگابایت </span> ترافیک داخلی
            </p>
          </li>
          <li>
            <ChevronSVG />
            دوره سرویس: یک ماهه
          </li>
        </Features>
      ) : (
        <Features>
          <li>
            <ChevronSVG />
            <p>
              <span>{tariff.burst}% </span>میزان burst سرویس
            </p>
          </li>
          <li>
            <ChevronSVG />
            <p>ارسال و دریافت متقارن</p>
          </li>
          <li>
            <ChevronSVG />
            <p>
              نوع تجهیزات:{' '}
              <span>{tariff.type == 'rent' ? 'اجاره‌ای' : 'امانی'}</span>
            </p>
          </li>
        </Features>
      )}
      {price}

      <Button confirm square onClick={selectService}>
        درخواست سرویس
      </Button>
    </Card>
  )
}

export default TariffCard
