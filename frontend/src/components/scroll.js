import styled from "styled-components"

const Scroll = styled.div`
  position: absolute;
  bottom: 5rem;
  right: 50%;
  transform: translateX(-50%);

  @keyframes scroll_1 {
    0% {
      transform: translateY(0);
    }
    25% {
      transform: translateY(-0.6em);
    }
    50% {
      transform: translateY(0);
    }
    75% {
      transform: translateY(0.6em);
    }
    100% {
      transform: translateY(0);
    }
  }
  .scroll-icon {
    display: block;
    position: relative;
    height: 4em;
    width: 2em;
    border: 0.15em solid ${props => props.theme.color_primary};
    border-radius: 1em;
  }

  .scroll-icon__wheel-outer {
    display: block;
    position: absolute;
    left: 50%;
    top: 0.6em;
    height: 1em;
    width: 0.4em;
    margin-left: -0.2em;
    border-radius: 0.4em;
    overflow: hidden;
  }

  .scroll-icon__wheel-inner {
    display: block;
    height: 100%;
    width: 100%;
    border-radius: inherit;
    background: ${props => props.theme.color_primary};
    animation: scroll_1 2.75s ease-in-out infinite;
  }
`

const scroll = () => {
  return (
    <Scroll>
      <span class='scroll-icon'>
        <span class='scroll-icon__wheel-outer'>
          <span class='scroll-icon__wheel-inner'></span>
        </span>
      </span>
    </Scroll>
  )
}

export default scroll
