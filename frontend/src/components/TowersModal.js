// ! revised
import { useState, useEffect } from 'react'
import styled from 'styled-components'
import axios from 'axios'
import setToast from '../utils/toast'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import Modal from './styles/Modal'
import Form from './styles/Form'
import InputGroup from '../components/styles/InputGroup'
import Button from '../components/styles/Button'
import CloseButton from './styles/CloseButton'
import Grid from './styles/Grid'
import RadioGroup from './styles/RadioGroup'
const TowersModal_ = styled.div`
  h3 {
    font-size: 2.5rem;
    margin-top: 7.5rem;
    margin-bottom: 2rem;
  }

  p {
    margin-bottom: 2rem;
  }

  .items-group {
    position: relative;
    margin: 3rem 0;

    .frame {
      padding: 3rem 2rem;
      border: ${props => props.theme.border};
    }

    .frame-label {
      position: absolute;
      right: 1.25rem;
      top: 0;
      transform: translateY(-50%);
      padding: 0 0.75rem;
      background-color: #fff;
      color: ${props => props.theme.color_grey};
    }
  }
`

const TowersModal = ({ modal, closeModal }) => {
  const [contact, setContact] = useState({
    name: '',
    tower_name: '',
    phone: '',
    province: '',
    city: '',
    address: '',
    mobile: '',
    floors: '',
    units: '',
    manager_name: '',
    phone_lines_available: '',
    using_another_service: '',
    mdf_room_available: '',
  })

  const {
    name,
    phone,
    province,
    city,
    address,
    mobile,
    floors,
    units,
    manager_name,
    phone_lines_available,
    using_another_service,
    mdf_room_available,
  } = contact

  const [loading, setLoading] = useState(false)
  const handleChange = e => {
    setContact({ ...contact, [e.target.name]: e.target.value })
  }

  const handleSubmit = async e => {
    try {
      e.preventDefault()
      setLoading(true)

      let additional = {
        province,
        city,
        address,
        mobile,
        floors,
        units,
        manager_name,
        phone_lines_available,
        using_another_service,
        mdf_room_available,
      }
      additional = JSON.stringify(additional)

      const mail = {
        name,
        service: 'towers',
        phone,
        additional,
      }

      await axios.post(`${process.env.BACKEND_URL}/mails`, mail)
      setToast(
        'پیام شما با موفقیت ارسال شد. کارشناسان ما در اسرع وقت با شما تماس خواهند گرفت.',
        'success'
      )
      await closeModal()
      // TODO: Remove Fields
      setLoading(false)
    } catch (error) {}
  }

  return (
    <TowersModal_>
      <Modal show={modal} minWidth='70%'>
        <div className='modal__content'>
          <CloseButton />
          <h3>درخواست اینترنت ویژه‌ی برج‌ها و مجتمع‌ها</h3>

          <Form onSubmit={handleSubmit}>
            {/* complex name, province, city */}
            <Grid templateColumns='repeat(3, 1fr)' gridGap='0.5rem'>
              {/* نام . نام خانوادگی */}
              <InputGroup>
                <input
                  type='text'
                  name='tower_name'
                  id='tower_name'
                  onChange={handleChange}
                  value={contact.tower_name}
                  required
                />
                <label htmlFor='tower_name'>نام مجتمع / شهرک</label>
              </InputGroup>
              <InputGroup>
                <input
                  type='text'
                  name='province'
                  id='province'
                  onChange={handleChange}
                  value={contact.province}
                  required
                />
                <label htmlFor='province'>استان</label>
              </InputGroup>
              <InputGroup>
                <input
                  type='text'
                  name='city'
                  id='city'
                  onChange={handleChange}
                  value={contact.city}
                  required
                />
                <label htmlFor='city'>شهر / شهرستان</label>
              </InputGroup>
            </Grid>

            {/* Address */}
            <InputGroup>
              <input
                type='text'
                name='address'
                id='address'
                onChange={handleChange}
                value={contact.address}
                required
              />
              <label htmlFor='address'>آدرس پستی</label>
            </InputGroup>

            {/* name, phone - mobile */}
            <Grid templateColumns='repeat(3,1fr)' gridGap='0.5rem'>
              <InputGroup>
                <input
                  type='text'
                  name='name'
                  id='name'
                  onChange={handleChange}
                  value={contact.name}
                  required
                />
                <label htmlFor='name'>نام و نام خانوادگی تکمیل کننده فرم</label>
              </InputGroup>
              <InputGroup direction='ltr'>
                <input
                  type='text'
                  name='phone'
                  id='phone'
                  onChange={handleChange}
                  value={contact.phone}
                  minLength={10}
                  required
                />
                <label htmlFor='phone'>
                  تلفن<span>(به همراه پیش شماره)</span>
                </label>
              </InputGroup>
              <InputGroup direction='ltr'>
                <input
                  type='text'
                  name='mobile'
                  id='mobile'
                  onChange={handleChange}
                  value={contact.mobile}
                  minLength={10}
                  required
                />
                <label htmlFor='mobile'>موبایل</label>
              </InputGroup>
            </Grid>
            <Grid templateColumns='repeat(3,1fr)' gridGap='0.5rem'>
              <InputGroup direction='ltr'>
                <input
                  type='text'
                  name='units'
                  id='units'
                  onChange={handleChange}
                  value={contact.units}
                  required
                />
                <label htmlFor='units'>تعداد واحد‌ها</label>
              </InputGroup>
              <InputGroup direction='ltr'>
                <input
                  type='text'
                  name='floors'
                  id='floors'
                  onChange={handleChange}
                  value={contact.floors}
                />
                <label htmlFor='floors'>
                  تعداد طبقات / بلوک<span>(اختیاری)</span>
                </label>
              </InputGroup>
              <InputGroup direction='rtl'>
                <input
                  type='text'
                  name='manager_name'
                  id='manager_name'
                  onChange={handleChange}
                  value={contact.manager_name}
                />
                <label htmlFor='manager_name'>
                  نام مدیر مجتمع / شهرک<span>(اختیاری)</span>
                </label>
              </InputGroup>
            </Grid>

            {/* Radio Buttons */}
            <div className='items-group'>
              <label className='frame-label' htmlFor='subjects'>
                اطلاعات درخواستی:
              </label>
              <div className='frame' id='subjects'>
                {/*First Radio Button Group */}
                <Grid templateColumns='50% repeat(2,1fr)'>
                  <label>
                    آیا در حال حاضر خطوط تلفن مجتمع / شهرک دایر است؟{' '}
                  </label>
                  <RadioGroup>
                    <input
                      type='radio'
                      id='phone_lines_available_1'
                      value='بله'
                      name='phone_lines_available'
                      checked={contact.phone_lines_available === 'بله'}
                      onChange={handleChange}
                    />
                    <label htmlFor='phone_lines_available_1'>
                      <span />
                      بله
                    </label>
                  </RadioGroup>
                  <RadioGroup>
                    <input
                      type='radio'
                      id='phone_lines_available_2'
                      value='خیر'
                      name='phone_lines_available'
                      checked={contact.phone_lines_available === 'خیر'}
                      onChange={handleChange}
                    />
                    <label htmlFor='phone_lines_available_2'>
                      <span />
                      خیر
                    </label>
                  </RadioGroup>
                </Grid>

                {/*Second Radio Button Group */}
                <Grid
                  templateColumns='50% repeat(2,1fr)'
                  style={{ marginTop: '2rem' }}
                >
                  <label>
                    آیا در حال حاضر از خدمات شرکت دیگری استفاده می کنید؟{' '}
                  </label>
                  <RadioGroup>
                    <input
                      type='radio'
                      id='using_another_service_1'
                      value='بله'
                      name='using_another_service'
                      checked={contact.using_another_service === 'بله'}
                      onChange={handleChange}
                    />
                    <label htmlFor='using_another_service_1'>
                      <span />
                      بله
                    </label>
                  </RadioGroup>
                  <RadioGroup>
                    <input
                      type='radio'
                      id='using_another_service_2'
                      value='خیر'
                      name='using_another_service'
                      checked={contact.using_another_service === 'خیر'}
                      onChange={handleChange}
                    />
                    <label htmlFor='using_another_service_2'>
                      <span />
                      خیر
                    </label>
                  </RadioGroup>
                </Grid>

                {/*Third Radio Button Group */}
                <Grid
                  templateColumns='50% repeat(2,1fr)'
                  style={{ marginTop: '2rem' }}
                >
                  <label>
                    آیا مجتمع دارای اتاق تاسیسات و تلفن مستقل می باشد؟
                  </label>
                  <RadioGroup>
                    <input
                      type='radio'
                      id='mdf_room_available_1'
                      value='بله'
                      name='mdf_room_available'
                      checked={contact.mdf_room_available === 'بله'}
                      onChange={handleChange}
                    />
                    <label htmlFor='mdf_room_available_1'>
                      <span />
                      بله
                    </label>
                  </RadioGroup>
                  <RadioGroup>
                    <input
                      type='radio'
                      id='mdf_room_available_2'
                      value='خیر'
                      name='mdf_room_available'
                      checked={contact.mdf_room_available === 'خیر'}
                      onChange={handleChange}
                    />
                    <label htmlFor='mdf_room_available_2'>
                      <span />
                      خیر
                    </label>
                  </RadioGroup>
                </Grid>
              </div>
            </div>

            <Button primary round moving disabled={loading}>
              ثبت درخواست
            </Button>
          </Form>
        </div>
      </Modal>
    </TowersModal_>
  )
}

const mapStateToProps = state => {
  return {
    modal: state.modal.filter(_ => _.page === 'towers')[0].modal,
  }
}

TowersModal.propTypes = {
  modal: PropTypes.bool.isRequired,
  // closeModal: PropTypes.func.isRequired,
}

export default connect(mapStateToProps)(TowersModal)
