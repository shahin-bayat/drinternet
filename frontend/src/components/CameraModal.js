// ! revised
import { useState, useEffect } from 'react'
import styled from 'styled-components'
import axios from 'axios'
import setToast from '../utils/toast'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import Modal from './styles/Modal'
import Form from './styles/Form'
import InputGroup from '../components/styles/InputGroup'
import Button from '../components/styles/Button'
import CloseButton from './styles/CloseButton'
import Grid from './styles/Grid'

const CameraModal_ = styled.div`
  h3 {
    font-size: 2.5rem;
    margin-top: 7.5rem;
    margin-bottom: 2rem;
  }

  p {
    margin-bottom: 2rem;
  }
`

const CameraModal = ({ modal, closeModal }) => {
  const [contact, setContact] = useState({
    name: '',
    phone: '',
    email: '',
    description: '',
  })

  const { name, phone, email, description } = contact

  const [loading, setLoading] = useState(false)
  const handleChange = e => {
    setContact({ ...contact, [e.target.name]: e.target.value })
  }

  const handleSubmit = async e => {
    try {
      e.preventDefault()
      setLoading(true)

      let additional = {
        email,
        description,
      }
      additional = JSON.stringify(additional)

      const mail = {
        name,
        service: 'camera',
        phone,
        additional,
      }
      await axios.post(`${process.env.BACKEND_URL}/mails`, mail)
      setToast(
        'پیام شما با موفقیت ارسال شد. کارشناسان ما در اسرع وقت با شما تماس خواهند گرفت.',
        'success'
      )
      await closeModal()
      // TODO: Remove Fields
      setLoading(false)
    } catch (error) {}
  }

  return (
    <CameraModal_>
      <Modal show={modal} minWidth='40%' bpSmallHeight='auto'>
        <div className='modal__content'>
          <CloseButton />
          <h3>درخواست سرویس دوربین‌های نظارتی</h3>

          <Form onSubmit={handleSubmit}>
            {/* نام . نام خانوادگی */}
            <InputGroup>
              <input
                type='text'
                name='name'
                id='camera-name'
                onChange={handleChange}
                value={contact.name}
                required
              />
              <label htmlFor='camera-name'>نام و نام خانوادگی / نام شرکت</label>
            </InputGroup>

            <InputGroup direction='ltr'>
              <input
                type='text'
                name='phone'
                id='camera-phone'
                onChange={handleChange}
                value={contact.phone}
                minLength={10}
                required
              />
              <label htmlFor='camera-phone'>
                تلفن<span>(به همراه پیش شماره)</span>
              </label>
            </InputGroup>

            {/* email*/}
            <InputGroup direction='ltr'>
              <input
                type='email'
                name='email'
                id='camera-email'
                onChange={handleChange}
                value={contact.email}
              />
              <label htmlFor='camera-email'>
                ایمیل<span>(اختیاری)</span>
              </label>
            </InputGroup>

            <InputGroup minHeight='20rem'>
              <textarea
                label='description'
                name='description'
                id='camera-description'
                onChange={handleChange}
                value={contact.description}
                required
              ></textarea>
              <label htmlFor='camera-description'>
                شرح مختصری از نیاز و پروژه
              </label>
            </InputGroup>

            <Button primary round moving disabled={loading}>
              ثبت درخواست
            </Button>
          </Form>
        </div>
      </Modal>
    </CameraModal_>
  )
}

const mapStateToProps = state => {
  return {
    modal: state.modal.filter(_ => _.page === 'camera')[0].modal,
  }
}

CameraModal.propTypes = {
  modal: PropTypes.bool.isRequired,
  // closeModal: PropTypes.func.isRequired,
}

export default connect(mapStateToProps)(CameraModal)
