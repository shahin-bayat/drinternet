// ! revised
// not used
import React from "react"
import styled from "styled-components"
import Slider from "react-slick"
import Card from "./styles/Card"

const Wrapper = styled.div`
  width: 100%;

  h2 {
    margin: 2rem 0;
  }
`

const Slide = styled(Card)`
  margin: 0 1rem;
  border: none;
  box-shadow: none;
  min-height: 30rem;

  :hover {
    border: ${props => props.theme.border};
    box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
  }
`

const ProductsSlide = ({ products }) => {
  const settings = {
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  }

  return (
    <Wrapper>
      <h2>آنتی ویروس</h2>
      <Slider {...settings}>
        {products.map(product => {
          return (
            <div key={product._id}>
              <Slide>
                <img src='' alt='' />
                <h3>آنتی ویروس ESET</h3>
                <p>135000</p>
              </Slide>
            </div>
          )
        })}
      </Slider>
    </Wrapper>
  )
}

export default ProductsSlide
