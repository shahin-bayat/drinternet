// ! revised
import { connect } from "react-redux"
import PropTypes from "prop-types"
import { readMessage } from "../redux/actions/messageActions"
import Router from "next/router"
import changeTimeZone from "../utils/time"

import Table from "../components/styles/Table"

const MessageTable = ({ results, readMessage, path }) => {
  const handleClick = messageId => {
    readMessage(messageId)
    Router.push(
      `/${path}/messages/[messageId]`,
      `/${path}/messages/${messageId}`
    )
  }

  const messages =
    results.length !== 0
      ? results.map(res => {
          const { jDate, tehranTime } = changeTimeZone(res.sentAt)

          return (
            <li
              className={`table__items ${res.isRead === false && "not-read"}`}
              key={res._id}
              onClick={e => handleClick(res._id)}
            >
              <span>{res.subject}</span>
              <span>{res.body.substr(0, 50) + "..."}</span>
              <span>{jDate + " ساعت " + tehranTime}</span>
            </li>
          )
        })
      : ""

  return (
    <Table>
      <li className='table__first-row'>
        <span className='table__first-row__subject'>موضوع</span>
        <span className='table__first-row__body'>متن پیام</span>
        <span className='table__first-row__subject'>تاریخ</span>
      </li>
      {messages}
    </Table>
  )
}

MessageTable.propTypes = {
  readMessage: PropTypes.func.isRequired
}

export default connect(state => state, { readMessage })(MessageTable)
