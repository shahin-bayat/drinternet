// ! revised
import { useState, useEffect } from 'react'
import styled from 'styled-components'
import axios from 'axios'
import setToast from '../utils/toast'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import Modal from './styles/Modal'
import Form from './styles/Form'
import InputGroup from '../components/styles/InputGroup'
import Button from '../components/styles/Button'
import CloseButton from './styles/CloseButton'
import Grid from './styles/Grid'

const TransitModal_ = styled.div`
  h3 {
    font-size: 2.5rem;
    margin-top: 7.5rem;
    margin-bottom: 2rem;
  }

  p {
    margin-bottom: 2rem;
  }
`

const TransitModal = ({ modal, closeModal }) => {
  const [contact, setContact] = useState({
    name: '',
    phone: '',
    interface_person: '',
    province: '',
    city: '',
    address: '',
    mobile: '',
    email: '',
    description: '',
  })

  const {
    name,
    phone,
    interface_person,
    province,
    city,
    address,
    mobile,
    email,
    description,
  } = contact

  const [loading, setLoading] = useState(false)
  const handleChange = e => {
    setContact({ ...contact, [e.target.name]: e.target.value })
  }

  const handleSubmit = async e => {
    try {
      e.preventDefault()
      setLoading(true)

      let additional = {
        interface_person,
        province,
        city,
        address,
        mobile,
        email,
        description,
      }
      additional = JSON.stringify(additional)

      const mail = {
        name,
        service: 'transit',
        phone,
        additional,
      }
      await axios.post(`${process.env.BACKEND_URL}/mails`, mail)
      setToast(
        'پیام شما با موفقیت ارسال شد. کارشناسان ما در اسرع وقت با شما تماس خواهند گرفت.',
        'success'
      )
      await closeModal()
      // TODO: Remove Fields
      setLoading(false)
    } catch (error) { }
  }

  return (
    <TransitModal_>
      <Modal show={modal} minWidth='60%'>
        <div className='modal__content'>
          <CloseButton />
          <h3>درخواست سرویس ارتباط بین شعب</h3>

          <Form onSubmit={handleSubmit}>
            {/* نام . نام خانوادگی */}
            <InputGroup>
              <input
                type='text'
                name='name'
                id='transit-name'
                onChange={handleChange}
                value={contact.name}
                required
              />
              <label htmlFor='transit-name'>
                نام و نام خانوادگی / نام شرکت
              </label>
            </InputGroup>

            {/* شهر شهرستان */}
            <Grid templateColumns='1fr 1fr' gridGap='0.5rem'>
              <InputGroup>
                <input
                  type='text'
                  name='province'
                  id='transit-province'
                  onChange={handleChange}
                  value={contact.province}
                  required
                />
                <label htmlFor='transit-province'>استان</label>
              </InputGroup>
              <InputGroup>
                <input
                  type='text'
                  name='city'
                  id='transit-city'
                  onChange={handleChange}
                  value={contact.city}
                  required
                />
                <label htmlFor='transit-city'>شهر / شهرستان</label>
              </InputGroup>
            </Grid>

            {/* آدرس */}
            <InputGroup>
              <input
                type='text'
                name='address'
                id='transit-address'
                onChange={handleChange}
                value={contact.address}
                required
              />
              <label htmlFor='transit-address'>آدرس پستی</label>
            </InputGroup>

            {/* phone - mobile */}
            <Grid templateColumns='1fr 1fr' gridGap='0.5rem'>
              <InputGroup direction='ltr'>
                <input
                  type='text'
                  name='phone'
                  id='transit-phone'
                  onChange={handleChange}
                  value={contact.phone}
                  minLength={10}
                  required
                />
                <label htmlFor='transit-phone'>
                  تلفن<span>(به همراه پیش شماره)</span>
                </label>
              </InputGroup>
              <InputGroup direction='ltr'>
                <input
                  type='text'
                  name='mobile'
                  id='transit-mobile'
                  onChange={handleChange}
                  value={contact.mobile}
                  minLength={10}
                  required
                />
                <label htmlFor='transit-mobile'>موبایل</label>
              </InputGroup>
            </Grid>

            {/* رابط */}
            <InputGroup>
              <input
                type='text'
                name='interface_person'
                id='transit-interface_person'
                onChange={handleChange}
                value={contact.interface_person}
                required
              />
              <label htmlFor='transit-interface_person'>نام شخص رابط</label>
            </InputGroup>

            {/* email*/}
            <InputGroup direction='ltr'>
              <input
                type='email'
                name='email'
                id='transit-email'
                onChange={handleChange}
                value={contact.email}
              />
              <label htmlFor='transit-email'>
                ایمیل<span>(اختیاری)</span>
              </label>
            </InputGroup>

            <InputGroup minHeight='2rem'>
              <textarea
                label='description'
                name='description'
                id='transit-description'
                onChange={handleChange}
                value={contact.description}
              ></textarea>
              <label htmlFor='transit-description'>
                توضیحات<span>(اختیاری)</span>
              </label>
            </InputGroup>

            <Button primary round moving disabled={loading}>
              ثبت درخواست
            </Button>
          </Form>
        </div>
      </Modal>
    </TransitModal_>
  )
}

const mapStateToProps = state => {
  return {
    modal: state.modal.filter(_ => _.page === 'transit')[0].modal,
  }
}

TransitModal.propTypes = {
  modal: PropTypes.bool.isRequired,
  // closeModal: PropTypes.func.isRequired,
}

export default connect(mapStateToProps)(TransitModal)
