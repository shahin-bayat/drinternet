// ! revised
import styled from "styled-components"
import changTimeZone from "../utils/time"

const Comment_ = styled.div`
  margin-top: 2rem;
  padding: 1rem 2rem;
  border-bottom: ${props => props.theme.border};

  .comment__details {
    margin-bottom: 1.5rem;
    display: flex;
    align-items: center;

    .comment__writer {
      color: ${props => props.theme.color_primary};
      font-weight: bold;
      font-size: 1.4rem;
      margin-left: 2rem;
    }

    .comment__time {
      color: ${props => props.theme.color_grey_light_3};
    }
  }
`

const Comment = ({ comment }) => {
  const { jDate, tehranTime } = changTimeZone(comment.sentAt)

  return (
    <Comment_>
      <div className='comment__details'>
        <span className='comment__writer'>{comment.from.name}</span>
        <span className='comment__time'>
          {jDate} | {tehranTime}{" "}
        </span>
      </div>
      <p>{comment.body}</p>
    </Comment_>
  )
}
export default Comment
