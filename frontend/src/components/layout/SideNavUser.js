// ! revised
import styled from "styled-components"
import { connect } from "react-redux"
import PropTypes from "prop-types"
import Router from "next/router"

import SideNav_ from "../styles/SideNav"
import NavTitle from "../styles/NavTitle"

const NavItems_ = styled.ul`
  display: flex;
  flex-direction: column;
  list-style: none;
  align-items: flex-start;
  > li {
    width: 100%;
    padding: 2rem 2rem;
    cursor: pointer;
    a {
      text-decoration: none;
      color: ${props => props.theme.color_grey};
    }

    :hover {
      background-color: ${props => props.theme.color_grey_light};
      a {
        color: ${props => props.theme.color_primary};
      }
    }
  }
`

const SideNav = ({ messages }) => {
  let numberOfNewMessages = 0
  if (messages.length !== 0) {
    messages.forEach(msg => {
      if (msg.isRead === false) numberOfNewMessages++
    })
  }
  const navItems = [
    { link: "پروفایل", location: "/profile", id: 1 },
    { link: "درخواست پشتیبانی", location: "/profile/ticket", id: 2 },
    { link: "پیام‌ها", location: "/profile/messages", id: 3 },
    { link: "سبد خرید", location: "/profile/cart", id: 4 }
  ]
  const navList = navItems.map(item => {
    if (item.id === 3) {
      return (
        <li onClick={e => handleClick(e, item.location)} key={item.id}>
          <a>
            {item.link}
            {numberOfNewMessages > 0 && (
              <span className='side-nav__messages'>
                {numberOfNewMessages + " " + "پیام جدید"}
              </span>
            )}
          </a>
        </li>
      )
    } else
      return (
        <li onClick={e => handleClick(e, item.location)} key={item.id}>
          <a>{item.link}</a>
        </li>
      )
  })
  const handleClick = (e, path) => {
    Router.push(path)
  }

  return (
    <SideNav_>
      <NavItems_>
        <NavTitle>
          <span>حساب کاربری شما</span>
        </NavTitle>
        {navList}
      </NavItems_>
    </SideNav_>
  )
}

SideNav.propTypes = {
  messages: PropTypes.array
}

const mapStateToProps = state => ({
  messages: state.message.messages
})

export default connect(mapStateToProps)(SideNav)
