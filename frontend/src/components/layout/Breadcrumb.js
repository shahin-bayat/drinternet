// ! revised
import Link from 'next/link'
import styled from 'styled-components'

import { getCategoryInEnglish } from '../../utils/BlogCategories'

const Breadcrumb = ({ category, subcategory, title }) => {
  const englishCategory = getCategoryInEnglish(category)
  return (
    <Breadcrumb_>
      <li className='category'>
        <Link
          href={`/blog/category/[category]`}
          as={`/blog/category/${englishCategory}`}
        >
          <a>{category}</a>
        </Link>
      </li>
      <li className='sub-category'>
        <Link href='#!'>
          <a>{subcategory}</a>
        </Link>
      </li>
      <li className='post-title'>
        <Link href='#!'>
          <a>{title}</a>
        </Link>
      </li>
    </Breadcrumb_>
  )
}

const Breadcrumb_ = styled.ul`
  list-style: none;
  display: flex;
  padding-bottom: 2rem;
  border-bottom: ${props => props.theme.border};

  li {
    display: flex;
    align-items: center;

    :not(:last-child)::after {
      display: block;
      content: '›';
      font-size: 2rem;
      line-height: 1.6;
      font-weight: bold;
      color: ${props => props.theme.color_primary};
      margin: 0 0.5rem;
    }

    a {
      font-size: 1.3rem;
      text-decoration: none;
      color: ${props => props.theme.color_grey};
      cursor: pointer;
    }
  }

  li.category {
    a {
      color: ${props => props.theme.color_primary};
      cursor: pointer;
    }
  }

  li.sub-category {
    a {
      cursor: text;
    }
  }

  li.post-title {
    a {
      cursor: text;
    }
  }
`

export default Breadcrumb
