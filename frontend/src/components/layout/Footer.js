// ! revised
import styled from 'styled-components'
import Link from 'next/link'
import Container from '../styles/Container'

const Footer_ = styled.footer`
  background: ${props => props.theme.color_grey_dark};
  padding: 3rem 0;
  font-size: 1.4rem;
`

const NavItems = styled.ul`
  display: flex;
  list-style: none;
  align-items: center;
  justify-content: space-between;

  li {
    padding: 1rem 2rem;
    transition: all 0.3s ease;
    :hover {
      background-color: ${props => props.theme.color_grey};
    }

    a {
      text-decoration: none;
      color: ${props => props.theme.color_grey_light};
      cursor: pointer;
      transition: color 0.1s ease-in;

      &:hover {
        color: ${props => props.theme.color_white};
      }
    }
  }
`

const Footer = () => {
  return (
    <Footer_>
      <Container alignItems='center'>
        {/* <NavItems>
          <li>
            <Link href=''>
              <a>درباره ما</a>
            </Link>
          </li>
          <li>
            <Link href=''>
              <a>قوانین خرید</a>
            </Link>
          </li>
          <li>
            <Link href=''>
              <a>سوالات متداول</a>
            </Link>
          </li>
          <li>
            <Link href=''>
              <a>استخدام</a>
            </Link>
          </li>
          <li>
            <Link href=''>
              <a>همکاری در فروش</a>
            </Link>
          </li>
          <li>
            <Link href=''>
              <a>تماس با ما</a>
            </Link>
          </li>
        </NavItems> */}

        <div style={{ textAlign: 'center' }}>
          <p>کلیه حقوق برای شرکت دکتر اینترنت محفوظ می باشد.</p>
          <p>Copyright © 2019 DrInternet Ltd ® , All Rights Reserved</p>
        </div>
      </Container>
    </Footer_>
  )
}

export default Footer
