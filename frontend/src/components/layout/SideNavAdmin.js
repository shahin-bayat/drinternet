// ! revised
import React from "react"
import styled from "styled-components"
import { connect } from "react-redux"
import PropTypes from "prop-types"

import SideNav_ from "../styles/SideNav"
import NavTitle from "../styles/NavTitle"
import Router from "next/router"

const NavItems_ = styled.ul`
  display: flex;
  flex-direction: column;
  list-style: none;
  align-items: flex-start;
  > li {
    width: 100%;
    padding: 2rem 2rem;
    cursor: pointer;
    a {
      text-decoration: none;
      color: ${props => props.theme.color_grey};
    }

    :hover {
      background-color: ${props => props.theme.color_grey_light};
      a {
        color: ${props => props.theme.color_primary};
      }
    }
  }
`

const SideNav = ({ messages }) => {
  const navItems = [
    { link: "داشبورد", location: "/admin", id: 1 },
    { link: "پست ها", location: "/admin/posts", id: 2 },
    { link: "محصولات", location: "/admin/products", id: 3 },
    { link: "سفارشات", location: "/admin/orders", id: 4 },
    { link: "پیام‌ها", location: "/admin/messages", id: 5 },
    {
      link: "ارسال پیام گروهی",
      location: "/admin/messages/group-message",
      id: 6
    }
  ]

  let numberOfNewMessages = 0
  if (messages.length !== 0) {
    messages.forEach(msg => {
      if (msg.isRead === false) numberOfNewMessages++
    })
  }

  const handleClick = (e, path) => {
    Router.push(path)
  }

  const navList = navItems.map(item => {
    if (item.id === 5) {
      return (
        <li onClick={e => handleClick(e, item.location)} key={item.id}>
          <a>
            {item.link}
            {numberOfNewMessages > 0 && (
              <span className='side-nav__messages'>
                {numberOfNewMessages + " " + "پیام جدید"}
              </span>
            )}
          </a>
        </li>
      )
    } else
      return (
        <li onClick={e => handleClick(e, item.location)} key={item.id}>
          <a>{item.link}</a>
        </li>
      )
  })
  return (
    <SideNav_>
      <NavItems_>
        <NavTitle>
          <span>پنل مدیریت وب سایت</span>
        </NavTitle>
        {navList}
      </NavItems_>
    </SideNav_>
  )
}

SideNav.propTypes = {
  messages: PropTypes.array
}

const mapStateToProps = state => ({
  messages: state.message.messages
})

export default connect(mapStateToProps)(SideNav)
