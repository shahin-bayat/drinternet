// ! revised
import styled from 'styled-components'
import Link from 'next/link'

const SideDrawer_ = styled.div`
  height: 100vh;
  background: ${props => props.theme.color_primary};
  width: 100%;
  /* max-width: 40rem; */
  position: fixed;
  right: 0;
  top: 6rem;
  z-index: 300;
  transform: ${props =>
    props.isDrawerOpen ? 'translateX(0)' : 'translateX(100%)'};
  transition: all 0.2s ease-in;

  @media (min-width: ${props => props.theme.bp_small}) {
    display: none;
  }

  ul {
    display: flex;
    flex-direction: column;
    list-style: none;
    padding: 3rem 2rem;

    li {
      padding: 1rem 0;

      a {
        text-decoration: none;
        color: ${props => props.theme.color_white};
        font-size: 1.6rem;
      }
    }
  }
`

const SideDrawer = ({ isDrawerOpen }) => {
  return (
    <SideDrawer_ isDrawerOpen={isDrawerOpen}>
      <ul>
        <li>
          <Link href='/'>
            <a>خانه</a>
          </Link>
        </li>
        <li>
          <Link href='/services'>
            <a>خدمات</a>
          </Link>
        </li>
        <li>
          <Link href='/blog'>
            <a>مجله الکترونیکی</a>
          </Link>
        </li>
        <li>
          <Link href='/contact-us'>
            <a>ارتباط با ما</a>
          </Link>
        </li>
      </ul>
    </SideDrawer_>
  )
}

export default SideDrawer
