import { ThemeProvider, createGlobalStyle } from 'styled-components'

import Meta from './../Meta'
import Nav from './Nav'
import Footer from './Footer'
import { ToastContainer } from 'react-toastify'

import { useState } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

const theme = {
  maxWidth: '120rem',
  color_grey_light: '#f7f7f7',
  color_grey_light_2: '#e4e0dc',
  color_grey_light_3: '#9f9f9f',
  color_grey_dark: '#333',
  color_grey: '#616161',
  color_orange: '#ed6f00',
  color_white: '#fff',
  color_black: '#252525',
  color_primary: '#288ED9',
  color_warning: '#cc3300',
  color_confirm: '#4caf50',
  color_valid: '#00B700',
  color_green: '#f5fdf0',
  border: '1px solid #e0e0e2',
  border_radius: '0.5rem',
  font_family: 'iranyekan',
  box_shadow_light: '0 12px 12px 0 hsla(0,0%,70.6%,.11)',
  box_shadow: '0 5px 8px 0 rgba(0, 0, 0, 0.2),0 7px 20px 0 rgba(0,0,0,0.17)',
  bp_largest: '75em', //1200px / 16 = 75em
  bp_large: '68.75em', //1100px
  bp_medium: '57.25em', //900px
  bp_small: '37.5em', //600px
  bp_smallest: '31.25em', //500px
}

const GlobalStyle = createGlobalStyle`
html {
    direction: rtl;
    font-size: 62.5%; /* 10px / 16 = 62.50% */
    scroll-behavior: smooth;

    @media (min-width: ${theme.bp_largest}) {
      font-size: 68.75%; /* 11px / 16 = 68.75% */
    }
    @media (max-width: ${theme.bp_small}) {
      font-size: 56.25%; /* 9px / 16 = 56.25% */
    }
    @media (max-width: ${theme.bp_largest}) {
      font-size: 50%; /* 8px / 16 = 50% */
    }

  }


  *, *:before, *:after {
    margin: 0;
    padding: 0;
    box-sizing: inherit;
  }

  body {
    box-sizing: border-box;
    font-family: iranyekan  !important;
    overflow: ${props => (props.modal === false ? 'visible' : 'hidden')};
    overflow-x: hidden;
  }

  h1 {
      font-size: 3.5rem;
      @media (max-width: ${theme.bp_medium}) {
      font-size: 3.5rem;
    }
  }

  h2 {
      font-size: 3rem;
      color: ${props => props.theme.color_primary};
  }
  
  h3 {
    font-size: 1.8rem;
    color: ${props => props.theme.color_grey}
  }

  a {
    font-size: 1.4rem;
  }

   p {
    font-size: 1.4rem;
    line-height: 1.8;
    color: ${props => props.theme.color_grey};
  }

  span {
    font-size: 1.2rem;
  }

  em {
    font-style: normal;
  }

  label {
    font-size: 1.3rem;
  }

  button {
    font-size: 1.5rem;
  }

  input{
    font-size: 1.4rem;
  }
`

const BarPath = [
  '/login',
  '/register',
  '/forgotPassword',
  '/resetPassword',
  '/profile/change-password',
]

const FooterPath = [
  '/login',
  '/register',
  '/forgotPassword',
  '/resetPassword',
  '/profile/change-password',
  '/profile',
  '/admin',
]
const Page = props => {
  const { pagePath, modal } = props
  let showBar = true
  let showFooter = true
  for (const page of BarPath) {
    if (pagePath.startsWith(page)) {
      showBar = false
    }
  }
  for (const page of FooterPath) {
    if (pagePath.startsWith(page)) {
      showFooter = false
    }
  }
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle modal={modal} />
      <Meta />
      {showBar && <Nav />}
      <ToastContainer rtl className='toast-container' toastClassName='toast' />
      {props.children}
      {showFooter && <Footer />}
    </ThemeProvider>
  )
}

Page.propTypes = {
  modal: PropTypes.bool.isRequired,
}

const mapStateToProps = state => {
  return {
    modal: state.modal.some(item => item.modal === true),
  }
}

export default connect(mapStateToProps)(Page)
