// ! revised
import styled from 'styled-components'
import Router from 'next/router'

import Spacer from '../styles/Spacer'
import PasswordSVG from '../../images/SVG/browser.svg'
import LogoutSVG from '../../images/SVG/logout.svg'

const SideProfile_ = styled.div`
  display: flex;
  flex-direction: column;
  background: ${props => props.theme.color_white};
  border: ${props => props.theme.border};
  box-shadow: ${props => props.theme.box_shadow_light};

  a {
    text-decoration: none;
    color: ${props => props.theme.color_grey};
  }

  h3,
  p {
    padding: 1rem 2rem;
  }

  p {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  h3 {
    border-bottom: ${props => props.theme.border};
  }

  span {
    padding: 0 1rem;
  }

  .item-box {
    display: grid;
    grid-template-columns: 1fr 1fr;
  }

  div[class^='item-box__'] {
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
    height: 5rem;
    border-top: ${props => props.theme.border};
    min-height: 6.65rem;
    padding: 1rem;
    cursor: pointer;
    transition: all 0.2s ease;

    :hover {
      background: ${props => props.theme.color_grey_light};
    }

    :first-child {
      border-left: ${props => props.theme.border};
    }
    a {
      font-size: 1.2rem;
    }
  }
`

const SideProfile = props => {
  const { user, handleLogout } = props

  return (
    <SideProfile_>
      <h3>{user.name}</h3>
      <p>
        سطح کاربری:
        <span>{user.role === 'user' ? 'کاربر عادی' : 'کاربر ویژه'}</span>
      </p>
      <p>
        امتیاز شما:
        <span>- امتیاز</span>
      </p>
      <Spacer />
      <div className='item-box'>
        <div
          onClick={e => Router.push('/profile/change-password')}
          className='item-box__password'
        >
          <PasswordSVG fill='#616161' />
          <a href='/profile/change-password'>تغییر رمز</a>
        </div>
        <div onClick={handleLogout} className='item-box__logout'>
          <LogoutSVG fill='#616161' />
          <a href=''>خروج از حساب</a>
        </div>
      </div>
    </SideProfile_>
  )
}

export default SideProfile
