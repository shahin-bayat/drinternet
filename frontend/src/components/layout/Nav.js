// ! revised
import { useState, Fragment } from 'react'
import Router from 'next/router'
import styled from 'styled-components'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { logoutUser } from '../../redux/actions/authActions'
import NProgress from 'nprogress'

import NavList from './NavList'
import HamburgerButton from '../HamburgerButton'
import SideDrawer from './SideDrawer'

const Navbar = styled.nav`
  display: flex;
  align-items: center;
  color: ${props => props.theme.color_grey};
  box-shadow: ${props => props.theme.box_shadow_light};
  border-bottom: ${props => props.theme.border};
  z-index: 150;
  padding: 0 1rem;
  width: 100%;
  background-color: ${props => props.theme.color_white};
  position: fixed;
`

const Inner = styled.div`
  max-width: ${props => props.theme.maxWidth};
  width: 100%;
  margin: 0 auto;
`

const Nav = ({ isAuthenticated, logoutUser, role, name, modal }) => {
  const [isDrawerOpen, setIsDrawerOpen] = useState(false)

  Router.onRouteChangeStart = () => {
    NProgress.start()
    // Close Side Drawer
    setIsDrawerOpen(false)
  }
  Router.onRouteChangeComplete = () => {
    NProgress.done()
  }
  Router.onRouteChangeError = () => {
    NProgress.done()
  }

  return (
    <Fragment>
      {!modal && (
        <Navbar>
          <HamburgerButton
            setIsDrawerOpen={setIsDrawerOpen}
            isDrawerOpen={isDrawerOpen}
          />
          <SideDrawer isDrawerOpen={isDrawerOpen} />
          <Inner>
            <NavList
              isAuthenticated={isAuthenticated}
              role={role}
              name={name}
              logout={logoutUser}
            />
          </Inner>
        </Navbar>
      )}
    </Fragment>
  )
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.authentication.user ? true : false,
    role: state.authentication.user ? state.authentication.user.role : null,
    name: state.authentication.user ? state.authentication.user.name : null,
    modal: state.modal.some(item => item.modal === true),
  }
}

Nav.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  modal: PropTypes.bool.isRequired,
}

export default connect(mapStateToProps, { logoutUser })(Nav)
