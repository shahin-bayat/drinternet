// ! revised
import React from "react"
import styled from "styled-components"

const Pagination_ = styled.div`
  padding: 2rem 2rem;
  ul.pagination {
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(2rem, 1fr));
    grid-gap: 2px;
    justify-items: center;
    list-style: none;

    li {
      border: ${props => props.theme.border};
      width: 100%;
      text-align: center;
      cursor: pointer;

      &.active {
        background-color: ${props => props.theme.color_primary};
        color: ${props => props.theme.color_white};
      }

      span {
        font-size: 1.3rem;
      }
    }
  }
`

const Pagination = ({ numberOfPages, currentPage, setCurrentPage }) => {
  const pageLinks = []
  for (let p = 1; p <= numberOfPages; p++) {
    let active = currentPage === p ? "active" : ""
    pageLinks.push(
      <li
        className={`pagination_page-number ${active}`}
        key={p}
        onClick={() => setCurrentPage(p)}
      >
        <span>{p}</span>
      </li>
    )
  }
  return (
    <Pagination_>
      <ul className='pagination'>{pageLinks}</ul>
    </Pagination_>
  )
}

export default Pagination
