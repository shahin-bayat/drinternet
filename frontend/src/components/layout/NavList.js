// ! revised
import styled from 'styled-components'
import Link from 'next/link'
import Router from 'next/router'

import Spacer from '../styles/Spacer'
import UserIconSVG from '../../images/SVG/user.svg'
import UserIconVerifiedSVG from '../../images/SVG/user_verified.svg'
import DropdownIconSVG from '../../images/SVG/arrow-dropdown.svg'

const NavList_ = styled.ul`
  display: flex;
  flex-direction: row;
  list-style: none;
  align-items: center;
  justify-content: space-between;

  /* direct navbar items */
  > li {
    padding: 2rem 2rem;
    cursor: pointer;
    a {
      text-decoration: none;
      color: ${props => props.theme.color_grey};
    }
    svg {
      width: 2rem;
      height: 2rem;
      align-self: center;
      fill: ${props => props.theme.color_grey};
    }
    :last-child {
      display: flex;
      align-items: center;
      border-radius: ${props => props.theme.border_radius};
      > svg {
        margin-left: 1rem;
      }
    }
    :hover {
      a {
        color: ${props => props.theme.color_primary};
      }
      svg {
        fill: ${props => props.theme.color_primary};
      }
    }

    @media (max-width: ${props => props.theme.bp_small}) {
      display: none;
    }
  }

  /* last navbar item - logged in */
  li.list-item {
    display: flex;
    align-items: center;
    position: relative;
    background-color: transparent;
    border-radius: ${props => props.theme.border_radius};

    &:hover ul.list-item__dropdown {
      display: block;
    }
    /* dropdown box */
    ul.list-item__dropdown {
      display: none;
      position: absolute;
      list-style: none;
      top: 6rem;
      left: 0.5rem;
      background-color: ${props => props.theme.color_white};
      border-radius: 1px;
      box-shadow: ${props => props.theme.box_shadow};
      min-width: 20rem;
      z-index: 200;
      li {
        padding: 1rem 2rem;
        :first-child {
          cursor: auto;
          border-bottom: ${props => props.theme.border};
          span.profile-name {
            display: block;
            font-weight: bold;
            color: ${props => props.theme.color_valid};
          }
        }
        a {
          text-decoration: none;
          color: ${props => props.theme.color_grey};
        }
        :hover {
          a {
            color: ${props => props.theme.color_primary};
          }
        }
      }
    }
  }
`

const NavList = ({ isAuthenticated, role, name, logout }) => {
  return (
    <NavList_>
      <li>
        <Link href='/'>
          <a>خانه</a>
        </Link>
      </li>
      <li>
        <Link href='/services'>
          <a>خدمات</a>
        </Link>
      </li>
      <li>
        <Link href='/blog'>
          <a>مجله الکترونیکی</a>
        </Link>
      </li>
      <li>
        <Link href='/contact-us'>
          <a>ارتباط با ما</a>
        </Link>
      </li>
      <Spacer />
      {isAuthenticated ? (
        <li className='list-item'>
          <div className='items'>
            <UserIconVerifiedSVG
              className='user-icon'
              onClick={e => Router.push('/profile')}
            />
            <DropdownIconSVG width='1.5rem' height='1.5rem' />
          </div>
          <ul className='list-item__dropdown'>
            <li>
              <span className='profile-name'>{name}</span>
            </li>
            <li>
              {role === 'user' && <a href='/profile'>پروفایل شخصی</a>}
              {role === 'admin' && <a href='/admin'>پنل ادمین</a>}
            </li>
            <li>
              <a href='/' onClick={e => logout()}>
                خروج
              </a>
            </li>
          </ul>
        </li>
      ) : (
        <li>
          <UserIconSVG className='user-icon' />
          <a href='/login'>ورود به حساب کاربری</a>
        </li>
      )}
    </NavList_>
  )
}

export default NavList
