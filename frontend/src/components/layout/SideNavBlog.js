// ! revised
import Link from 'next/link'
import styled from 'styled-components'
import SideNav from '../styles/SideNav'
import NavTitle from '../styles/NavTitle'
import { navItems } from '../../utils/BlogCategories'

const NavItems_ = styled.ul`
  display: flex;
  flex-direction: column;
  list-style: none;
  align-items: flex-start;
  > li {
    width: 100%;
    padding: 2rem 2rem;
    cursor: pointer;
    a {
      text-decoration: none;
      color: ${props => props.theme.color_grey};
    }

    :hover {
      background-color: ${props => props.theme.color_grey_light};
      a {
        color: ${props => props.theme.color_primary};
      }
    }
  }

  @media (max-width: ${props => props.theme.bp_medium}) {
    flex-direction: row;
    text-align: center;
  }
`

const SideNavBlog = () => {
  const navList = navItems.map(item => {
    return (
      <li key={item.id}>
        <Link href={`/blog/category/[category]`} as={item.location}>
          <a>{item.link}</a>
        </Link>
      </li>
    )
  })
  return (
    <SideNav>
      <NavItems_>
        <NavTitle invisible>
          <div className='nav-title--big'>موضوعات</div>
        </NavTitle>
        {navList}
      </NavItems_>
    </SideNav>
  )
}

export default SideNavBlog
