import RadioGroup_ from "./styles/RadioGroup"

const RadioGroup = ({ label, id, value, checked, onChange }) => {
  return (
    <RadioGroup_>
      <input
        type='radio'
        id={id}
        value={value}
        checked={checked}
        onChange={onChange}
      />
      <label htmlFor={id}>
        <span />
        {label}
      </label>
    </RadioGroup_>
  )
}

export default RadioGroup
