import styled from 'styled-components'

const Background_ = styled.div.attrs(props => ({
  image: props.img || '',
  minHeight: props.minHeight || '50rem',
  backgroundPosition: props.backgroundPosition || 'bottom right',
}))`
  height: ${props => props.minHeight};
  width: 100%;
  background: ${props => `url(${props.image})`};
  background-size: cover;
  background-repeat: no-repeat;
  background-position: ${props => props.backgroundPosition};

  @media (max-width: ${props => props.theme.bp_medium}) {
  }
  @media (max-width: ${props => props.theme.bp_small}) {
  }
  @media (max-width: ${props => props.theme.bp_smallest}) {
    background-position: ${props => props.left && 'left'};
  }
`

const Background = ({ img, minHeight, backgroundPosition }) => {
  return (
    <Background_
      img={img}
      minHeight={minHeight}
      backgroundPosition={backgroundPosition}
    />
  )
}

export default Background
