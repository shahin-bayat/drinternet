import styled from 'styled-components'
import Container from './styles/Container'
import Breadcrumb from './layout/Breadcrumb'
import PostTags from './PostTags'
import Comments from './Comments'
import HR from './styles/HR'
import changeTimeZone from '../utils/time'
import Head from 'next/head'

const PostLayout = ({ data }) => {
  const { tehranTime, jDate } = changeTimeZone(data.createdAt)
  return (
    <Post_>
      <Head>
        <meta property='og:title' content={data.title} key='og_title' />
        <meta property='og:type' content='article' key='og_type' />
        <meta property='og:image' content={data.postImage} key='og_image' />
        <title>{data.title}</title>
      </Head>
      <Container alignItems='flex-start'>
        <PostLayout_>
          <div className='main-content' style={{ backgroundColor: '#fff' }}>
            <Breadcrumb
              category={data.category}
              subcategory={data.subcategory}
              title={data.title}
            />
            <PostTitle>
              <h1>{data.title}</h1>
              <p className='post__date'>
                تاریخ انتشار: <span className='date'>{jDate}</span>
                {' | '}
                <span className='time'>{tehranTime}</span>
              </p>
            </PostTitle>
            <img className='post__main-image' src={data.postImage} alt='' />
            <PostBody dangerouslySetInnerHTML={{ __html: data.body }} />
            <PostTags tags={data.tags} />
            <HR />
            <Comments comments={data.comments} post={data} />
          </div>
          <Ads className='side left-side'>
            <p>پنل تبلیغات</p>
          </Ads>
        </PostLayout_>
      </Container>
    </Post_>
  )
}

const Ads = styled.aside`
  background-color: #fff;

  p {
    font-size: 1.4rem;
    color: ${props => props.theme.color_grey_light_3};
    text-align: center;
    padding: 1.5rem;
    border-bottom: ${props => props.theme.border};
  }
`

const Post_ = styled.section`
  background: ${props => props.theme.color_grey_light};
  padding: 0 2rem;
  padding-top: 10rem;
  min-height: 100vh;
`

const PostTitle = styled.div`
  margin-bottom: 2rem;

  .post__date,
  .time,
  .date {
    font-size: 1.3rem;
    color: ${props => props.theme.color_grey};
  }

  h1 {
    font-size: 2rem;
    margin: 2rem 0;
    color: ${props => props.theme.color_black};
  }
`

const PostLayout_ = styled.div`
  display: grid;
  grid-template-columns: 1fr minmax(25rem, auto);
  grid-template-areas: 'main-content side-bar';
  grid-gap: 1.5rem;
  margin-bottom: 2rem;

  @media (max-width: ${props => props.theme.bp_medium}) {
    grid-template-columns: 1fr;
  }

  .main-content {
    grid-area: main-content;
    padding: 2.5rem 3rem;
    box-shadow: ${props => props.theme.box_light};
    border: ${props => props.theme.border};
    overflow: auto;

    .post__main-image {
      width: 100%;
      margin: 2rem 0;
    }
  }

  aside.side {
    grid-area: side-bar;
    height: calc(100vh - 10.2rem);
    min-width: 25rem;
    box-shadow: ${props => props.theme.box_light};
    border: ${props => props.theme.border};
    left: 10rem;
    @media (max-width: ${props => props.theme.bp_medium}) {
      display: none;
    }
  }
`

const PostBody = styled.article`
  overflow: hidden;
  h2 {
    font-size: 1.6rem;
    color: ${props => props.theme.color_primary};
  }

  p {
    font-size: 1.4rem;
    color: ${props => props.theme.color_black};
    margin-bottom: 1.5rem;
    text-align: justify;
  }

  img {
    margin: 3rem 0;
    width: 100% !important;
  }

  a {
    text-decoration: none;
    color: ${props => props.theme.color_primary};
  }

  strong {
    text-decoration: none;
    position: relative;
    display: inline-block;
    font-weight: normal;
    background: linear-gradient(
      180deg,
      rgba(255, 255, 255, 0) 50%,
      #c2dff4 50%
    );
  }

  em {
    text-decoration: none;
    font-weight: bold;
  }

  ul {
    list-style: none;
    font-size: 1.4rem;
  }

  ol {
    font-size: 1.4rem;
    margin-right: 2rem;
  }

  blockquote {
    font-size: 1.4rem;
  }

  code {
    background-color: ${props => props.theme.color_grey_light};
    padding: 1rem;
    display: block;
    width: 100%;
    direction: ltr;
    font-size: 1.4rem;
  }
`

export default PostLayout
