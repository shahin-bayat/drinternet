import Head from 'next/head'

const Meta = () => (
  <Head>
    <meta name='viewport' content='width=device-width, initial-scale=1' />
    <meta charSet='utf-8' />
    <title>خدمات اینترنت، وب سایت و امنیت شبکه - دکتر اینترنت</title>
    <meta
      name='description'
      content='دکتر اینترنت خدمات ویژه‌ای در حوزه اینترنت، مشاوره و راه‌اندازی امنیت شبکه، تلفن ثابت اینترنتی و طراحی وب سایت و اپلیکیشن موبایل ارائه می‌دهد.'
    />

    <meta
      property='og:title'
      content='خدمات اینترنت، وب سایت و امنیت شبکه - دکتر اینترنت'
      key='og_title'
    />
    <meta
      property='og:description'
      content='دکتر اینترنت خدمات ویژه‌ای در حوزه اینترنت، مشاوره و راه‌اندازی امنیت شبکه، تلفن ثابت اینترنتی و طراحی وب سایت و اپلیکیشن موبایل ارائه می‌دهد.'
      key='og_description'
    />
    <meta property='og:url' content='https://www.drinternet.ir/' />
    <meta property='og:type' content='website' key='og_type' />
    <meta property='og:image' content='/drinternet.png' key='og_image' />
    <meta property='og:locale' content='fa_IR' />
    <link rel='icon' href='/favicon.ico' />
  </Head>
)

export default Meta
