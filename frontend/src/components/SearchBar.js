// ! revised
// not used
import styled from "styled-components"

import SearchIconSVG from "../images/SVG/search.svg"

const SearchBar_ = styled.div`
  padding: 0 2rem;
  margin: 2rem 0;
  display: flex;
  justify-content: center;
  align-items: center;
`

const SearchBox = styled.form`
  display: flex;
  min-width: 70%;
  align-items: stretch;
  input {
    width: 100%;
    padding: 1.5rem 1rem;
    border: ${props => props.theme.border};
    transition: all 0.2s ease;
    font-family: inherit;
    border-top-left-radius: 0.5rem;
    border-bottom-left-radius: 0.5rem;

    &::placeholder {
      color: ${props => props.theme.color_grey_light_3};
      font-size: 1.2rem;
    }

    &:focus {
      outline: none;
      border: 1px solid ${props => props.theme.color_primary};
    }
  }
  button {
    cursor: pointer;
    padding: 0.75rem 1.25rem;
    outline: none;
    background-color: ${props => props.theme.color_orange};
    border-color: transparent;
    border-top-right-radius: 0.5rem;
    border-bottom-right-radius: 0.5rem;
  }
`
const SearchBar = () => {
  return (
    <SearchBar_>
      <SearchBox>
        <button>
          <SearchIconSVG width='2rem' height='2rem' fill='#fff' />
        </button>
        <input type='text' placeholder='جستجو در فروشگاه دکتر اینترنت...' />
      </SearchBox>
    </SearchBar_>
  )
}

export default SearchBar
