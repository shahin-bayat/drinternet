// ! revised
import React, { useState, useEffect } from "react"
import {
  EditorState,
  convertToRaw,
  convertFromHTML,
  ContentState
} from "draft-js"
import { Editor } from "react-draft-wysiwyg"
import draftToHtml from "draftjs-to-html"

const NextEditorEdit = props => {
  const contentBlocks = convertFromHTML(props.html)

  const sampleEditorContent = ContentState.createFromBlockArray(contentBlocks)

  const [editorState, setEditorState] = useState(
    EditorState.createWithContent(sampleEditorContent)
  )

  useEffect(() => {
    props.setHtmlBody(
      draftToHtml(convertToRaw(editorState.getCurrentContent()))
    )
  }, [editorState])

  return (
    <div>
      <Editor
        editorState={editorState}
        wrapperClassName='demo-wrapper'
        editorClassName='demo-editor'
        toolbarClassName='toolbar-class'
        onEditorStateChange={setEditorState}
        toolbar={{
          options: [
            "inline",
            "blockType",
            "list",
            "colorPicker",
            "link",
            "embedded",
            "image"
          ],
          blockType: {
            inDropdown: false,
            options: ["Normal", "H1", "H2", "Blockquote"]
          },
          image: {
            alt: { present: true, mandatory: true },
            alignmentEnabled: false,
            defaultSize: false
          }
        }}
      />
    </div>
  )
}

export default NextEditorEdit
