import Link from "next/link"
import { useState } from "react"
import { connect } from "react-redux"
import PropTypes from "prop-types"
import { loginUser } from "../redux/actions/authActions"

import Modal from "./styles/Modal"
import AccountBox from "./styles/AccountBox"
import Form from "./styles/Form"
import InputGroup from "./styles/InputGroup"
import Button from "./styles/Button"

import LoginSVG from "../images/SVG/login.svg"

const LoginModal = ({ showLogin, onClick, loginUser, setShowLogin }) => {
  const handleSubmit = async e => {
    e.preventDefault()
    await loginUser(user, false)
    setUser({ email: "", password: "" })
    setShowLogin(false)
  }

  const [user, setUser] = useState({
    email: "",
    password: ""
  })
  const { email, password } = user

  const handleChange = e => {
    setUser({ ...user, [e.target.name]: e.target.value })
  }

  return (
    <Modal show={showLogin} onClick={onClick}>
      <div className='modal__content' style={{ padding: "0" }}>
        {/* <CloseButton onClick={onClick} />
        <SectionHeader style={{ alignSelf: "flex-start" }}>
          <p>لطفا برای ارسال دیدگاه وارد شوید.</p>
        </SectionHeader> */}
        <AccountBox>
          <h3>ورود به حساب کاربری</h3>
          <Form onSubmit={handleSubmit}>
            <InputGroup direction='ltr'>
              <input
                type='email'
                name='email'
                id='email'
                value={email}
                onChange={handleChange}
                placeholder='example@gmail.com'
                required
              />
              <label htmlFor='email'>ایمیل</label>
            </InputGroup>
            <InputGroup direction='ltr'>
              <input
                type='password'
                name='password'
                id='password'
                value={password}
                onChange={handleChange}
                required
              />
              <label htmlFor='password'>رمز عبور</label>
              <div>
                <Link href='/forgotPassword'>
                  <a>رمز عبور خود را فراموش کرده‌ام</a>
                </Link>
              </div>
            </InputGroup>
            <Button square primary stretch>
              <LoginSVG fill='white' />
              وارد شوید
            </Button>
          </Form>
          <div className='account-box__footer'>
            <p>
              کاربر جدید هستید؟ &nbsp;
              <a href='/register'>ثبت نام در دکتر اینترنت</a>
            </p>
          </div>
        </AccountBox>
      </div>
    </Modal>
  )
}

LoginModal.propTypes = {
  loginUser: PropTypes.func.isRequired
}

export default connect(null, { loginUser })(LoginModal)
