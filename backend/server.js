const stoppable = require("stoppable")
const app = require("./app")
const server = stoppable(require("http").Server(app))

const PORT = process.env.PORT || 5000

// quit on ctrl-c when running docker in terminal
process.on("SIGINT", function onSigint() {
  console.info(
    "Got SIGINT (aka ctrl-c in docker). Graceful shutdown ",
    new Date().toISOString()
  )
  shutdown()
})

// quit properly on docker stop
process.on("SIGTERM", function onSigterm() {
  console.info(
    "Got SIGTERM (docker container stop). Graceful shutdown ",
    new Date().toISOString()
  )
  shutdown()
})

// shut down server
function shutdown() {
  console.info("starting stoppable")
  server.stop() // this might take a while depending on connections
  process.exit()
}

server.listen(
  PORT,
  // eslint-disable-next-line no-console
  console.log(
    `Server running in ${process.env.NODE_ENV} mode on port ${PORT}`.yellow.bold
  )
)

// handling UNHANDLED REJECTION like problem connecting to DB - like a last safety net
process.on("unhandledRejection", err => {
  console.log("UNHANDLED REJECTION! 💥 Shutting down...")
  console.log(err.name, err.message)
  // bad method to immediately stop process. we first close the server and then. we should restart the server after this and there tools to do so
  server.close(() => {
    process.exit(1)
  })
})
