const mongoose = require("mongoose")
const validator = require("validator")
const bcrypt = require("bcryptjs")
const crypto = require("crypto")

const userSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "Please tell us your name"]
    },
    role: {
      type: String,
      enum: ["user", "admin"],
      default: "user"
    },
    email: {
      type: String,
      required: [true, "Please provide your email"],
      unique: true,
      lowercase: true,
      validate: [validator.isEmail, "Please provide a valid email"]
    },
    password: {
      type: String,
      required: [true, "Please provide a password'"],
      minlength: 6,
      select: false
    },
    phone: {
      type: String
    },
    birthday: {
      type: String
    },
    passwordResetToken: String,
    passwordResetExpires: Date,
    active: {
      type: Boolean,
      default: true,
      select: false
    }
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
  }
)

userSchema.virtual("cart", {
  ref: "CartItem",
  localField: "_id",
  foreignField: "user"
})

userSchema.pre(/^find/, function(next) {
  this.populate({
    path: "cart",
    populate: {
      path: "product",
      select: "name price images"
    }
  })
  next()
})

//! Middlewares
// hashing passwords
userSchema.pre("save", async function(next) {
  if (!this.isModified("password")) return next()
  this.password = await bcrypt.hash(this.password, 12)
  next()
})

// showing only active users
userSchema.pre(/^find/, function(next) {
  this.find({ active: { $ne: false } })
  next()
})

//! Instance Methods
// validating passwords
userSchema.methods.correctPassword = async function(candidatePassword) {
  return await bcrypt.compare(candidatePassword, this.password)
}
// Generate password reset token
userSchema.methods.createPasswordResetToken = function() {
  const resetToken = crypto.randomBytes(32).toString("hex")

  this.passwordResetToken = crypto
    .createHash("sha256")
    .update(resetToken)
    .digest("hex")

  // the problem come from UTC and Local time 5h = 5 * 60 * 60 * 1000
  this.passwordResetExpires = Date.now() + 5 * 60 * 60 * 1000
  return resetToken
}

const User = mongoose.model("User", userSchema)

module.exports = User
