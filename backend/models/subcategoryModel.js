const mongoose = require("mongoose")

const subcategorySchema = mongoose.Schema({
  name: {
    type: String,
    required: [true, "A subcategory should have a name"],
    unique: true,
    trim: true
  },
  category: {
    type: mongoose.Schema.ObjectId,
    ref: "Category",
    required: [true, "A product should have a category"]
  }
})

const Subcategory = mongoose.model("Subcategory", subcategorySchema)

module.exports = Subcategory
