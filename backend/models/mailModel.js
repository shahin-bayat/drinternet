const mongoose = require("mongoose")

const mailSchema = mongoose.Schema({
  service: {
    type: String,
    required: [true, "A mail can not be without service!"]
  },
  name: {
    type: String,
    required: [true, "A mail should have a name"]
  },
  phone: {
    type: String,
    require: [true, "A mail should have a phone number"]
  },
  additional: {
    type: {}
  },
  sentAt: {
    type: Date,
    default: Date.now
  },
  isRead: {
    type: Boolean,
    default: false
  }
})

const Mail = mongoose.model("Mail", mailSchema)

module.exports = Mail
