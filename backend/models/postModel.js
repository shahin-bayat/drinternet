const mongoose = require("mongoose")

const postSchema = mongoose.Schema({
  title: {
    type: String,
    required: [true, "Please provide a post title"]
  },
  category: {
    type: String,
    enum: ["تکنولوژی", "آموزش", "نقد و بررسی", "اخبار"],
    required: [true, "Please provide a post category"]
  },
  subcategory: {
    type: String,
    enum: [
      "موبایل و گجت",
      "دوربین",
      "اینترنت",
      "سخت افزار",
      "نرم‌افزار و اپلیکیشن",
      "صوتی و تصویری",
      "برنامه‌نویسی",
      "نامشخص"
    ]
  },
  tags: {
    type: [String]
  },
  excerpt: {
    type: String,
    required: [true, "Please provide a post excerpt"]
  },
  fileName: {
    type: String,
    required: [true, "Please provide a post filename"],
    unique: true
  },
  postImage: {
    type: String,
    required: [true, "Please provide a post image url"]
  },
  slug: {
    type: String,
    unique: true
  },
  decodedSlug: {
    type: String
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  published: {
    type: Boolean,
    default: false
  },
  isHighlighted: {
    type: Boolean,
    default: false
  }
})

postSchema.virtual("comments", {
  ref: "Comment",
  foreignField: "post",
  localField: "_id"
})

postSchema.pre("save", function(next) {
  this.slug = this.title
    .trim()
    .split(" ")
    .join("-")
    .replace(/[!?@#$%^&*]/g, "")

  next()
})
postSchema.pre("save", function(next) {
  this.decodedSlug = encodeURI(this.slug)

  next()
})

postSchema.pre(/^find/, function(next) {
  // "this" points to current query
  // this.find({ active: { $ne: false } })
  next()
})

const Post = mongoose.model("Post", postSchema)

module.exports = Post
