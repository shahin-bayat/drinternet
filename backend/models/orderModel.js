const mongoose = require("mongoose")

const orderSchema = mongoose.Schema({
  items: [
    {
      quantity: Number,
      price: Number,
      productName: String,
      productSubcategory: {
        type: mongoose.Schema.ObjectId,
        ref: "Subcategory"
      },
      image: String
    }
  ],
  totalPrice: Number,
  user: {
    type: mongoose.Schema.ObjectId,
    ref: "User",
    required: [true, "A cart should belong to a user"]
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
})

const Order = mongoose.model("Order", orderSchema)

module.exports = Order
