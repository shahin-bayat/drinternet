const mongoose = require("mongoose")

const commentSchema = mongoose.Schema({
  body: {
    type: String,
    required: [true, "Please provide a comment text"]
  },
  sentAt: {
    type: Date,
    default: Date.now
  },
  post: {
    type: mongoose.Schema.ObjectId,
    ref: "Post"
  },
  product: {
    type: mongoose.Schema.ObjectId,
    ref: "Product"
  },
  from: {
    type: mongoose.Schema.ObjectId,
    ref: "User",
    required: [true, "A comment should belong to a user"]
  }
})

commentSchema.pre(/^find/, function(next) {
  this.populate({
    path: "from",
    select: "name"
  })

  next()
})

const Comment = mongoose.model("Comment", commentSchema)

module.exports = Comment
