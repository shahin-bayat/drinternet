const mongoose = require("mongoose")

const cartItemSchema = mongoose.Schema({
  user: {
    type: mongoose.Schema.ObjectId,
    ref: "User",
    required: [true, "A cart should belong to a user"]
  },
  product: {
    type: mongoose.Schema.ObjectId,
    ref: "Product",
    required: [true, "A cart item should have a product"]
  },
  quantity: {
    type: Number,
    default: 1
  }
})

const CartItem = mongoose.model("CartItem", cartItemSchema)

module.exports = CartItem
