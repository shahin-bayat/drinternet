const mongoose = require("mongoose")

const messageSchema = mongoose.Schema({
  subject: {
    type: String,
    max: [60, "Please provide a subject with maximum 60 characters"],
    required: [true, "A message can not be without subject!"]
  },
  body: {
    type: String,
    required: [true, "A message can not be without body!"]
  },
  sentAt: {
    type: Date,
    default: Date.now
  },
  to: {
    type: mongoose.Schema.ObjectId,
    ref: "User",
    required: [true, "A message must be sent to someone!"]
  },
  from: {
    type: mongoose.Schema.ObjectId,
    ref: "User",
    required: [true, "A message is sent from someone!"]
  },
  isPublic: {
    type: Boolean,
    default: false
  },
  isRead: {
    type: Boolean,
    default: false
  }
})

const Message = mongoose.model("Message", messageSchema)

module.exports = Message
