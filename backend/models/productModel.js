const mongoose = require("mongoose")

const productSchema = mongoose.Schema(
  {
    name: {
      type: String,
      unique: true,
      required: [true, "A product should have a name"],
      trim: true
    },
    description: {
      type: String,
      required: [true, "A product should have a description"],
      trim: true
    },
    price: {
      type: Number,
      required: [true, "A product should have a price"]
    },
    quantity: {
      type: Number,
      required: [true, "A product should have a quantity"]
    },
    createdAt: {
      type: Date,
      default: Date.now
    },
    subcategory: {
      type: mongoose.Schema.ObjectId,
      ref: "Subcategory"
    },
    attributes: [
      {
        name: {
          type: String,
          maxlength: [
            40,
            "An attribute must have less or equal than 40 characters"
          ],
          required: [true, "An attribute should have a name"]
        },
        value: {
          type: String,
          required: [true, "An attribute should have a value"]
        },
        id: {
          type: String,
          required: [true, "An attribute should have an id"]
        }
      }
    ],
    images: [String],
    tags: [
      {
        type: String,
        maxlength: [25, "A tag must have less or equal than 25 characters"],
        required: [true, "A tag should have a name"]
      }
    ]
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
  }
)

productSchema.virtual("comments", {
  ref: "Comment",
  localField: "_id",
  foreignField: "product"
})

productSchema.pre(/^find/, function(next) {
  this.populate({ path: "subcategory", select: "name" })
  next()
})

const Product = mongoose.model("Product", productSchema)

module.exports = Product
