const mongoose = require("mongoose")

const categorySchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "A category should have a name"],
      unique: true,
      trim: true
    },
    id: false
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
  }
)

categorySchema.virtual("subcategories", {
  ref: "Subcategory",
  localField: "_id",
  foreignField: "category"
})

const Category = mongoose.model("Category", categorySchema)

module.exports = Category
