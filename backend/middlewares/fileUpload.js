const multer = require("multer")
const path = require("path")
const storage = multer.memoryStorage()
// const upload = multer({ storage: storage }).single("file")

function checkFileType(file, cb) {
  // allowed ext
  const fileTypes = /jpeg|jpg|png|webp/
  // check ext
  const extName = fileTypes.test(path.extname(file.originalname).toLowerCase())
  // check mime
  const mimeType = fileTypes.test(file.mimetype.toLowerCase().split("/")[1])

  if (mimeType && extName) {
    cb(null, true)
  } else {
    cb("Error: Images Only!")
  }
}

const multipleUpload = multer({
  storage: storage,
  limits: { fileSize: 2000000 },
  fileFilter: function (request, file, cb) {
    checkFileType(file, cb)
  },
}).array("image")

module.exports = multipleUpload
