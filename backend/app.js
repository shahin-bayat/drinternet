const express = require("express")
const dotenv = require("dotenv")
const helmet = require("helmet")
const morgan = require("morgan")
const cookieParser = require("cookie-parser")
const rateLimit = require("express-rate-limit")
const mongoSanitize = require("express-mongo-sanitize")
const xss = require("xss-clean")
const cors = require("cors")
// eslint-disable-next-line no-unused-vars
const color = require("colors")
const ConnectDB = require("./config/db")
const globalErrorHandler = require("./controllers/errorController")
const AppError = require("./utils/appError")

dotenv.config({ path: "./config/config.env" })

const app = express()

//  CONNECT DB
ConnectDB()

// MIDDLEWARES
app.use(helmet())
if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"))
} else {
  app.use(morgan("tiny"))
}
const limiter = rateLimit({
  max: 50, // req per ip
  windowMS: 60 * 60 * 1000, // in 1 hour
  message: "Too many requests from this ip, please try again in an hour!",
})
app.use(mongoSanitize())
app.use("/api", limiter)
app.use(express.json({ limit: "10kb" }))
app.use(cookieParser())
app.use(cors({ origin: true, credentials: true }))
// app.use(express.static("public"))
// ROUTES
app.use("/api/v1/users", require("./routes/userRoutes"))
app.use("/api/v1/posts", require("./routes/postRoutes"))
app.use("/api/v1/messages", require("./routes/messageRoutes"))
app.use("/api/v1/mails", require("./routes/mailRoutes"))
app.use("/api/v1/comments", require("./routes/commentRoutes"))
app.use("/api/v1/products", require("./routes/productRoutes"))

// ! fix this, doesn't work
app.get("/healthcheck", (req, res) => {
  console.log("I am happy and healthy\n")
  res.status(200).send("I am happy and healthy\n")
})

app.all("*", (req, res, next) => {
  next(new AppError(`Can't find ${req.originalUrl} on this server!`, 404))
})

app.use(globalErrorHandler)

module.exports = app
