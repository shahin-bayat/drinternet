const { Router } = require("express")

const commentController = require("../controllers/commentController")
const authController = require("../controllers/authController")

const router = Router()

router.use(authController.protect)

router.post("/", commentController.createComment)
router.delete("/:commentId", commentController.deleteComment)

module.exports = router
