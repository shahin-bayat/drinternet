const { Router } = require("express")
const mailController = require("../controllers/mailController")
const authController = require("../controllers/authController")

const router = Router()

router
  .route("/")
  .post(mailController.createMail)
  .get(
    authController.protect,
    authController.restrictTo("admin"),
    mailController.getAllMails
  )

router.use(authController.protect, authController.restrictTo("admin"))
router
  .route("/:id")
  .get(mailController.getMail)
  .delete(mailController.deleteMail)

module.exports = router
