const { Router } = require("express")

const messageController = require("../controllers/messageController")
const authController = require("../controllers/authController")

const router = Router()

router.use(authController.protect)

router
  .route("/")
  .post(authController.restrictTo("user"), messageController.createMessage)
  .get(messageController.getAllMessages)

router
  .route("/admin")
  .post(
    authController.restrictTo("admin"),
    messageController.createAdminMessage
  )
router
  .route("/:id")
  .delete(messageController.deleteMessage)
  .get(messageController.getMessage)

module.exports = router
