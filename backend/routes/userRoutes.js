const { Router } = require("express")

const authController = require("../controllers/authController")
const userController = require("../controllers/userController")

const router = Router()

// public routes
router.post("/signup", authController.signup)
router.post("/login", authController.login)
router.post("/forgotPassword", authController.forgotPassword)
router.patch("/resetPassword/:token", authController.resetPassword)

// protected routes
router.use(authController.protect)
router.get("/me", userController.getMe)
router.patch("/updateMe", userController.updateMe)
router.delete("/deleteMe", userController.deleteMe)
router.patch("/updateMyPassword", authController.updatePassword)

// admin restricted
router.get("/:userId", userController.getUser)

module.exports = router
