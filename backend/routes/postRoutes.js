const { Router } = require("express")
const multipleUpload = require("../middlewares/fileUpload")

const postController = require("../controllers/postController")
const authController = require("../controllers/authController")

const router = Router()

router
  .route("/top-5-highlights")
  .get(postController.aliasHighlights, postController.getHighlightedPosts)

router.get("/:slug", postController.getPost)

router
  .route("/")
  .get(postController.getPosts)
  .post(
    authController.protect,
    authController.restrictTo("admin"),
    postController.createPost
  )

router.use(authController.protect)
router.use(authController.restrictTo("admin"))

router.post("/images", multipleUpload, postController.uploadImages)
router.patch("/:postId", postController.updatePost)
router.delete("/:postId", postController.deletePost)

module.exports = router
