const { Router } = require("express")

const productController = require("../controllers/productController")
const authController = require("../controllers/authController")
const categoryController = require("../controllers/categoryController")
const cartController = require("../controllers/cartController")
const orderController = require("../controllers/orderController")

const router = Router()

// ! Subcategory Routes
router
  .route("/category/:categoryId/subcategory")
  .post(
    authController.protect,
    authController.restrictTo("admin"),
    categoryController.createSubcategory
  )
  .get(categoryController.getSubcategories)

// ! Category Routes
router
  .route("/category")
  .get(categoryController.getCategories)
  .post(
    authController.protect,
    authController.restrictTo("admin"),
    categoryController.createCategory
  )
router
  .route("/category/:categoryId")
  .get(categoryController.getCategory)
  .delete(
    authController.protect,
    authController.restrictTo("admin"),
    categoryController.deleteCategory
  )
  .patch(
    authController.protect,
    authController.restrictTo("admin"),
    categoryController.updateCategory
  )

router
  .route("/category/:categoryId/subcategory/:subcategoryId")
  .patch(
    authController.protect,
    authController.restrictTo("admin"),
    categoryController.updateSubcategory
  )
  .delete(
    authController.protect,
    authController.restrictTo("admin"),
    categoryController.deleteSubcategory
  )

// ! Product Routes
router
  .route("/")
  .post(
    authController.protect,
    authController.restrictTo("admin"),
    productController.createProduct
  )
  .get(productController.getProducts)

router
  .route("/:productId")
  .get(productController.getProduct)
  .delete(
    authController.protect,
    authController.restrictTo("admin"),
    productController.deleteProduct
  )
  .patch(
    authController.protect,
    authController.restrictTo("admin"),
    productController.editProduct
  )

// ! Cart Routes
router.route("/cart").post(authController.protect, cartController.addToCart)
router
  .route("/cart/:cartItemId")
  .delete(authController.protect, cartController.removeFromCart)

// ! Order Routes
router.route("/order").post(authController.protect, orderController.createOrder)

module.exports = router
