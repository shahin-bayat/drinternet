const mongoose = require("mongoose")
const environments = require("./env")

const connectDB = async () => {
  try {
    const URI = `mongodb://${environments.production.mongodb_username}:${environments.production.mongodb_password}@database:27017/drinternet`
    const conn = await mongoose.connect(URI, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
    })

    // eslint-disable-next-line no-console
    console.log(
      `MongoDB Connected ${conn.connection.host} ${conn.connection.name}`.cyan
        .bold
    )
  } catch (error) {
    console.log(error)
  }
}

module.exports = connectDB
