const dockerSecret = require("./../utils/dockerSecret")

const environments = {}

environments.production = {
  NODE_ENV: "production",
  mongodb_username: dockerSecret(process.env.MONGO_USERNAME_FILE),
  mongodb_password: dockerSecret(process.env.MONGO_PASSWORD_FILE),
  bucket_access_key: dockerSecret(process.env.BUCKET_ACCESS_KEY_FILE),
  bucket_secret_key: dockerSecret(process.env.BUCKET_SECRET_KEY_FILE),
  bucket_endpoint_url: dockerSecret(process.env.BUCKET_ENDPOINT_URL_FILE),
  jwt_secret: dockerSecret(process.env.JWT_SECRET_FILE),
  gmail_password: dockerSecret(process.env.GMAIL_PASSWORD_FILE),
}

module.exports = environments
