const User = require("../models/userModel")
const asyncHandler = require("../utils/handleAsync")
const AppError = require("../utils/appError")
const allowFields = require("../utils/allowFields")

// @desc      get me
// @route     GET /api/v1/users/me
// @access    Private
exports.getMe = asyncHandler(async (req, res, next) => {
  const user = await User.findById(req.user._id)

  if (!user) {
    return next(new AppError("No user found with that ID", 404))
  }

  res.status(200).json({
    status: "success",
    data: {
      data: user
    }
  })
})

// @desc      update me
// @route     PATCH /api/v1/users/updateMe
// @access    Private
exports.updateMe = asyncHandler(async (req, res, next) => {
  if (req.body.password) {
    return next(new AppError("This route is not for password updates", 400))
  }

  const filteredBody = allowFields(
    req.body,
    "name",
    "email",
    "phone",
    "birthday"
  )

  const updatedUser = await User.findByIdAndUpdate(req.user._id, filteredBody, {
    new: true,
    runValidators: true
  })

  res.status(200).json({
    status: "success",
    data: {
      user: updatedUser
    }
  })
})

// @desc      delete me
// @route     DELETE /api/v1/users/updateMe
// @access    Private
exports.deleteMe = asyncHandler(async (req, res, next) => {
  await User.findByIdAndUpdate(req.user._id, { active: false })

  res.status(204).json({
    status: "success",
    data: null
  })
})

// @desc      get user
// @route     GET /api/v1/users/:userId
// @access    Private, Admin
exports.getUser = asyncHandler(async (req, res, next) => {
  const user = await User.findById(req.params.userId).populate({
    path: "cart"
  })

  if (!user) {
    return next(new AppError("No user found with that ID", 404))
  }

  res.status(200).json({
    status: "success",
    data: {
      data: user
    }
  })
})
