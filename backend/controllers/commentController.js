const asyncHandler = require("../utils/handleAsync")
const AppError = require("../utils/appError")
const Comment = require("../models/commentModel")

// @desc      create comment
// @route     POST /api/v1/comments
// @access    Private
exports.createComment = asyncHandler(async (req, res, next) => {
  const { body, post, product } = req.body
  let comment
  if (post) {
    comment = await Comment.create({
      body,
      post,
      from: req.user
    })
  } else {
    comment = await Comment.create({
      body,
      product,
      from: req.user
    })
  }

  res.status(200).json({
    status: "success",
    data: {
      comment
    }
  })
})

// @desc      delete comment
// @route     POST /api/v1/comments/:commentId
// @access    Private
exports.deleteComment = asyncHandler(async (req, res, next) => {
  const comment = await Comment.findByIdAndDelete(req.params.commentId)

  if (!comment) {
    return next(new AppError("No comment found with that ID", 404))
  }

  res.status(204).json({
    status: "success",
    data: null
  })
})
