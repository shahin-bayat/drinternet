const asyncHandler = require("../utils/handleAsync")
const AppError = require("../utils/appError")
const Product = require("../models/productModel")
const Subcategory = require("../models/subcategoryModel")

// @desc      create product
// @route     POST /api/v1/products
// @access    Private, Admin
exports.createProduct = asyncHandler(async (req, res, next) => {
  const product = await Product.create(req.body)

  res.status(200).json({
    status: "success",
    data: { product }
  })
})

// @desc      get products
// @route     GET /api/v1/products
// @access    Public
exports.getProducts = asyncHandler(async (req, res, next) => {
  let query

  if (req.query.subcategory) {
    const _id = await Subcategory.findOne({ name: req.query.subcategory })
    console.log("id", req.query.subcategory)
    if (!_id) {
      return next(new AppError("Subcategory not found", 401))
    }
    query = Product.find({ subcategory: _id })
  } else {
    query = Product.find()
  }

  if (req.query.fields) {
    const fields = req.query.fields.split(",").join(" ")
    query = query.select(fields)
  } else {
    query = query.select("-__v")
  }

  if (req.query.sort) {
    const sortBy = req.query.sort.split(",").join(" ")
    query = query.sort(sortBy)
  } else {
    query.sort("-createdAt")
  }

  const products = await query

  res.status(200).json({
    status: "success",
    data: { products }
  })
})

// @desc      get single product
// @route     GET /api/v1/products/:productId
// @access    Public
exports.getProduct = asyncHandler(async (req, res, next) => {
  const product = await Product.findById(req.params.productId).populate({
    path: "comments"
  })

  if (!product) {
    return next(new AppError("Product not found", 401))
  }

  res.status(200).json({
    status: "success",
    data: { product }
  })
})

// @desc      delete product
// @route     DELETE /api/v1/products/:productId
// @access    Private, Admin
exports.deleteProduct = asyncHandler(async (req, res, next) => {
  const product = await Product.findByIdAndDelete(req.params.productId)

  if (!product) {
    return next(new AppError("Product not found", 401))
  }

  res.status(204).json({
    status: "success",
    data: null
  })
})

// @desc      edit product
// @route     PATCH /api/v1/products/:productId
// @access    Private, Admin
exports.editProduct = asyncHandler(async (req, res, next) => {
  const product = await Product.findByIdAndUpdate(
    req.params.productId,
    req.body,
    {
      runValidators: true,
      new: true
    }
  )

  if (!product) {
    return next(new AppError("Product not found", 401))
  }

  res.status(200).json({
    status: "success",
    data: { product }
  })
})
