const asyncHandler = require("../utils/handleAsync")
const AppError = require("../utils/appError")
const Order = require("../models/orderModel")
const CartItem = require("../models/cartItemModel")

// @desc      create order
// @route     POST /api/v1/products/order
// @access    Private
exports.createOrder = asyncHandler(async (req, res, next) => {
  // const { total } = req.body
  // 1. recalculate the total price (font and back should match)
  const totalPrice = req.user.cart.reduce(
    (tally, cartItem) => tally + cartItem.product.price * cartItem.quantity,
    0
  )
  /* if (total !== totalPrice) {
    return next(new AppError("the total price does not match", 401))
  } */
  // 3. create the charge (FROM ZARINPAL)
  // 4. convert cart items to order items
  const orderItems = req.user.cart.map(cartItem => {
    const orderItem = {
      quantity: cartItem.quantity,
      price: cartItem.product.price,
      productName: cartItem.product.name,
      productSubcategory: cartItem.product.subcategory._id,
      image: cartItem.product.images[0]
    }
    return orderItem
  })
  // 5. create the order
  const order = await Order.create({
    user: req.user._id,
    items: orderItems,
    totalPrice
  })
  // 6. clear user cart, delete cart items, cart is a virtual property of user
  await CartItem.deleteMany({ user: req.user._id })

  // 7. return order to client

  res.status(200).json({ order })
})
