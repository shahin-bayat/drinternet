const { promises: fs } = require("fs")
const AWS = require("aws-sdk")
const asyncHandler = require("../utils/handleAsync")
const {
  createFileName,
  createTagsArray,
  createFilePath,
  bodySanitizer,
} = require("../utils/stringUtils")
const AppError = require("../utils/appError")
const Post = require("../models/postModel")
const Comment = require("../models/commentModel")
const environments = require("../config/env")

// @desc      create post
// @route     POST /api/v1/posts
// @access    Private, Admin
exports.createPost = asyncHandler(async (req, res, next) => {
  const sanitizedBody = bodySanitizer(req.body.body)
  const newFileName = createFileName(req.body.fileName)
  const tagsArray = createTagsArray(req.body.tags)

  createAndRespond()

  async function createAndRespond() {
    try {
      // create post
      const postPromise = Post.create({
        ...req.body,
        fileName: newFileName,
        tags: tagsArray,
      })

      const filePath = createFilePath(newFileName)
      // create file
      const filePromise = fs.writeFile(filePath, sanitizedBody)

      const [post, file] = await Promise.all([postPromise, filePromise])

      // respond
      res.status(200).json({
        status: "success",
        data: {
          post,
        },
      })
    } catch (err) {
      console.log(err)
      return next(new AppError("Post create Error", 404))
    }
  }
})

// @desc      delete post
// @route     DELETE /api/v1/posts/:id
// @access    Private, Admin
exports.deletePost = asyncHandler(async (req, res, next) => {
  const post = await Post.findById(req.params.postId)

  if (!post) {
    return next(new AppError("No post found with that ID", 404))
  }

  const filePath = createFilePath(post.fileName)
  await Post.findByIdAndDelete(req.params.postId)
  // if file exists
  if (fs.access(filePath)) {
    await fs.unlink(filePath)
  }

  res.status(204).json({
    status: "success",
    data: null,
  })
})

// @desc      edit post
// @route     PATCH /api/v1/posts/:postId
// @access    Private
exports.updatePost = asyncHandler(async (req, res, next) => {
  const post = await Post.findById(req.params.postId)
  if (!post) {
    return next(new AppError("No post found with that ID", 404))
  }
  const isBodyEdited = req.body.body && true

  if (isBodyEdited) {
    // editing post itself
    const sanitizedBody = bodySanitizer(req.body.body)
    const tagsArray = createTagsArray(req.body.tags)
    const filePath = createFilePath(post.fileName)
    const editAndRespond = async () => {
      try {
        const postPromise = Post.findByIdAndUpdate(
          req.params.postId,
          { ...req.body, tags: tagsArray, fileName: post.fileName },
          {
            new: true,
            runValidators: true,
          }
        )
        const filePromise = fs.writeFile(filePath, sanitizedBody)

        const [newPost, file] = await Promise.all([postPromise, filePromise])

        res.status(200).json({
          status: "success",
          data: {
            newPost,
          },
        })
      } catch (err) {
        return next(new AppError("Post create Error", 404))
      }
    }
    editAndRespond()
  } else {
    // publish/unpublish post
    const newPost = await Post.findByIdAndUpdate(req.params.postId, req.body, {
      new: true,
      runValidators: true,
    })
    res.status(200).json({
      status: "success",
      data: {
        newPost,
      },
    })
  }
})

// @desc      get single post
// @route     GET /api/v1/posts/:slug
// @access    Public
exports.getPost = asyncHandler(async (req, res, next) => {
  const post = await Post.findOne({ slug: req.params.slug })
    .lean()
    .select("+filePath")
    .populate({ path: "comments" })

  if (!post) {
    return next(new AppError("No post found with that ID", 404))
  }
  // number of comments for each post
  const numComments = await Comment.find({ post: post._id }).countDocuments()

  const readAndRespond = async () => {
    try {
      const filePath = createFilePath(post.fileName)
      const postBody = await fs.readFile(filePath, "utf8")
      res.status(200).json({
        status: "success",
        data: {
          ...post,
          body: postBody,
          numComments,
        },
      })
    } catch (err) {
      return next(new AppError("Post grab error", 404))
    }
  }

  readAndRespond()
})

// @desc      get all posts
// @route     GET /api/v1/posts
// @access    Public
exports.getPosts = asyncHandler(async (req, res, next) => {
  let query

  if (req.query.category) {
    let category

    if (req.query.category === "tech") {
      category = "تکنولوژی"
    } else if (req.query.category === "education") {
      category = "آموزش"
    } else if (req.query.category === "review") {
      category = "نقد و بررسی"
    } else if (req.query.category === "news") {
      category = "اخبار"
    }
    if (req.query.published) {
      query = Post.find({ category, published: true })
    } else {
      query = Post.find({ category })
    }
  } else {
    query = Post.find()
    if (req.query.published) {
      query = Post.find({ published: true })
    } else {
      query = Post.find()
    }
  }

  // fields limiting
  if (req.query.fields) {
    const fields = req.query.fields.split(",").join(" ")
    query = query.select(fields)
  } else {
    query = query.select("-__v")
  }

  // sorting
  if (req.query.sort) {
    const sortBy = req.query.sort.split(",").join(" ")
    query = query.sort(sortBy)
  } else {
    query.sort("-createdAt")
  }

  // pagination
  const numberOfPosts = await Post.find().countDocuments()
  const postsPerPage = 15
  const numberOfPages = Math.ceil(numberOfPosts / postsPerPage)

  const page = req.query.page * 1 || 1
  const limit = req.query.limit * 1 || postsPerPage
  const skip = (page - 1) * limit

  if (req.query.page && skip >= numberOfPosts) {
    return next(new AppError("This page does not exist", 401))
  }

  query = query.skip(skip).limit(limit)

  const posts = await query

  res.status(200).json({
    status: "success",
    data: {
      posts,
      numberOfPages,
      numberOfPosts,
    },
  })
})

// @desc      upload images
// @route     POST /api/v1/posts/images
// @access    Private, Admin

exports.uploadImages = async (req, res, next) => {
  const files = req.files
  const {
    bucket_access_key,
    bucket_secret_key,
    bucket_endpoint_url,
  } = environments.production

  // defining bucket arvan cloud
  let s3bucket = new AWS.S3({
    accessKeyId: bucket_access_key,
    secretAccessKey: bucket_secret_key,
    region: "",
    endpoint: bucket_endpoint_url,
    s3ForcePathStyle: true,
  })

  // upload files to bucket
  files.map((file) => {
    const params = {
      Bucket: "drinternet",
      Key: file.originalname,
      Body: file.buffer,
      ACL: "public-read",
    }
    s3bucket.upload(params, (err, data) => {
      if (err) {
        return res.status(500).json({ error: true, Message: err })
      }
    })
  })

  let items = req.files.map((file) => {
    const url = `https://${"drinternet"}.s3.ir-thr-at1.arvanstorage.com/${
      file.originalname
    }`
    const filename = file.originalname
    return {
      url,
      filename,
    }
  })

  res.status(200).json({ items })
}

// @desc      get highlighted posts
// @route     GET /api/v1/posts/top-5-highlights"
// @access    Public
exports.getHighlightedPosts = async (req, res, next) => {
  let query = Post.find({ isHighlighted: true, published: true })

  // fields limiting
  if (req.query.fields) {
    const fields = req.query.fields.split(",").join(" ")
    query = query.select(fields)
  } else {
    query = query.select("__v")
  }

  // sorting
  if (req.query.sort) {
    const sortBy = req.query.sort.split(",").join(" ")
    query = query.sort(sortBy)
  } else {
    // default sort
    query.sort("-createdAt")
  }

  // limiting
  const limit = req.query.limit * 1 || 50
  query = query.limit(limit)

  const posts = await query

  res.status(200).json({
    status: "success",
    data: {
      posts,
    },
  })
}

exports.aliasHighlights = (req, res, next) => {
  req.query.limit = req.query.limit || "5"
  req.query.sort = "-createdAt"
  req.query.fields = "title,slug,postImage,createdAt,excerpt"

  next()
}
