const mongoose = require("mongoose")
const asyncHandler = require("../utils/handleAsync")
const AppError = require("../utils/appError")
const Category = require("../models/categoryModel")
const Subcategory = require("../models/subcategoryModel")

// @desc      create category
// @route     POST /api/v1/products/category
// @access    Private, Admin
exports.createCategory = asyncHandler(async (req, res, next) => {
  const { name } = req.body

  const category = await Category.create({ name })

  res.status(200).json({
    status: "success",
    data: { category }
  })
})

// @desc      delete category
// @route     POST /api/v1/products/category/:categoryId
// @access    Private, Admin
exports.deleteCategory = asyncHandler(async (req, res, next) => {
  const category = await Category.findByIdAndDelete(req.params.categoryId)

  if (!category) {
    return next(new AppError("No category found with that ID", 404))
  }

  res.status(204).json({
    status: "success",
    data: null
  })
})

// @desc      update category
// @route     PATCH /api/v1/products/category/:categoryId
// @access    Private, Admin
exports.updateCategory = asyncHandler(async (req, res, next) => {
  const updatedCategory = await Category.findByIdAndUpdate(
    req.params.categoryId,
    req.body,
    {
      new: true,
      runValidators: true
    }
  )

  if (!updatedCategory) {
    return next(new AppError("No category found with that ID", 404))
  }

  res.status(200).json({
    status: "success",
    data: {
      updatedCategory
    }
  })
})

// @desc      get all categories
// @route     GET /api/v1/products/category
// @access    Public
exports.getCategories = asyncHandler(async (req, res, next) => {
  const categories = await Category.find()

  res.status(200).json({
    status: "success",
    data: { categories }
  })
})

// @desc      get category with populated subs
// @route     GET /api/v1/products/category/:categoryId
// @access    Public
exports.getCategory = asyncHandler(async (req, res, next) => {
  const category = await Category.findById(req.params.categoryId).populate({
    path: "subcategories",
    select: "name"
  })

  res.status(200).json({
    status: "success",
    data: { category }
  })
})

//! Subcategory Controllers

// @desc      create subcategory
// @route     POST /api/v1/products/category/:categoryId/subcategory
// @access    Private, Admin
exports.createSubcategory = asyncHandler(async (req, res, next) => {
  const { name } = req.body

  const subcategory = await Subcategory.create({
    name,
    category: req.params.categoryId
  })

  res.status(200).json({
    status: "success",
    data: { subcategory }
  })
})

// @desc      delete subcategory
// @route     PATCH /api/v1/products/category/:categoryId/subcategory/:subcategoryId
// @access    Private, Admin
exports.deleteSubcategory = asyncHandler(async (req, res, next) => {
  const subcategory = await Subcategory.findByIdAndDelete(
    req.params.subcategoryId
  )

  if (!subcategory) {
    return next(new AppError("No subcategory found with that ID", 404))
  }

  res.status(204).json({
    status: "success",
    data: null
  })
})

// @desc      update subcategory
// @route     PATCH /api/v1/products/category/:categoryId/subcategory/:subcategoryId
// @access    Private, Admin
exports.updateSubcategory = asyncHandler(async (req, res, next) => {
  const updatedSubcategory = await Subcategory.findByIdAndUpdate(
    req.params.subcategoryId,
    req.body,
    {
      new: true,
      runValidators: true
    }
  )

  if (!updatedSubcategory) {
    return next(new AppError("No subcategory found with that ID", 404))
  }

  res.status(200).json({
    status: "success",
    data: { updatedSubcategory }
  })
})

// @desc      get all subcategories
// @route     GET /api/v1/products/category/:categoryId/subcategory
// @access    Public
exports.getSubcategories = asyncHandler(async (req, res, next) => {
  // ! mongoose id validation
  if (mongoose.Types.ObjectId.isValid(req.params.categoryId) === false) {
    return next(new AppError("Category Id is invalid", 401))
  }
  const subcategories = await Subcategory.find({
    category: req.params.categoryId
  }).populate({ path: "category" })

  res.status(200).json({
    status: "success",
    data: { subcategories }
  })
})
