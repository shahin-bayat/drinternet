const jwt = require("jsonwebtoken")
const crypto = require("crypto")
const { promisify } = require("util")
const asyncHandler = require("../utils/handleAsync")
const AppError = require("../utils/appError")
const sendEmail = require("../utils/email")
const allowFields = require("../utils/allowFields")
const User = require("../models/userModel")
const environments = require("../config/env")

const createSendToken = (user, statusCode, res) => {
  const token = jwt.sign(
    { id: user._id, role: user.role },
    environments.production.jwt_secret,
    {
      expiresIn: process.env.JWT_EXPIRES_IN,
    }
  )

  const cookieOptions = {
    expires: new Date(Date.now() + 6 * 1 * 60 * 60 * 1000), //  6 hr
    httpOnly: true,
  }
  // TODO: TURN ON IN PRODUCTION
  // if (process.env.NODE_ENV === "production") cookieOptions.secure = true

  res.cookie("token", token, cookieOptions)
  // to prevent showing in client side
  user.password = undefined
  // TODO: should I prevent sending role?

  res.status(statusCode).json({
    status: "success",
    data: {
      user,
    },
  })
}

exports.protect = asyncHandler(async (req, res, next) => {
  let cookie = {}
  let token = null
  // to get cookie from headers if the request is send from next SSR
  // Todo: filtering the cookies to get only token
  if (req.headers.cookie) {
    // eslint-disable-next-line prefer-destructuring
    cookie = req.headers.cookie
  } else {
    return next(
      new AppError("You are not logged in! Please log in to get access", 401)
    )
  }
  // in postman cookie is string not JSON
  function isJSON(str) {
    try {
      JSON.parse(str)
    } catch (err) {
      return false
    }
    return true
  }

  // we stringified the cookie in client but postman sends token as string not json
  if (isJSON(cookie)) {
    cookie = JSON.parse(cookie)
    token = cookie.token
  } else {
    // Todo: fixed here
    token = cookie
      .split(";")
      .map((el) => el.trim())
      .find((el) => el.startsWith("token"))

    // token = cookie.split("=")[1]
    token = token.split("=")[1]
  }

  if (!token) {
    return next(
      new AppError("You are not logged in! Please log in to get access", 401)
    )
  }
  const decoded = await promisify(jwt.verify)(
    token,
    environments.production.jwt_secret
  )

  const currentUser = await User.findById(decoded.id)

  if (!currentUser) {
    return next(
      new AppError("The user belonging to this token does no longer exist", 401)
    )
  }

  req.user = currentUser

  next()
})

exports.restrictTo = (...roles) => {
  return (req, res, next) => {
    // roles ['admin','lead-guide]. role = 'user'
    if (!roles.includes(req.user.role)) {
      return next(
        new AppError("You do not have permission to perform this action.", 403)
      )
    }
    next()
  }
}

// @desc      signup user
// @route     POST /api/v1/users/signup
// @access    Public
exports.signup = asyncHandler(async (req, res, next) => {
  const filteredBody = allowFields(req.body, "name", "email", "password")
  const { email } = filteredBody
  const currentUser = await User.findOne({ email })

  if (currentUser) {
    return next(new AppError("User has already registered", 400))
  }

  const newUser = await User.create(filteredBody)

  res.status(200).json({
    status: "success",
    data: {
      user: newUser,
    },
  })
})

// @desc      login user
// @route     POST /api/v1/users/login
// @access    Public
exports.login = asyncHandler(async (req, res, next) => {
  const { email, password } = req.body

  if (!email || !password) {
    return next(new AppError("Please provide email and password", 400))
  }

  const user = await User.findOne({ email }).select("+password")

  if (!user || !(await user.correctPassword(password))) {
    return next(new AppError("Incorrect email or password", 401))
  }

  createSendToken(user, 200, res)
})

// @desc      forgot password
// @route     POST /api/v1/users/forgotPassword
// @access    Public

exports.forgotPassword = asyncHandler(async (req, res, next) => {
  // 1) Get user based on posted email
  const user = await User.findOne({ email: req.body.email })
  if (!user) {
    return next(new AppError("There is no user with this email", 404))
  }
  // 2) Generate a random token
  const resetToken = user.createPasswordResetToken()
  // now if you want to save, all the validation should be done, this is the secret trick: Options: validateBeforeSave
  await user.save({ validateBeforeSave: false })

  const resetLink = `http://drinternet.ir/resetPassword/${resetToken}`

  const message = `کاربر گرامی: ${req.body.email} \n
  سلام\n
  این ایمیل به درخواست شما برای بازیابی کلمه عبور در دکتر اینترنت برای شما ارسال شده است.\n
  برای تغییر کلمه عبور لینک زیر را باز کنید:\n
  \n
  لطفاً توجه داشته باشید، این لینک پس از 5 ساعت منقضی خواهد شد.\n
  ${resetLink}`

  try {
    await sendEmail({
      from: "Dr.Internet Support <support@drinternet.ir>",
      email: user.email,
      subject: "فراموشی رمز عبور",
      message,
    })

    res.status(200).json({
      status: "success",
      message: "Token sent to email!",
    })
  } catch (err) {
    user.passwordResetToken = undefined
    user.passwordResetExpires = undefined
    await user.save({ validateBeforeSave: false })

    return next(
      new AppError("There was an error sending the email. Try again later", 500)
    )
  }
})

// @desc      reset password
// @route     PATCH /api/v1/users/resetPassword
// @access    Public
exports.resetPassword = asyncHandler(async (req, res, next) => {
  // 1) Get user based on the token
  const hashedToken = crypto
    .createHash("sha256")
    .update(req.params.token)
    .digest("hex")

  const user = await User.findOne({
    passwordResetToken: hashedToken,
    passwordResetExpires: { $gt: Date.now() },
  })

  // 2) If token has not expired, an there is user, set the new password
  if (!user) {
    return next(new AppError("Token is invalid or has expired", 400))
  }
  user.password = req.body.password
  user.passwordResetToken = undefined
  user.passwordResetExpires = undefined
  await user.save()

  res.status(200).json({
    status: "success",
    message: "password successfully changed.",
  })
})

// @desc      update password
// @route     PATCH /api/v1/users/updateMyPassword
// @access    Private
exports.updatePassword = asyncHandler(async (req, res, next) => {
  const user = await User.findById(req.user._id).select("+password")

  if (!(await user.correctPassword(req.body.passwordCurrent, user.password))) {
    return next(new AppError("Your current password is wrong", 401))
  }

  // 3) If so, updated password
  // keep in mind that password validation only works on save an create! (findByIdAndUpdate() is not applicable)
  // never user updated method for anything related to password!
  user.password = req.body.password
  await user.save()

  // 4) Log user in, send JWT
  createSendToken(user, 200, res)
})
