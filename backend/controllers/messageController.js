const asyncHandler = require("../utils/handleAsync")
const AppError = require("../utils/appError")
const Message = require("../models/messageModel")
const User = require("../models/userModel")
const sendEmail = require("../utils/email")

// @desc      create message
// @route     POST /api/v1/messages
// @access    Private
exports.createMessage = asyncHandler(async (req, res, next) => {
  const { subject, body } = req.body

  if (!subject || !body) {
    return next(
      new AppError("Please provide the subject and message body", 400)
    )
  }

  const admin = await User.findOne({ role: "admin" }).select({ email: 1 })

  // ! send message to admin
  const message = await Message.create({
    subject,
    body,
    from: req.user._id,
    to: admin._id
  })

  await sendEmail({
    from: req.user.email,
    email: admin.email,
    subject,
    message: body
  })

  res.status(200).json({
    status: "success",
    data: {
      message
    }
  })
})

// @desc      create admin message
// @route     POST /api/v1/messages/admin
// @access    Private, Admin
exports.createAdminMessage = asyncHandler(async (req, res, next) => {
  const { subject, body, groupEmail = false } = req.body
  let { to } = req.body

  // ! send message to all
  if (!to) {
    const allUsers = await User.find({ _id: { $ne: req.user._id } }).select({
      _id: 1,
      email: 1
    })

    const messagePromises = []
    const emailPromises = []

    allUsers.forEach(user => {
      to = user._id
      messagePromises.push(
        Message.create({
          subject,
          body,
          from: req.user._id, //admin id
          to,
          isPublic: true
        })
      )
      // ! send email to all
      if (groupEmail === true) {
        emailPromises.push(
          sendEmail({
            from: "DrInternet Admin <support@drinternet.ir>",
            email: user.email,
            subject,
            message: body
          })
        )
      }
    })
    await Promise.all(messagePromises)
    if (groupEmail === true) await Promise.all(emailPromises)

    res.status(200).json({
      status: "success",
      message: "Message sent to all users"
    })
  } else {
    // ! send message to single user
    const recipient = await User.findById(to).select({ email: 1 })

    const message = await Message.create({
      subject,
      body,
      from: req.user._id,
      to
    })
    // ! send email to single user
    await sendEmail({
      from: "DrInternet Admin <support@drinternet.ir>",
      email: recipient.email,
      subject,
      message: body
    })

    res.status(200).json({
      status: "success",
      data: {
        message
      }
    })
  }
})

// @desc      get all messages
// @route     GET /api/v1/messages
// @access    Private
exports.getAllMessages = asyncHandler(async (req, res, next) => {
  // TODO: adding pagination based on date added

  let query = Message.find({ to: req.user._id })

  // fields limiting
  if (req.query.fields) {
    const fields = req.query.fields.split(",").join(" ")
    query = query.select(fields)
  } else {
    query = query.select("-__v")
  }

  // sorting
  if (req.query.sort) {
    const sortBy = req.query.sort.split(",").join(" ")
    query = query.sort(sortBy)
  } else {
    // default sort
    query.sort("-sentAt")
  }

  // pagination
  const page = req.query.page * 1 || 1
  const limit = req.query.limit * 1 || 10
  const skip = (page - 1) * limit
  if (req.query.page) {
    const numMessages = await Message.find({
      to: req.user._id
    }).countDocuments()
    if (skip >= numMessages)
      return next(new AppError("This page does not exist", 401))
  }
  query = query.skip(skip).limit(limit)

  // making the final query
  const messages = await query

  res.status(200).json({
    status: "success",
    data: {
      messages
    }
  })
})

// @desc      delete Message
// @route     delete /api/v1/messages/:id
// @access    Private
exports.deleteMessage = asyncHandler(async (req, res, next) => {
  const message = await Message.findByIdAndDelete(req.params.id)

  if (!message) {
    return next(new AppError("No message found with that ID", 404))
  }

  res.status(204).json({
    status: "success",
    data: null
  })
})

// @desc      get Message
// @route     GET /api/v1/messages/:id
// @access    Private
exports.getMessage = asyncHandler(async (req, res, next) => {
  const message = await Message.findById(req.params.id)

  if (!message) {
    return next(new AppError("No message found with that ID", 404))
  }

  message.isRead = true

  const currentMessage = await message.save()

  res.status(200).json({
    status: "success",
    data: {
      currentMessage
    }
  })
})
