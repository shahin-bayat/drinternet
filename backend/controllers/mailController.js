const asyncHandler = require("../utils/handleAsync")
const AppError = require("../utils/appError")
const Mail = require("../models/mailModel")
const User = require("../models/userModel")
const sendEmail = require("../utils/email")

// @desc      create mail
// @route     POST /api/v1/mails
// @access    Public
exports.createMail = asyncHandler(async (req, res, next) => {
  const { service, name, phone, ...additional } = req.body

  if (!service || !name || !phone) {
    throw next(new AppError("Required information is not provided", 400))
  }

  // console.log(service, name, phone, additional)
  const newMail = await Mail.create({ service, name, phone, additional })

  const admin = await User.findOne({ role: "admin" }).select({ email: 1 })
  const emailBody = {
    name,
    phone,
    body: additional
  }

  await sendEmail({
    from: process.env.SITE_EMAIL,
    email: admin.email,
    subject: service,
    message: JSON.stringify(emailBody)
  })

  res.status(200).json({
    status: "success",
    data: {
      newMail
    }
  })
})

// @desc      get all mails
// @route     POST /api/v1/mails
// @access    Public
exports.getAllMails = asyncHandler(async (req, res, next) => {
  const mails = await Mail.find().sort("-sentAt")

  res.status(200).json({
    status: "success",
    data: {
      mails
    }
  })
})

// @desc      delete mail
// @route     delete /api/v1/messages/:id
// @access    Private
exports.deleteMail = asyncHandler(async (req, res, next) => {
  const mail = await Mail.findByIdAndDelete(req.params.id)

  if (!mail) {
    return next(new AppError("No mail found with that ID", 404))
  }

  res.status(204).json({
    status: "success",
    data: null
  })
})

// @desc      get mail
// @route     GET /api/v1/messages/:id
// @access    Private
exports.getMail = asyncHandler(async (req, res, next) => {
  const mail = await Mail.findById(req.params.id)

  if (!mail) {
    return next(new AppError("No message found with that ID", 404))
  }

  mail.isRead = true

  const currentMail = await mail.save()

  res.status(200).json({
    status: "success",
    data: {
      currentMail
    }
  })
})
