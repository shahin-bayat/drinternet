const asyncHandler = require("../utils/handleAsync")
const AppError = require("../utils/appError")
const CartItem = require("../models/cartItemModel")

// @desc      add to cart
// @route     POST /api/v1/products/cart
// @access    Private
exports.addToCart = asyncHandler(async (req, res, next) => {
  const { product } = req.body

  // const user = await User.findById(req.user)
  let cartItem = await CartItem.findOne({ product, user: req.user }).select(
    "-user"
  )

  if (!cartItem) {
    // item is not in the cart
    cartItem = await CartItem.create({ product, user: req.user })
  } else {
    // cart item is in the cart
    cartItem.quantity += 1
  }

  await cartItem.save()

  // const cartItem = await CartItem.create({ product, user: req.user, quantity })

  res.status(200).json({
    status: "success",
    data: { cartItem }
  })
})

// @desc      remove from cart
// @route     POST /api/v1/products/cart/:cartItemId
// @access    Private
exports.removeFromCart = asyncHandler(async (req, res, next) => {
  const cartItem = await CartItem.findById(req.params.cartItemId)

  if (!cartItem) {
    return next(new AppError("Cart item not found", 401))
  }

  if (!cartItem.user.equals(req.user._id)) {
    return next(new AppError("Cart item does not belong to the user", 403))
  }

  await CartItem.findByIdAndDelete(req.params.cartItemId)

  res.status(204).json({
    status: "success",
    data: null
  })
})
