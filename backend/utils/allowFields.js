const allowFields = (body, ...allowedFields) => {
  // Object.keys(obj) returns an array will all the key names in that obj
  const newObj = {}
  Object.keys(body).forEach(key => {
    if (allowedFields.includes(key)) newObj[key] = body[key]
  })

  return newObj
}

module.exports = allowFields
