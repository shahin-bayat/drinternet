class AppError extends Error {
  constructor(message, statusCode) {
    super(message)

    this.statusCode = statusCode
    this.status = `${statusCode}`.startsWith("4") ? "fail" : "error"
    this.isOperational = true

    // stack shows where the error has happened (err.stack) and we want to preserve that
    Error.captureStackTrace(this, this.constructor)
  }
}

module.exports = AppError
