const nodemailer = require("nodemailer")
const environments = require("../config/env")

const sendEmail = async (options) => {
  // 1) Create a transporter
  /* const transporter = nodemailer.createTransport({
    host: process.env.EMAIL_HOST,
    port: process.env.EMAIL_PORT,
    auth: {
      user: process.env.EMAIL_USERNAME,
      pass: process.env.EMAIL_PASSWORD
    }
  }) */

  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: "drinternet20@gmail.com",
      pass: environments.production.gmail_password,
    },
  })

  // 2) Define email options
  const mailOptions = {
    from: options.from,
    to: options.email,
    subject: options.subject,
    text: options.message,
    // html: "<button>hello</button>"
  }
  // 3) Send email
  await transporter.sendMail(mailOptions)
}

module.exports = sendEmail
