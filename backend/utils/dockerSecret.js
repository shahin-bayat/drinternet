// dependencies
const fs = require("fs")
const AppError = require("./appError")

function dockerSecret(secretNameAndPath) {
  try {
    return fs.readFileSync(`${secretNameAndPath}`, "utf8")
  } catch (err) {
    if (err.code !== "ENOENT") {
      return new AppError(
        `An error occurred while trying to read the secret: ${secretNameAndPath}. Err: ${err}`
      )
    } else {
      return new AppError(
        `Could not find the secret, probably not running in swarm mode: ${secretNameAndPath}. Err: ${err}`
      )
    }
  }
}

module.exports = dockerSecret
